﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AOT;

public class Example : MonoBehaviour {

	public GameObject UIContainerEnter;
	public UnityEngine.UI.Text LabelEnter;

	public GameObject UIContainerStay;
	public UnityEngine.UI.Text LabelStay;

	public GameObject UIContainerExit;
	public UnityEngine.UI.Text LabelExit;

	public GameObject UIContainerHover;
	public UnityEngine.UI.Text LabelHover;

	public GameObject UIContainerAdditional;
	public UnityEngine.UI.Text LabelAdditional;

	
	Tamoco.IOS.Tamoco.TriggerCallback _iOSBeaconHolder;
	Tamoco.IOS.Tamoco.TriggerCallback _iOSEddyHolder;
	Tamoco.IOS.Tamoco.TriggerCallback _iOSWiFiHolder;
	Tamoco.IOS.Tamoco.TriggerCallback _iOSGeoFenceHolder;



	void Awake(){
		if(Application.platform == RuntimePlatform.IPhonePlayer)
			Tamoco.IOS.Tamoco.TamocoInitialise ("1088", "87141510322930363d5a210ccbc91c9151c3b47a");
	}

	public void StartButton(){
		Debug.Log ("Start Clicked");
		Tamoco.Android.Tamoco.Instance.StartTamoco ();
	}

	void OnEnable(){
		if (Application.platform == RuntimePlatform.Android) {
			Tamoco.Android.Tamoco.Instance.SetIdandKey ("1088", "87141510322930363d5a210ccbc91c9151c3b47a");
			SetCallbacks (true);
		} 

		if (Application.platform == RuntimePlatform.IPhonePlayer) {
			_iOSBeaconHolder = new Tamoco.IOS.Tamoco.TriggerCallback (IOSBeaconCallback);
			Tamoco.IOS.Tamoco.TamocoSetCallback (Tamoco.IOS.Tamoco.CallbackType.BeaconCallback, _iOSBeaconHolder);

			_iOSEddyHolder = new Tamoco.IOS.Tamoco.TriggerCallback (IOSEddyCallback);
			Tamoco.IOS.Tamoco.TamocoSetCallback (Tamoco.IOS.Tamoco.CallbackType.EddystoneCallback, _iOSEddyHolder);

			_iOSWiFiHolder = new Tamoco.IOS.Tamoco.TriggerCallback (IOSWiFiCallback);
			Tamoco.IOS.Tamoco.TamocoSetCallback (Tamoco.IOS.Tamoco.CallbackType.WifiCallback, _iOSWiFiHolder);

			_iOSGeoFenceHolder = new Tamoco.IOS.Tamoco.TriggerCallback (IOSGeoFenceCallback);
			Tamoco.IOS.Tamoco.TamocoSetCallback (Tamoco.IOS.Tamoco.CallbackType.FenceCallback, _iOSGeoFenceHolder);
		}
	}

	void OnDisable(){
		if (Application.platform == RuntimePlatform.Android) {
			SetCallbacks (false);
		}

		if (Application.platform == RuntimePlatform.IPhonePlayer) {
			Tamoco.IOS.Tamoco.TamocoSetCallback (Tamoco.IOS.Tamoco.CallbackType.EddystoneCallback);
			Tamoco.IOS.Tamoco.TamocoSetCallback (Tamoco.IOS.Tamoco.CallbackType.BeaconCallback);
			Tamoco.IOS.Tamoco.TamocoSetCallback (Tamoco.IOS.Tamoco.CallbackType.FenceCallback);
			Tamoco.IOS.Tamoco.TamocoSetCallback (Tamoco.IOS.Tamoco.CallbackType.WifiCallback);
		}
	}

	//@@@@@ IOS @@@@@

	[MonoPInvokeCallback(typeof(Tamoco.IOS.Tamoco.TriggerCallback))]
	public void IOSBeaconCallback(Tamoco.IOS.Tamoco.ActionType type, string id, string name){
			switch (type) {

				case Tamoco.IOS.Tamoco.ActionType.ActionTypeBleEnter:
					if (!UIContainerEnter.activeInHierarchy) {
						LabelEnter.text = "Beacon(IOS) enter, " + " name of event " + name + " id of event: " + id;
						UIContainerEnter.SetActive(true);
						StartCoroutine (TurnOffContainer (UIContainerEnter, 10));
					}
					break;

				case Tamoco.IOS.Tamoco.ActionType.ActionTypeBleExit:
					if (!UIContainerExit.activeInHierarchy) {
						LabelExit.text = "Eddystone(IOS) exit, " + " name of event " + name + " id of event: " + id;
						UIContainerExit.SetActive(true);
						StartCoroutine (TurnOffContainer (UIContainerExit, 10)); 
					}
					break;
			
				case Tamoco.IOS.Tamoco.ActionType.ActionTypeBleDwell: 	
					if (!UIContainerStay.activeInHierarchy) {
						LabelStay.text = "Beacon(IOS) stay, " + " name of event " + name + " id of event: " + id;
						UIContainerStay.SetActive(true);
						StartCoroutine (TurnOffContainer (UIContainerStay, 10));
					}
					break;

				case Tamoco.IOS.Tamoco.ActionType.ActionTypeBleHover:
					if (!UIContainerHover.activeInHierarchy) {
						LabelExit.text = "Beacon(IOS) hover, " + " name of event " + name + " id of event: " + id;
						UIContainerHover.SetActive(true);
						StartCoroutine (TurnOffContainer (UIContainerHover, 10)); 
					}

					break;
			}

	}

	[MonoPInvokeCallback(typeof(Tamoco.IOS.Tamoco.TriggerCallback))]
	public void IOSEddyCallback(Tamoco.IOS.Tamoco.ActionType type, string id, string name){
		switch (type) {

			case Tamoco.IOS.Tamoco.ActionType.ActionTypeEddyDwell:
				if (!UIContainerStay.activeInHierarchy) {
					LabelStay.text = "Eddystone(IOS) stay, " + " name of event " + name + " id of event: " + id;
					UIContainerStay.SetActive(true);
					StartCoroutine (TurnOffContainer (UIContainerStay, 10)); 
				}
				break;

			case Tamoco.IOS.Tamoco.ActionType.ActionTypeEddyEnter:
				if (!UIContainerEnter.activeInHierarchy) {
					LabelEnter.text = "Eddystone(IOS) enter, " + " name of event " + name + " id of event: " + id;
					UIContainerEnter.SetActive(true);
					StartCoroutine (TurnOffContainer (UIContainerEnter, 10)); 
				}
				break;

			case Tamoco.IOS.Tamoco.ActionType.ActionTypeEddyExit: 	
				if (!UIContainerExit.activeInHierarchy) {
					LabelExit.text = "Eddystone(IOS) exit, " + " name of event " + name + " id of event: " + id;
					UIContainerExit.SetActive(true);
					StartCoroutine (TurnOffContainer (UIContainerExit, 10)); 
				}
				break;

			case Tamoco.IOS.Tamoco.ActionType.ActionTypeEddyHover:
				if (!UIContainerHover.activeInHierarchy) {
					LabelHover.text = "Eddystone(IOS) hover, " + " name of event " + name + " id of event: " + id;
					UIContainerHover.SetActive(true);
					StartCoroutine (TurnOffContainer (UIContainerHover, 10)); 
				}
				break;
		}
	}

	[MonoPInvokeCallback(typeof(Tamoco.IOS.Tamoco.TriggerCallback))]
	public void IOSWiFiCallback(Tamoco.IOS.Tamoco.ActionType type, string id, string name){
		switch (type) {

		case Tamoco.IOS.Tamoco.ActionType.ActionTypeWifiConnect:
			if (!UIContainerHover.activeInHierarchy) {
				LabelHover.text = "WiFi(IOS) Connected, " + " name of event " + name + " id of event: " + id;
				UIContainerHover.SetActive(true);
				StartCoroutine (TurnOffContainer (UIContainerHover, 10)); 
			}

			break;

		case Tamoco.IOS.Tamoco.ActionType.ActionTypeWifiDisconnect:
			if (!UIContainerAdditional.activeInHierarchy) {
				LabelAdditional.text = "WiFi(IOS) Disconnect, " + " name of event " + name + " id of event: " + id;
				UIContainerAdditional.SetActive(true);
				StartCoroutine (TurnOffContainer (UIContainerAdditional, 10)); 
			}

			break;

		case Tamoco.IOS.Tamoco.ActionType.ActionTypeWifiDwell: 	
			if (!UIContainerStay.activeInHierarchy) {
				LabelStay.text = "Wifi(IOS) stay, " + " name of event " + name + " id of event: " + id;
				UIContainerStay.SetActive(true);
				StartCoroutine (TurnOffContainer (UIContainerStay, 10)); 
			}

			break;

		case Tamoco.IOS.Tamoco.ActionType.ActionTypeWifiEnter:
			if (!UIContainerEnter.activeInHierarchy) {
				LabelEnter.text = "WiFi(IOS) enter, " + " name of event " + name + " id of event: " + id;
				UIContainerEnter.SetActive(true);
				StartCoroutine (TurnOffContainer (UIContainerEnter, 10)); 
			}

			break;
		
		case Tamoco.IOS.Tamoco.ActionType.ActionTypeWifiExit:
			if (!UIContainerExit.activeInHierarchy) {
				LabelExit.text = "WiFi(IOS) exit, " + " name of event " + name + " id of event: " + id;
				UIContainerExit.SetActive(true);
				StartCoroutine (TurnOffContainer (UIContainerExit, 10)); 
			}

			break;
		}
	}

	[MonoPInvokeCallback(typeof(Tamoco.IOS.Tamoco.TriggerCallback))]
	public void IOSGeoFenceCallback(Tamoco.IOS.Tamoco.ActionType type, string id, string name){
		switch (type) {

		case Tamoco.IOS.Tamoco.ActionType.ActionTypeGeoFenceEnter:
			if (!UIContainerEnter.activeInHierarchy) {
				LabelEnter.text = "GeoFence(IOS) enter, " + " name of event " + name + " id of event: " + id;
				UIContainerEnter.SetActive(true);
				StartCoroutine (TurnOffContainer (UIContainerEnter, 10)); 
			}

			break;

		case Tamoco.IOS.Tamoco.ActionType.ActionTypeGeoFenceExit:
			if (!UIContainerExit.activeInHierarchy) {
				LabelExit.text = "GeoFence(IOS) exit, " + " name of event " + name + " id of event: " + id;
				UIContainerExit.SetActive(true);
				StartCoroutine (TurnOffContainer (UIContainerExit, 10)); 
			}

			break;

		case Tamoco.IOS.Tamoco.ActionType.ActionTypeGeoFenceDwell: 	
			if (!UIContainerStay.activeInHierarchy) {
				LabelStay.text = "GeoFence(IOS) stay, " + " name of event " + name + " id of event: " + id;
				UIContainerStay.SetActive(true);
				StartCoroutine (TurnOffContainer (UIContainerStay, 10)); 
			}

			break;


		}
	}

	//@@@@@ ANDROID @@@@@@

	public void AndroidBeaconEnterCallback(string name, string id){
		if (!UIContainerEnter.activeInHierarchy) {
			LabelEnter.text = "Beacon(android) enter, " + " name of event " + name + " id of event: " + id;
			UIContainerEnter.SetActive(true);
			StartCoroutine (TurnOffContainer (UIContainerEnter, 10));
		}
	}

	public void AndroidBeaconExitCallback(string name, string id){
		if (!UIContainerExit.activeInHierarchy) {
			LabelExit.text = "Eddystone(android) exit, " + " name of event " + name + " id of event: " + id;
			UIContainerExit.SetActive(true);
			StartCoroutine (TurnOffContainer (UIContainerExit, 10)); 
		}
	}

	public void AndroidBeaconStayCallback(string name, string id){
		if (!UIContainerStay.activeInHierarchy) {
			LabelStay.text = "Beacon(android) stay, " + " name of event " + name + " id of event: " + id;
			UIContainerStay.SetActive(true);
			StartCoroutine (TurnOffContainer (UIContainerStay, 10));
		}
	}

	public void AndroidBeaconHoverCallback(string name, string id){
		if (!UIContainerHover.activeInHierarchy) {
			LabelExit.text = "Beacon(android) hover, " + " name of event " + name + " id of event: " + id;
			UIContainerHover.SetActive(true);
			StartCoroutine (TurnOffContainer (UIContainerHover, 10)); 
		}
	}

	public void AndroidEddyEnterCallback(string name, string id){
		if (!UIContainerEnter.activeInHierarchy) {
			LabelEnter.text = "Eddystone(android) enter, " + " name of event " + name + " id of event: " + id;
			UIContainerEnter.SetActive(true);
			StartCoroutine (TurnOffContainer (UIContainerEnter, 10)); 
		}
	}

	public void AndroidEddyExitCallback(string name, string id){
		if (!UIContainerExit.activeInHierarchy) {
			LabelExit.text = "Eddystone(android) exit, " + " name of event " + name + " id of event: " + id;
			UIContainerExit.SetActive(true);
			StartCoroutine (TurnOffContainer (UIContainerExit, 10)); 
		}
	}

	public void AndroidEddyStayCallback(string name, string id){
		if (!UIContainerStay.activeInHierarchy) {
			LabelStay.text = "Eddystone(android) stay, " + " name of event " + name + " id of event: " + id;
			UIContainerStay.SetActive(true);
			StartCoroutine (TurnOffContainer (UIContainerStay, 10)); 
		}
	}

	public void AndroidEddyHoverCallback(string name, string id){
		if (!UIContainerHover.activeInHierarchy) {
			LabelHover.text = "Eddystone(android) hover, " + " name of event " + name + " id of event: " + id;
			UIContainerHover.SetActive(true);
			StartCoroutine (TurnOffContainer (UIContainerHover, 10)); 
		}
	}

	public void AndroidWiFiConnectCallback(string name, string id){
		if (!UIContainerHover.activeInHierarchy) {
			LabelHover.text = "WiFi(android) Connected, " + " name of event " + name + " id of event: " + id;
			UIContainerHover.SetActive(true);
			StartCoroutine (TurnOffContainer (UIContainerHover, 10)); 
			Debug.Log ("WiFi(android) Connected, " + " name of event " + name + " id of event: " + id);
		}
	}

	public void AndroidWiFiDisconnectCallback(string name, string id){
		if (!UIContainerAdditional.activeInHierarchy) {
			LabelAdditional.text = "WiFi(android) Disconnect, " + " name of event " + name + " id of event: " + id;
			UIContainerAdditional.SetActive(true);
			StartCoroutine (TurnOffContainer (UIContainerAdditional, 10));
			Debug.Log ("WiFi(android) Disconnect, " + " name of event " + name + " id of event: " + id);
		}
	}

	public void AndroidWiFiEnterCallback(string name, string id){
		if (!UIContainerEnter.activeInHierarchy) {
			LabelEnter.text = "WiFi(android) enter, " + " name of event " + name + " id of event: " + id;
			UIContainerEnter.SetActive(true);
			StartCoroutine (TurnOffContainer (UIContainerEnter, 10)); 
			Debug.Log ("WiFi(android) enter, " + " name of event " + name + " id of event: " + id);
		}
	}

	public void AndroidWiFiExitCallback(string name, string id){
		if (!UIContainerExit.activeInHierarchy) {
			LabelExit.text = "WiFi(android) exit, " + " name of event " + name + " id of event: " + id;
			UIContainerExit.SetActive(true);
			StartCoroutine (TurnOffContainer (UIContainerExit, 10)); 
			Debug.Log ("WiFi(android) exit, " + " name of event " + name + " id of event: " + id);
		}
	}

	public void AndroidWiFiStayCallback(string name, string id){
		if (!UIContainerStay.activeInHierarchy) {
			LabelStay.text = "Wifi(android) stay, " + " name of event " + name + " id of event: " + id;
			UIContainerStay.SetActive(true);
			StartCoroutine (TurnOffContainer (UIContainerStay, 10)); 
			Debug.Log ("Wifi(android) stay, " + " name of event " + name + " id of event: " + id);
		}
	}

	public void AndroidGeoFenceEnterCallback(string name, string id){
		if (!UIContainerEnter.activeInHierarchy) {
			LabelEnter.text = "GeoFence(android) enter, " + " name of event " + name + " id of event: " + id;
			UIContainerEnter.SetActive(true);
			StartCoroutine (TurnOffContainer (UIContainerEnter, 10)); 
		}
	}

	public void AndroidGeoFenceExitCallback(string name, string id){
		if (!UIContainerExit.activeInHierarchy) {
			LabelExit.text = "GeoFence(android) exit, " + " name of event " + name + " id of event: " + id;
			UIContainerExit.SetActive(true);
			StartCoroutine (TurnOffContainer (UIContainerExit, 10)); 
		}
	}

	public void AndroidGeoFenceStayCallback(string name, string id){
		if (!UIContainerStay.activeInHierarchy) {
			LabelStay.text = "GeoFence(android) stay, " + " name of event " + name + " id of event: " + id;
			UIContainerStay.SetActive(true);
			StartCoroutine (TurnOffContainer (UIContainerStay, 10)); 
		}
	}

	private void SetCallbacks(bool flag){
		if (flag) {

			Tamoco.Android.Tamoco.Instance.InventoryUpdate += delegate {
				Debug.Log ("inventory Update");
			};
			
			Tamoco.Android.Tamoco.Instance.BleEnter += AndroidBeaconEnterCallback;
			Tamoco.Android.Tamoco.Instance.BleDwell += AndroidBeaconStayCallback;
			Tamoco.Android.Tamoco.Instance.BleExit += AndroidBeaconExitCallback;
			Tamoco.Android.Tamoco.Instance.BleHover += AndroidBeaconHoverCallback;

			Tamoco.Android.Tamoco.Instance.EddystoneExit += AndroidEddyExitCallback;
			Tamoco.Android.Tamoco.Instance.EddystoneEnter += AndroidEddyEnterCallback;
			Tamoco.Android.Tamoco.Instance.EddystoneDwell += AndroidEddyStayCallback;
			Tamoco.Android.Tamoco.Instance.EddystoneHover += AndroidEddyHoverCallback;

			Tamoco.Android.Tamoco.Instance.GeofenceDwell += AndroidGeoFenceStayCallback;
			Tamoco.Android.Tamoco.Instance.GeofenceEnter += AndroidGeoFenceEnterCallback;
			Tamoco.Android.Tamoco.Instance.GeofenceExit += AndroidGeoFenceExitCallback;

			Tamoco.Android.Tamoco.Instance.WifiExit += AndroidWiFiExitCallback;
			Tamoco.Android.Tamoco.Instance.WifiEnter += AndroidWiFiEnterCallback;
			Tamoco.Android.Tamoco.Instance.WifiDwell += AndroidWiFiStayCallback;
			Tamoco.Android.Tamoco.Instance.WifiDisconnect += AndroidWiFiDisconnectCallback;
			Tamoco.Android.Tamoco.Instance.WifiConnect += AndroidWiFiConnectCallback;

		} else {

			Tamoco.Android.Tamoco.Instance.BleEnter -= AndroidBeaconEnterCallback;
			Tamoco.Android.Tamoco.Instance.BleDwell -= AndroidBeaconStayCallback;
			Tamoco.Android.Tamoco.Instance.BleExit -= AndroidBeaconExitCallback;
			Tamoco.Android.Tamoco.Instance.BleHover -= AndroidBeaconHoverCallback;

			Tamoco.Android.Tamoco.Instance.EddystoneExit -= AndroidEddyExitCallback;
			Tamoco.Android.Tamoco.Instance.EddystoneEnter -= AndroidEddyEnterCallback;
			Tamoco.Android.Tamoco.Instance.EddystoneDwell -= AndroidEddyStayCallback;
			Tamoco.Android.Tamoco.Instance.EddystoneHover -= AndroidEddyHoverCallback;

			Tamoco.Android.Tamoco.Instance.GeofenceDwell -= AndroidGeoFenceStayCallback;
			Tamoco.Android.Tamoco.Instance.GeofenceEnter -= AndroidGeoFenceEnterCallback;
			Tamoco.Android.Tamoco.Instance.GeofenceExit -= AndroidGeoFenceExitCallback;

			Tamoco.Android.Tamoco.Instance.WifiExit -= AndroidWiFiExitCallback;
			Tamoco.Android.Tamoco.Instance.WifiEnter -= AndroidWiFiEnterCallback;
			Tamoco.Android.Tamoco.Instance.WifiDwell -= AndroidWiFiStayCallback;
			Tamoco.Android.Tamoco.Instance.WifiDisconnect -= AndroidWiFiDisconnectCallback;
			Tamoco.Android.Tamoco.Instance.WifiConnect -= AndroidWiFiConnectCallback;

		}

	}


	IEnumerator TurnOffContainer(GameObject UIContainer, float time){
		yield return new WaitForSeconds (time);
		UIContainer.SetActive (false);
	}
}
