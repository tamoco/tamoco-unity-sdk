﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Tamoco.Android{
	public class Tamoco : Singleton<Tamoco>{

		public Action<string, string> EddystoneEnter;
		public Action<string, string> EddystoneHover;
		public Action<string, string> EddystoneExit;
		public Action<string, string> GeofenceEnter;
		public Action<string, string> GeofenceDwell;
		public Action<string, string> GeofenceExit;
		public Action<string, string> EddystoneDwell;
		public Action<string, string> WifiEnter;
		public Action<string, string> WifiExit;
		public Action<string, string> WifiDwell;
		public Action<string, string> WifiConnect;
		public Action<string, string> WifiDisconnect;
		public Action<string, string> BleEnter;
		public Action<string, string> BleHover;
		public Action<string, string> BleDwell;
		public Action<string, string> BleExit;
		public Action InventoryUpdate;
		public Action<string, string> PushToVault;
		public Action<string, string> PushToVaultError;
		public Action<string, string> TrackTap;
		public Action<string, string> TrackTapError;
		public Action<string> JsonReceived;

		private AndroidJavaObject _Jo;
		private bool _Initalised;
		private bool _Running;



		void Awake(){
			if(Application.platform == RuntimePlatform.Android && !Tamoco.Instance._Initalised){
				Tamoco.Instance._Jo = new AndroidJavaObject("co.tamo.proximity.unity.UnityBridge");
				Tamoco.Instance._Initalised = true;
				Tamoco.Instance._Running = false;
				DontDestroyOnLoad(Tamoco.Instance.gameObject);
			}
		}

		#region Message Receivers
			
		private void JsonHandler(string json){
			if (Tamoco.Instance.JsonReceived != null)
				Tamoco.Instance.JsonReceived.Invoke (json);
		}

		private void ProximityEventReceiver(string msg){
			string[] result = msg.Split(new string[]{"@action:", "@name:", "@id:"}, StringSplitOptions.RemoveEmptyEntries);
			
			switch(result[0]){
				case "INVENTORY_UPDATED_ACTION":
					if(Tamoco.Instance.InventoryUpdate != null){
						Tamoco.Instance.InventoryUpdate.Invoke();
					}
					break;
				case "GEOFENCE_ENTER":
					if(Tamoco.Instance.GeofenceEnter != null){
						Tamoco.Instance.GeofenceEnter.Invoke(result[1], result[2]);
					}
					break;
				case "GEOFENCE_DWELL":
					if(Tamoco.Instance.GeofenceDwell != null){
						Tamoco.Instance.GeofenceDwell.Invoke(result[1], result[2]);
					}
					break;
				case "GEOFENCE_EXIT":
					if(Tamoco.Instance.GeofenceExit != null){
						Tamoco.Instance.GeofenceExit.Invoke(result[1], result[2]);
					}
					break;
				case "BLE_ENTER":
					if(Tamoco.Instance.BleEnter != null){
						Tamoco.Instance.BleEnter.Invoke(result[1], result[2]);
					}
					break;
				case "BLE_HOVER":
					if(Tamoco.Instance.BleHover != null){
						Tamoco.Instance.BleHover.Invoke(result[1], result[2]);
					}
					break;
				case "BLE_DWELL":
					if(Tamoco.Instance.BleDwell != null){
						Tamoco.Instance.BleDwell.Invoke(result[1], result[2]);
					}
					break;
				case "BLE_EXIT":
					if(Tamoco.Instance.BleExit != null){
						Tamoco.Instance.BleExit.Invoke(result[1], result[2]);
					}
					break;
				case "EDY_ENTER":
					if(Tamoco.Instance.EddystoneEnter != null){
						Tamoco.Instance.EddystoneEnter.Invoke(result[1], result[2]);
					}
					break;
				case "EDY_HOVER":
					if(Tamoco.Instance.EddystoneHover != null){
						Tamoco.Instance.EddystoneHover.Invoke(result[1], result[2]);
					}
					break;
				case "EDY_DWELL":
					if(Tamoco.Instance.EddystoneDwell != null){
						Tamoco.Instance.EddystoneDwell.Invoke(result[1], result[2]);
					}
					break;
				case "EDY_EXIT":
					if(Tamoco.Instance.EddystoneExit != null){
						Tamoco.Instance.EddystoneExit.Invoke(result[1], result[2]);
					}
					break;
				case "WIFI_ENTER":
					if(Tamoco.Instance.WifiEnter != null){
						Tamoco.Instance.WifiEnter.Invoke(result[1], result[2]);
					}
					break;
				case "WIFI_DWELL":
					if(Tamoco.Instance.WifiDwell != null){
						Tamoco.Instance.WifiDwell.Invoke(result[1], result[2]);
					}
					break;
				case "WIFI_EXIT":
					if(Tamoco.Instance.WifiExit != null){
						Tamoco.Instance.WifiExit.Invoke(result[1], result[2]);
					}
					break;
				case "WIFI_CONNECT":
					if(Tamoco.Instance.WifiConnect != null){
						Tamoco.Instance.WifiConnect.Invoke(result[1], result[2]);
					}
					break;
				case "WIFI_DISCONNECT":
					if(Tamoco.Instance.WifiDisconnect != null){
						Tamoco.Instance.WifiDisconnect.Invoke(result[1], result[2]);
					}
					break;

				
			}
		}

		#endregion

		#region Methods to Call TamocoSDK

		public void ClearCustomID(){
			if(_Running)
				_Jo.Call("ClearCustomID");
		}

		public string GetCustomID(){
			if(_Running)
				return _Jo.Call<string>("ClearCustomID");
			else	
				return "";
		}

		public void SetCustomID(string ID){
			if(_Running)
				_Jo.Call("SetCustomID" , ID);
		}

		public void ClearIdandKey(){
			if(_Running)
				_Jo.Call("ClearIdandKey");
		}

		public void SetIdandKey(string ApiKey, string ApiSecret){
			if(_Running)
				_Jo.Call("SetIdandKey", ApiKey, ApiSecret);
		}

		public string GetApiKey(){
			if(_Running)
				return _Jo.Call<string>("GetApiKey");
			else
				return "";
		}

		public string GetApiSecret(){
			if(_Running)
				return _Jo.Call<string>("GetApiSecret");
			else
				return "";
		}

		public void ListenOnlyMode(bool setListenOnlymode){
			if(_Running)
					_Jo.Call("ListenOnlyMode", setListenOnlymode);
		}

		public void StartTamoco(){
			if (_Initalised) {
				_Jo.Call ("StartTamoco");
				_Running = true;
			}
		}

		public void StopTamoco(){
			if (_Running) {
				_Jo.Call ("StopTamoco");
				_Running = false;
			}
		}

		public void ForegroundOnly(bool enabled){
			if (_Running) {
				_Jo.Call ("ForegroundOnlyMode", enabled);
			}
		}


		public void TamocoInit(string ApiKey, string ApiSecret, bool startImmediate = true, bool foregroudOnly = false, bool listenOnly = false, bool debugNotification = false, bool debugLog = false, 
								int locationUpdateTime = 60, int inventoryUpdateTime = 60, int settingsUpdateTime = 480, int beaconsScanInterval = 15, int beaconsScanDuration = 3,
								long minInventoryUpdateInterval = 1, long requestRetryTime = 5, int geofenceDwellTime = 5, int wifiDwellTime = 5, int beaconDwellTime = 1, int beaconHoverTime = 10, double beaconNearDistance = 3)
		{

			_Jo.Call("init", ApiKey, ApiSecret, startImmediate, foregroudOnly, listenOnly, debugNotification, debugLog, locationUpdateTime, inventoryUpdateTime, settingsUpdateTime, beaconsScanInterval, 
					beaconsScanDuration, minInventoryUpdateInterval, requestRetryTime, geofenceDwellTime, wifiDwellTime, beaconDwellTime, beaconHoverTime, beaconNearDistance
					);
		
		}


		#endregion
	
}

}
