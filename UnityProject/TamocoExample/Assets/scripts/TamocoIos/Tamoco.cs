﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AOT;
using System.Runtime.InteropServices;

namespace Tamoco.IOS
{
[StructLayout (LayoutKind.Sequential)]
public class Tamoco {

	public delegate void TriggerCallback(ActionType type, string ID, string name);

	[DllImport ("__Internal")]
	private static extern void Framework_setCustomFenceAction (CallbackType type, TriggerCallback callback = null);

	[DllImport ("__Internal")]
	private static extern void Framework_Initialise(string Api_Key, string Api_Secret);

	[DllImport ("__Internal")]
	private static extern bool Framework_SetCustomID (string Id);

	[DllImport ("__Internal")]
	private static extern void Framework_isCurrent ();

	[DllImport ("__Internal")]
	private static extern void Framework_PushKeyViaVault(string key);

	[DllImport ("__Internal")]
	private static extern void Framework_TapViaTracking (string code, string variant);


	public static void TamocoInitialise(string Api_Key, string Api_Secret){
		Framework_Initialise (Api_Key, Api_Secret);
	}


	public static bool TamocoSetCustomID(string Id){
		return Framework_SetCustomID (Id);
	}

	public static void TamocoSetCallback(CallbackType type, TriggerCallback callback = null){
		Framework_setCustomFenceAction (type, callback);
	}
			

	public static void TamocoPushKeyWithVault(string key){
		Framework_PushKeyViaVault (key);
	}

	public static void voidTamocoTapWithTracking(string code, string variant){
		Framework_TapViaTracking(code, variant);
	}


	public enum ActionType {

			/// When the device enters Beacon region.
			ActionTypeBleEnter = 11,

			/// When the device is within 1m range from the beacon.
			ActionTypeBleHover = 12,

			/// When the device is withing Beacon region for a certen time.
			ActionTypeBleDwell = 13,

			/// When te device exits the Beacon region.
			ActionTypeBleExit = 14,

			/// When the device enters GeoFence region.
			ActionTypeGeoFenceEnter = 21,

			/// When the device is within a GeoFence for a certen time.
			ActionTypeGeoFenceDwell = 22,

			/// When the device exits GeoFence region.
			ActionTypeGeoFenceExit = 23,

			/// When the device enters the WiFi region.
			ActionTypeWifiEnter = 31,

			/// When the device is connected to a Wifi for a certen time.
			ActionTypeWifiDwell = 32,

			/// When the device exits the WiFi region.
			ActionTypeWifiExit = 33,

			/// When the device connects to the WiFi.
			ActionTypeWifiConnect = 34,

			/// When the device disconnects to the WiFi
			ActionTypeWifiDisconnect = 35,

			/// When the device enters Beacon region.
			ActionTypeEddyEnter = 61,

			/// When the device is within 1m range from the beacon.
			ActionTypeEddyHover = 62,

			/// When the device is withing Beacon region for a certen time.
			ActionTypeEddyDwell = 63,

			/// When te device exits the Beacon region.
			ActionTypeEddyExit = 64,

		};

	public enum CallbackType{
		
		BeaconCallback,

		WifiCallback,

		EddystoneCallback,

		FenceCallback,

		ErrorCallback,

		Debug

	};
}

}
