﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Tamoco.Android{
public class Tamoco : MonoBehaviour {

	public static Action<string, string> EddystoneEnter;
	public static Action<string, string> EddystoneHover;
	public static Action<string, string> EddystoneExit;
	public static Action<string, string> GeofenceEnter;
	public static Action<string, string> GeofenceDwell;
	public static Action<string, string> GeofenceExit;
	public static Action<string, string> EddystoneDwell;
	public static Action<string, string> WifiEnter;
	public static Action<string, string> WifiExit;
	public static Action<string, string> WifiDwell;
	public static Action<string, string> WifiConnect;
	public static Action<string, string> WifiDisconnect;
	public static Action<string, string> BleEnter;
	public static Action<string, string> BleHover;
	public static Action<string, string> BleDwell;
	public static Action<string, string> BleExit;
	public static Action InventoryUpdate;
	public static Action<string, string> PushToVault;
	public static Action<string, string> PushToVaultError;
	public static Action<string, string> TrackTap;
	public static Action<string, string> TrackTapError;


	private static Tamoco _Instance;
	private AndroidJavaObject _Jo;
	private bool _Initalised;


	public static Tamoco Instance{
		get{
			return _Instance;
		}
	}


	void Awake(){
		_Initalised = false;
		_Instance = this;
		DontDestroyOnLoad(this);
	}

	// Use this for initialization
	void Start () {
		if(Application.platform == RuntimePlatform.Android && !_Initalised){
			_Jo = new AndroidJavaObject("co.tamo.proximity.unity.UnityBridge");
			_Jo.Call("init");
			_Initalised = true;
		}
	}
	

	private void ProximityEventReceiver(string msg){
		string[] result = msg.Split(new string[]{"@action:", "@name:", "@id:"}, StringSplitOptions.RemoveEmptyEntries);
		
		switch(result[0]){
			case "INVENTORY_UPDATED_ACTION":
				if(Tamoco.InventoryUpdate != null){
					Tamoco.InventoryUpdate.Invoke();
				}
				break;
			case "GEOFENCE_ENTER":
				if(Tamoco.GeofenceEnter != null){
					Tamoco.GeofenceEnter.Invoke(result[1], result[2]);
				}
				break;
			case "GEOFENCE_DWELL":
				if(Tamoco.GeofenceDwell != null){
					Tamoco.GeofenceDwell.Invoke(result[1], result[2]);
				}
				break;
			case "GEOFENCE_EXIT":
				if(Tamoco.GeofenceExit != null){
					Tamoco.GeofenceExit.Invoke(result[1], result[2]);
				}
				break;
			case "BLE_ENTER":
				if(Tamoco.BleEnter != null){
					Tamoco.BleEnter.Invoke(result[1], result[2]);
				}
				break;
			case "BLE_HOVER":
				if(Tamoco.BleHover != null){
					Tamoco.BleHover.Invoke(result[1], result[2]);
				}
				break;
			case "BLE_DWELL":
				if(Tamoco.BleDwell != null){
					Tamoco.BleDwell.Invoke(result[1], result[2]);
				}
				break;
			case "BLE_EXIT":
				if(Tamoco.BleExit != null){
					Tamoco.BleExit.Invoke(result[1], result[2]);
				}
				break;
			case "EDY_ENTER":
				if(Tamoco.EddystoneEnter != null){
					Tamoco.EddystoneEnter.Invoke(result[1], result[2]);
				}
				break;
			case "EDY_HOVER":
				if(Tamoco.EddystoneHover != null){
					Tamoco.EddystoneHover.Invoke(result[1], result[2]);
				}
				break;
			case "EDY_DWELL":
				if(Tamoco.EddystoneDwell != null){
					Tamoco.EddystoneDwell.Invoke(result[1], result[2]);
				}
				break;
			case "EDY_EXIT":
				if(Tamoco.EddystoneExit != null){
					Tamoco.EddystoneExit.Invoke(result[1], result[2]);
				}
				break;
			case "WIFI_ENTER":
				if(Tamoco.WifiEnter != null){
					Tamoco.WifiEnter.Invoke(result[1], result[2]);
				}
				break;
			case "WIFI_DWELL":
				if(Tamoco.WifiDwell != null){
					Tamoco.WifiDwell.Invoke(result[1], result[2]);
				}
				break;
			case "WIFI_EXIT":
				if(Tamoco.WifiExit != null){
					Tamoco.WifiExit.Invoke(result[1], result[2]);
				}
				break;
			case "WIFI_CONNECT":
				if(Tamoco.WifiConnect != null){
					Tamoco.WifiConnect.Invoke(result[1], result[2]);
				}
				break;
			case "WIFI_DISCONNECT":
				if(Tamoco.WifiDisconnect != null){
					Tamoco.WifiDisconnect.Invoke(result[1], result[2]);
				}
				break;

			
		}
	}

	public void ClearCustomID(){
		if(_Initalised)
			_Jo.Call("ClearCustomID");
	}

	public string GetCustomID(){
		if(_Initalised)
			return _Jo.Call<string>("ClearCustomID");
		else	
			return "";
	}

	public void SetCustomID(string ID){
		if(_Initalised)
			_Jo.Call("SetCustomID" + ID);
	}

	public void ClearIdandKey(){
		if(_Initalised)
			_Jo.Call("ClearIdandKey");
	}

	public void SetIdandKey(string ApiKey, string ApiSecret){
		if(_Initalised)
			_Jo.Call("SetIdandKey", ApiKey, ApiSecret);
	}

	public string GetApiKey(){
		if(_Initalised)
			return _Jo.Call<string>("GetApiKey");
		else
			return "";
	}

	public string GetApiSecret(){
		if(_Initalised)
			return _Jo.Call<string>("GetApiSecret");
		else
			return "";
	}

	public void ListenOnlyMode(bool setListenOnlymode){
		if(_Initalised)
		_Jo.Call("ListenOnlyMode");
	}
}

}
