package co.tamo.proximity.unity;


import android.Manifest;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.unity3d.player.UnityPlayer;

import java.util.concurrent.TimeUnit;

import co.tamo.proximity.EventType;
import co.tamo.proximity.ProximityService;
import co.tamo.proximity.ProximityServiceBinder;
import co.tamo.proximity.ProximityServiceManager;
import co.tamo.proximity.PushToVaultArgumentException;
import co.tamo.proximity.TamocoPayloadListener;
import co.tamo.proximity.TamocoServiceBuilder;

public class UnityBridge extends Fragment {

    public static final String TAG = "Tamoco_Unity";

    public static String UnityMessageHandler = "TamocoGameObject";
    protected ProximityEventReceiver EventReceiver;
    protected BinderListener binderListener;

    public interface BinderListener {
        void onBinderConnected(ProximityServiceBinder newBinder);
    }


    @Override
    public void onResume() {
        super.onResume();
        Log.d("Unity", "resume - v: 1.2");
        EventReceiver = new ProximityEventReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ProximityService.INVENTORY_UPDATED_ACTION);
        intentFilter.addAction(ProximityService.PROXIMITY_EVENT_ACTION);
        intentFilter.addAction(ProximityService.PUSH_TO_VAULT_ACTION);
        intentFilter.addAction(ProximityService.TRACK_TAP_ACTION);
        LocalBroadcastManager.getInstance(UnityPlayer.currentActivity).registerReceiver(EventReceiver, intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("Unity", "Pause - v: 1.2");
        LocalBroadcastManager.getInstance(UnityPlayer.currentActivity).unregisterReceiver(EventReceiver);
        EventReceiver = null;
        binderListener = null;
    }


    static final long DEFAULT_INVENTORY_MIN_UPDATE_INTERVAL_MILLIS =
            TimeUnit.MILLISECONDS.convert(1,TimeUnit.MINUTES);
    static final long DEFAULT_REQUEST_RETRY_UPDATE_TIME_MILLIS =
            TimeUnit.MILLISECONDS.convert(5,TimeUnit.MINUTES);
    static final int DEFAULT_GEOFENCE_DWELL_DELAY_MILLIS =
            (int) TimeUnit.MILLISECONDS.convert(5,TimeUnit.MINUTES);
    static final int DEFAULT_WIFI_DWELL_DELAY_MILLIS =
            (int) TimeUnit.MILLISECONDS.convert(5,TimeUnit.MINUTES);
    static final int DEFAULT_BEACON_DWELL_DELAY_MILLIS =
            (int) TimeUnit.MILLISECONDS.convert(1,TimeUnit.MINUTES);
    static final int DEFAULT_BEACON_HOVER_DELAY_MILLIS =
            (int) TimeUnit.MILLISECONDS.convert(10,TimeUnit.SECONDS);
    static final double DEFAULT_BEACON_NEAR_DISTANCE_METERS = 3.0;

    public void init(String ApiKey, String ApiSecret, boolean startImmediate, boolean foregroudOnly, boolean listenOnly, boolean debugNotification, boolean debugLog, Integer locationUpdateTime, Integer inventoryUpdateTime,
                     Integer settingsUpdateTime, Integer beaconsScanInterval, Integer beaconsScanDuration, long minInventoryTime, long requestRetryTime, int geofenceDwellTime, int wifiDwellTime,
                     int beaconDwellTime, int beaconHoverTime, double beaconNearDistance){

        Log.d("Unity", "init sdk - v: 1.2");

        Fragment frag = UnityPlayer.currentActivity.getFragmentManager().findFragmentByTag(TAG);


        if(frag == null) {
            UnityPlayer.currentActivity.getFragmentManager().beginTransaction().add(this, TAG).commit();

            String[] requestedPermissions = ProximityServiceManager.requestPermissions(UnityPlayer.currentActivity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION});

            if (requestedPermissions.length == 0) {
                Log.d("Unity", "onApp sdk - v: 1.2");

                ProximityServiceManager.onRequestPermissionsResult(UnityPlayer.currentActivity,21,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},new int[]{PackageManager.PERMISSION_GRANTED});

                TamocoServiceBuilder.newInstance(UnityPlayer.currentActivity.getApplication())
                        .setApiIdAndSecret(ApiKey, ApiSecret)
                        .setDebugNotifications(debugNotification)
                        .setForegroundOnly(foregroudOnly)
                        .setDebugLog(debugLog)
                        .setStartImmediate(startImmediate)
                        .setBeaconDwellDelay(beaconDwellTime)
                        .setWifiDwellDelay(wifiDwellTime)
                        .setBeaconHoverDelay(beaconHoverTime)
                        .setBeaconNearDistance(beaconNearDistance)
                        .setBeaconsScanDuration(beaconsScanDuration)
                        .setBeaconsScanInterval(beaconsScanInterval)
                        .setGeofenceDwellDelay(geofenceDwellTime)
                        .setInventoryUpdateTime(inventoryUpdateTime)
                        .setListenOnly(listenOnly)
                        .setLocationUpdateTime(locationUpdateTime)
                        .setSettingsUpdateTime(settingsUpdateTime)
                        .setMinInventoryUpdateInterval(minInventoryTime)
                        .setRequestRetryUpdateTime(requestRetryTime)
                        .setPayloadListener(new TamocoPayloadListener() {
                            @Override
                            public void didReceiveJSON(@NonNull String payload) {
                                Log.d("TamocoApp","Received JSON Payload: "+payload);
                                UnityPlayer.UnitySendMessage(UnityBridge.UnityMessageHandler, "JsonHandler", payload);
                            }
                        })
                        .build();

            }
        }



    }

    public String GetApiSecret(){

        return ProximityServiceManager.getApiSecret(UnityPlayer.currentActivity);
    }

    public String GetApiKey(){
        return ProximityServiceManager.getApiId(UnityPlayer.currentActivity);
    }

    public void SetIdandKey(String ApiKey, String ApiSecret){
        ProximityServiceManager.setApiIdAndSecret(UnityPlayer.currentActivity, ApiKey, ApiSecret);
    }

    public void ClearIdandKey(){
        ProximityServiceManager.clearApiIdAndSecret(UnityPlayer.currentActivity);
    }

    public void SetCustomID(String id){
        ProximityServiceManager.setCustomId(UnityPlayer.currentActivity, id);
    }

    public String GetCustomID(){
        return ProximityServiceManager.getCustomId(UnityPlayer.currentActivity);
    }

    public void ClearCustomID(){
        ProximityServiceManager.clearCustomId(UnityPlayer.currentActivity);
    }

    public void ListenOnlyMode(Boolean flag){
        if(flag){
            ProximityServiceManager.enterListenOnlyMode(UnityPlayer.currentActivity);
        }else{
            ProximityServiceManager.exitListenOnlyMode(UnityPlayer.currentActivity);
        }
    }

    public void ForegroundOnlyMode(Boolean enable){
        ProximityServiceManager.changeForegroundOnlyMode(UnityPlayer.currentActivity.getApplication(), enable);
    }

    public void StopTamoco(){
        Log.d("Unity", "Stop - v: 1.2");
        ProximityServiceManager.pauseTamoco(UnityPlayer.currentActivity.getApplication());
    }

    public void StartTamoco(){
        Log.d("Unity", "Start - v: 1.2");
        ProximityServiceManager.resumeTamoco(UnityPlayer.currentActivity.getApplication());
    }

    public void PushToVault(String vaultKey){
        if (!vaultKey.isEmpty()) {
            try {
                ProximityServiceManager.pushToVault(UnityPlayer.currentActivity, vaultKey);
            } catch (PushToVaultArgumentException e) {
                Log.d("Unity" , "Error: " + e.getMessage());
            }
        }
    }

    protected class ProximityEventReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("Unity", "onReceive sdk - v: 1.2");
            String action = intent != null ? intent.getAction() : null;
            if (ProximityService.INVENTORY_UPDATED_ACTION.equals(action)) {
                UnityPlayer.UnitySendMessage(UnityBridge.UnityMessageHandler, "ProximityEventReceiver", "@action:INVENTORY_UPDATED_ACTION@name:@id:");
            } else if (ProximityService.PROXIMITY_EVENT_ACTION.equals(action)) {
                EventType eventType = (EventType) intent.getSerializableExtra(ProximityService.PROXIMITY_EVENT_EXTRA_TYPE);

                long id = intent.getLongExtra(ProximityService.PROXIMITY_EVENT_EXTRA_ID, 0);
                String name = intent.getStringExtra(ProximityService.PROXIMITY_EVENT_EXTRA_NAME);

                switch (eventType) {
                    case GEOFENCE_ENTER:
                        UnityPlayer.UnitySendMessage(UnityBridge.UnityMessageHandler, "ProximityEventReceiver", "@action:GEOFENCE_ENTER" + "@name:" + name + "@id: " + id);
                        break;
                    case GEOFENCE_DWELL:
                        UnityPlayer.UnitySendMessage(UnityBridge.UnityMessageHandler, "ProximityEventReceiver", "@action:GEOFENCE_DWELL" + "@name:" + name + "@id:" + id);
                        break;
                    case GEOFENCE_EXIT:
                        UnityPlayer.UnitySendMessage(UnityBridge.UnityMessageHandler, "ProximityEventReceiver", "@action:GEOFENCE_EXIT" + "@name:" + name + "@id:" + id);
                        break;
                    case BLE_ENTER:
                        UnityPlayer.UnitySendMessage(UnityBridge.UnityMessageHandler, "ProximityEventReceiver", "@action:BLE_ENTER" + "@name:" + name + "@id:" + id);
                        break;
                    case BLE_HOVER:
                        UnityPlayer.UnitySendMessage(UnityBridge.UnityMessageHandler, "ProximityEventReceiver", "@action:BLE_HOVER" + "@name:" + name + "@id:" + id);
                        break;
                    case BLE_DWELL:
                        UnityPlayer.UnitySendMessage(UnityBridge.UnityMessageHandler, "ProximityEventReceiver", "@action:BLE_DWELL" + "@name:" + name + "@id:" + id);
                        break;
                    case BLE_EXIT:
                        UnityPlayer.UnitySendMessage(UnityBridge.UnityMessageHandler, "ProximityEventReceiver", "@action:BLE_EXIT" + "@name:" + name + "@id:" + id);
                        break;
                    case EDY_ENTER:
                        UnityPlayer.UnitySendMessage(UnityBridge.UnityMessageHandler, "ProximityEventReceiver", "@action:EDY_ENTER" + "@name:" + name + "@id:" + id);
                        break;
                    case EDY_HOVER:
                        UnityPlayer.UnitySendMessage(UnityBridge.UnityMessageHandler, "ProximityEventReceiver", "@action:EDY_HOVER " + "@name:" + name + "@id:" + id);
                        break;
                    case EDY_DWELL:
                        UnityPlayer.UnitySendMessage(UnityBridge.UnityMessageHandler, "ProximityEventReceiver", "@action:EDY_DWELL" + "@name:" + name + "@id:" + id);
                        break;
                    case EDY_EXIT:
                        UnityPlayer.UnitySendMessage(UnityBridge.UnityMessageHandler, "ProximityEventReceiver", "@action:EDY_EXIT" + "@name:" + name + "@id:" + id);
                        break;
                    case WIFI_ENTER:
                        UnityPlayer.UnitySendMessage(UnityBridge.UnityMessageHandler, "ProximityEventReceiver", "@action:WIFI_ENTER" + "@name:" + name + "@id:" + id);
                        break;
                    case WIFI_DWELL:
                        UnityPlayer.UnitySendMessage(UnityBridge.UnityMessageHandler, "ProximityEventReceiver", "@action:WIFI_DWELL" + "@name:" + name + "@id:" + id);
                        break;
                    case WIFI_EXIT:
                        UnityPlayer.UnitySendMessage(UnityBridge.UnityMessageHandler, "ProximityEventReceiver", "@action:WIFI_EXIT" + "@name:" + name + "@id:" + id);
                        break;
                    case WIFI_CONNECT:
                        UnityPlayer.UnitySendMessage(UnityBridge.UnityMessageHandler, "ProximityEventReceiver", "@action:WIFI_CONNECT" + "@name:" + name + "@id:" + id);
                        break;
                    case WIFI_DISCONNECT:
                        UnityPlayer.UnitySendMessage(UnityBridge.UnityMessageHandler, "ProximityEventReceiver", "@action:WIFI_DISCONNECT" + "@name:" + name + "@id:" + id);
                        break;
                    default:
                        /* ignore */
                        break;
                }
            } else if (ProximityService.PUSH_TO_VAULT_ACTION.equals(action)) {

                if (intent.hasExtra(ProximityService.PUSH_TO_VAULT_ACTION_EXTRA_ERROR)) {
                    String error = intent.getStringExtra(ProximityService.PUSH_TO_VAULT_ACTION_EXTRA_ERROR);
                    Log.e(TAG, error);
                    UnityPlayer.UnitySendMessage(UnityBridge.UnityMessageHandler, "ProximityEventReceiver", "@action:PUSH_TO_VAULT_ACTION_EXTRA_ERROR" + "@name:" + "@id:");
                } else {
                    UnityPlayer.UnitySendMessage(UnityBridge.UnityMessageHandler, "ProximityEventReceiver", "@action:PUSH_TO_VAULT_ACTION" + "@name:" + "@id:");
                }
            } else if (ProximityService.TRACK_TAP_ACTION.equals(action)) {
                if (intent.hasExtra(ProximityService.TRACK_TAP_ACTION_EXTRA_ERROR)) {
                    String error = intent.getStringExtra(ProximityService.TRACK_TAP_ACTION_EXTRA_ERROR);
                    Log.e(TAG, error);
                    UnityPlayer.UnitySendMessage(UnityBridge.UnityMessageHandler, "ProximityEventReceiver", "@action:TRACK_TAP_ACTION_EXTRA_ERROR" + "@name:" + "@id:");
                } else {
                    UnityPlayer.UnitySendMessage(UnityBridge.UnityMessageHandler, "ProximityEventReceiver", "@action:TRACK_TAP_ACTION" + "@name:" + "@id:");
                }
            }
        }
    }

    private class ProximityServiceConnection implements ServiceConnection {

        private ProximityServiceBinder binder;

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            binder = (ProximityServiceBinder) service;
            if (binderListener != null) {
                binderListener.onBinderConnected(binder);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            binder = null;
        }

        public void clear() {
            binder = null;
        }

        public ProximityServiceBinder getBinder() {
            return binder;
        }
    }

}

