## Changes in project

- Project now contains two new files, UnityBridge.mm and UnityBridge.h. UnityBridge serves as connection between swift source code and Unity project, written in obj-cpp, since Swift
can not be bridged directly with C#. It also uses <Tamoco/Tamoco-Swift.h> header.
- Trigger class now extends NSObject, otherwise it's not bridgeable by obj-cpp.
- "@objc" keyword is added before "public enum ActionType", so it can be exposed to obj-cpp.
- Added #import <CoreLocation/CoreLocation.h> and #import <UIKit/UIKit.h> to <Tamoco/Tamoco-Swift.h>. Note. This needs to be done every time when a new clean build is made and <Tamoco/Tamoco-Swift.h> is not immediately available after a clean start, you first must start to "build", so this header can be generated.

After all these changes are applied just build Tamoco framework like you normally would have. 


### Unity Editor INTEGRATION iOS
- You need to create a folder with path "Assets/Plugins/iOS".
- Then you drag the Tamoco.framework folder into the iOS folder, you also need to add the Tamoco.cs into a project. 

### USE IN PROJECT
Tamoco.cs exposes some of the methods contained in UnityBridge.mm, main methods are: 

- public static void TamocoInitialise(string Api_Key, string Api_secret){} ; this method calls Framework_Initialise, it's used to create TamocoObject in unmanaged(native) code

- public static bool TamocoSetCustomID(string id){} ; this method calls framework_SetCustomId, it's used to set optional parameter "customId" of Tamoco Object 

- public static void TamocoSetCallback(CallbackType type, TriggerCallback callback = null){} ; this method is used to set a unity delegate as the method that will subscribe to specific closure in 
native code. CallbackType is public enum located in Tamoco.cs it is used to determine which OnAction method we want to subscribe to, CallbackType.BeaconCallback is tied to "onCustomBeaconAction"
CallbackType.WifiCallback is tied to "onCustomWIFIAction" etc. TriggerCallback is Unity delegate located in Tamoco.cs, declared as "public delegate void TriggerCallback(ActionType type, string ID, string name)".
Note. Currently only triggers.id and trigger.name is passed to unity. Here's an example of using this method:

Tamoco.TriggerCallback _CallbackHolder;

void func(){
	_CallbackHolder = new Tamoco.TriggerCallback(CallbackFunc);
	//Unsets current delegate 
	Tamoco.TamocoSetCallback(CallbackType.BeaconCallback);
	//Sets new delegate
	Tamoco.TamocoSetCallback(CallbackType.BeaconCallback, _CallbackHolder);
}

[MonoPInvokeCallback(typeof(Tamoco.TriggerCallback))]
public static void CallbackFunc(Tamoco.ActionType, string ID, string name){
	Debug.Log("Unity Delegate");
}

Important. These methods will only work on iOS device, so you can not test them in the editor mode

## Building 
When you hit build in Unity it will generate xcode project, you must open it and go with a standard Tamoco configuration listed below

## Project Configuration
Your app project will require the following configuration settings to function properly with the Tamoco SDK.

### EMBEDDED CONTENT
If you are using Obj C project, then you need to go to Build Settings of your target and under "Build Options" set "Embedded Content Contains Swift Code" to YES

### DISABLE BITCODE
Bitcode is an optional XCode compilation setting that optimises your app for distribution based on a specific device.

The Tamoco SDK is not currently compatible with Bitcode. As a result, youâ€™ll need to disable Bitcode support within your app. To disable Bitcode support, go to Project > Build Settings > Build Options and change the â€œEnable Bitcodeâ€ setting to â€˜Noâ€™

### LOCATION PERMISSION DESCRIPTION
As a proximity SDK, the Tamoco client always requires location permissions. In iOS 8 and above, you must include an NSLocationAlwaysUsageDescription string entry within your appâ€™s info.plist. The string entered here will appear within the permissions dialogue that is presented to the user when the app requests location permissions. This description should provide a clear explanation and value for why the app requires location information.

### BACKGROUND MODES
You need to enable background modes for Location updates, otherwise you will get an runtime error similar to this:

	Terminating app due to uncaught exception 'NSInternalInconsistencyException', reason: 'Invalid parameter not satisfying: !stayUp || CLClientIsBackgroundable(internal->fClient)'

Go to your target and select "Capabilities" then enable "Background Modes" and thick:  

- Location updates
- Uses Bluetooth LE accessories
- Background fetch



## Unity Editor Integration Android 
* Set up a folder with a path ‘Assets/Plugins/Android’ 
* The next step is to upload the following files onto a created folder: play-services-basement-8.4.0.aar, play-services-location-8.4.0.aar, support-v4-23.4.0.aar, mainTemplate.gradle and the build unity project with UnityBridge( it is located in the repo, path: tamoco-unity-sdk / UnityBridge Android / Source / tamocounity / ) 
* Subsequently, add Tamoco.cs and TamocoGameObject.prefab( both in the repo, path: tamoco-unity-sdk / UnityBridge Android / UnityAsset / Plugins / ), to the project 
* In order to use the plugin move TamocoGameObject.prefab to the scene and more specifically a single copy of it.  


## Use In Project 
* Tamoco.cs initiaties on its own in the ‘Start’ Method, the only thing to be done here is to move TamocoGameObject.prefab to a scene.    
* Tamoco.cs has a few basic methods of calling to the tamoco-sdk. Their main functions are to set the downloads keys and IDs. It is possible to access those by using -  	Tamoco.Instance.MethodName  
* The second part of the file are the static events Action<string, string> which are used to handle an event that is sent from Tamoco-sdk. There are two strings which are sent here - name and ID of an event. To each event we can assign methods which will take those strings as parameters(it is possible to assign a few methods to a single action). What is vital here, is to ensure that each method in a given action, which becomes unavailable, needs to be unassigned. 

## Building 
In order to build a project you have to:


* Go to file -> build settings  
* Set Android as environment 
* Change Build system from  “Internal” to Gradle(new), select ‘export project’ 
* Go to ‘Player settings’ -> in the tab ‘Other Settings’ set minimal api to version 18 and maximal to version 25   
* Change the backend scripting to ‘Mono’ 
* Press ‘build’, after a compilation is finished Unity will return an Android project which has to be opened in Android Studio.  
* If needed, synchronise ‘gradle’  
* Find option to ‘create apk’ in ‘build’