//
//  AppDelegate.h
//  TamocoObjCTest
//
//  Created by Sašo Sečnjak on 13/05/16.
//  Copyright © 2016 Inova. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

