//
//  main.m
//  TamocoObjCTest
//
//  Created by Sašo Sečnjak on 13/05/16.
//  Copyright © 2016 Inova. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
