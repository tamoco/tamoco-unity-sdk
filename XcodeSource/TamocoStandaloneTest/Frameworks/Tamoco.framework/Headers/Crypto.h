//
//  Crypto.h
//  Tamoco
//
//  Created by Sašo Sečnjak on 12/05/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Crypto : NSObject

+(NSData *)hmacForKeyAndData:(NSString *)key data:(NSData *)data;

@end
