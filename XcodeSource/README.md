![Tamoco](http://tamo.co/wp-content/themes/tamoco/images/Tamoco_logo.png)

Tamoco project contains Framework with example apps.

## Content
* TamocoTestApp
	* It's an example app, how to use the framework into the Swift environment.
* TamocoObjCTest
	* It's an example app, how to include the framework into the Objective C environment.
* Tamoco
	* This is a Framework and its [documentation](Tamoco/README.md).