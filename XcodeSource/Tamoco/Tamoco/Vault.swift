//
//  Vault.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 08/08/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

open class Vault: NSObject {
    
    fileprivate var impl: VaultImplementation!
    
    /// Initializes the object.
    init(impl: VaultImplementation) {
        super.init()
        self.impl = impl
    }
    
    /// This validates the key and push it to the server.
    ///
    /// - Parameters:
    ///     - key: Key that will be validated and pushed to the server.
    ///     - finished: Callback when pushing is finished. If push failes `VaultError.PushFailed` is returned in parameter.
    open func push(_ key: String, finished: ((_ exception: VaultError) -> Void)?) {
        impl.pushKey(key, finished: finished)
    }
    
    /// This validates the key and push it to the server.
    ///
    /// - Parameters:
    ///     - key: Key that will be validated and pushed to the server.
    ///     - finished: Callback when pushing is finished with exception integer and key that was used. If push failes `VaultError.PushFailed` is returned in exception parameter.
    open func push(_ key: String, finished: ((_ exception: Int, _ key: String?) -> Void)?) {
        impl.pushKey(key) { (exception) in
            let (exceptionInt, exceptionKey) = exception.toTuple()
            finished?(exceptionInt, exceptionKey)
        }
    }
}
