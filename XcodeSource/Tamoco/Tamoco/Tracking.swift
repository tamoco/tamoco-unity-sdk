//
//  Track.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 26/09/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

open class Tracking: NSObject {
    
    fileprivate var impl: TrackingImplementation!
    
    /// Initializes the object.
    init(impl: TrackingImplementation) {
        super.init()
        self.impl = impl
    }
    
    /// This tracks the scan on the server.
    ///
    /// - Parameters:
    ///     - code: The value that is scanned.
    ///     - variant: Variant of the scan.
    open func tap(_ code: String, variant: String, completion: @escaping (Bool) -> Void) {
        impl.tap(code, variant: variant, completion: completion)
    }
}
