//
//  TimerController.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 26/08/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

class TimerController {
    
    var onTimeInterval: (() -> Void)?
    
    fileprivate var updateTimer: Timer?
    static fileprivate let updateDataInterval: TimeInterval = 5
    
    /// This method is used to clear every strong reference or similar, so this instance can be released.
    func clear() {
        stopUpdateTimer()
    }
    
    func startUpdateTimer() {
        updateTimer?.invalidate()
        updateTimer = Timer.scheduledTimer(timeInterval: TimerController.updateDataInterval, target: self, selector: #selector(TimerController.updateInterval), userInfo: nil, repeats: true)
    }
    
    func stopUpdateTimer() {
        updateTimer?.invalidate()
        updateTimer = nil
    }
    
    @objc func updateInterval() {
        onTimeInterval?()
    }
}
