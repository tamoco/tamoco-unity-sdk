//
//  ConfigFileConfiguration.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 29/04/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

private func loadFrom(_ path: String) -> Dictionary<String, Any> {
    let defaults = (NSDictionary(contentsOfFile: path) as? Dictionary<String, Any>) ?? Dictionary<String, Any>()
    return defaults
}

private func castTo<T>(_ obj: Any?, fallback: T) -> T {
    return obj as? T ?? fallback
}

private func getObj<T>(_ key: String, initConfig: Dictionary<String, Any>?, dictionary: Dictionary<String, Any>, fallbackDictionary: Dictionary<String, Any>, fallbackValue: T) -> T {
    return castTo(initConfig?[key] as AnyObject?, fallback: castTo(dictionary[key], fallback: castTo(fallbackDictionary[key], fallback: fallbackValue)))
}

struct ConfigFileConfiguration {
    
    let enableGeofenceRanging: Bool
    let enableBeaconRanging: Bool
    let enableWifiRanging: Bool
    let enableEddystoneRanging: Bool
    
    let triggersUpdateTime: TimeInterval
    
    let regionMonitoringAccuracy: Int
    let wifiMonitoringAccuracy: Int
    
    let trackQueueInterval: Int
    
    let delegateNotifications: Bool
    let delegateLocalNotifications: Bool
    
    let localNotificationSound: String
    
    let enableDebugLocalNotifications: Bool
    
    init() {
        // Path of the config file in library. We can assume that this always exists
        let fallbackPath = Bundle.tamocoBundle().path(forResource: "TamocoConfigDefault", ofType: "plist") ?? ""
        
        // We try to get path of the config file from the app, otherwise we will just use the fallback one
        let path = Bundle.main.path(forResource: "TamocoConfig", ofType: "plist") ?? fallbackPath
        
        // Configuration set while initializing Library
        let initConfig = TamocoImplementation.config
        
        // Lets load the file from the app
        let defaults = loadFrom(path)
        
        // And also a fallback file
        let fallback = loadFrom(fallbackPath)
        
        enableGeofenceRanging = getObj("ENABLE_GEOFENCE_RANGING", initConfig: initConfig, dictionary: defaults, fallbackDictionary: fallback, fallbackValue: false)
        enableBeaconRanging = getObj("ENABLE_BEACON_RANGING", initConfig: initConfig, dictionary: defaults, fallbackDictionary: fallback, fallbackValue: false)
        enableWifiRanging = getObj("ENABLE_WIFI_RANGING", initConfig: initConfig, dictionary: defaults, fallbackDictionary: fallback, fallbackValue: false)
        enableEddystoneRanging = getObj("ENABLE_EDDYSTONE_RANGING", initConfig: initConfig, dictionary: defaults, fallbackDictionary: fallback, fallbackValue: false)
        
        triggersUpdateTime = getObj("TRIGGERS_UPDATE_TIME", initConfig: initConfig, dictionary: defaults, fallbackDictionary: fallback, fallbackValue: 0)
        
        regionMonitoringAccuracy = getObj("REGION_MONITORING_ACCURACY", initConfig: initConfig, dictionary: defaults, fallbackDictionary: fallback, fallbackValue: 0)
        wifiMonitoringAccuracy = getObj("WIFI_MONITORING_ACCURACY", initConfig: initConfig, dictionary: defaults, fallbackDictionary: fallback, fallbackValue: 0)
        
        trackQueueInterval = getObj("TRACK_QUEUE_INTERVAL", initConfig: initConfig, dictionary: defaults, fallbackDictionary: fallback, fallbackValue: 0)
        
        delegateNotifications = getObj("DELEGATE_NOTIFICATIONS", initConfig: initConfig, dictionary: defaults, fallbackDictionary: fallback, fallbackValue: false)
        delegateLocalNotifications = getObj("DELEGATE_LOCAL_NOTIFICATIONS", initConfig: initConfig, dictionary: defaults, fallbackDictionary: fallback, fallbackValue: false)
        
        localNotificationSound = getObj("LOCAL_NOTIFICATION_SOUND", initConfig: initConfig, dictionary: defaults, fallbackDictionary: fallback, fallbackValue: "")
        
        enableDebugLocalNotifications = getObj("ENABLE_DEBUG_LOCAL_NOTIFICATIONS", initConfig: initConfig, dictionary: defaults, fallbackDictionary: fallback, fallbackValue: false)
    }
    
    
    
}
