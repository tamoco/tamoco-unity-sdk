//
//  ConfigurationService.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 20/04/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

struct ConfigurationService {
    static let config: ConfigFileConfiguration = ConfigFileConfiguration()
    static let monitor: MonitorServiceConfiguration = MonitorServiceConfiguration()
    static let defaults: DefaultsServiceConfiguration = DefaultsServiceConfiguration()
}