//
//  DefaultsServiceConfiguration.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 05/05/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation
import CoreLocation

let fallbackLatitude: CLLocationDegrees = 46.550689
let fallbackLongitude: CLLocationDegrees = 15.643618

struct DefaultsServiceConfiguration {

    let fallbackLocation: CLLocation = CLLocation(latitude: fallbackLatitude, longitude: fallbackLongitude)
    let fallbackRadius: Double = 100
    let triggersServiceRetryTimeout: Double = 300
}
