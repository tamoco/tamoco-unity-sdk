//
//  MonitorServiceConfiguration.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 25/04/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

struct MonitorServiceConfiguration {
    
    let wifiDWELLTimeout: TimeInterval = 300
    let beaconDWELLTimeout: TimeInterval = 60
    let geofenceDWELLTimeout: TimeInterval = 300
    let beaconHOVERTimeout: TimeInterval = 30
}
