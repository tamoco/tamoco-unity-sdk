//
//  VaultImplementation.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 08/08/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

class VaultImplementation {
    
    fileprivate let hexadecimalRegex: String = "[0-9A-F]+"
    fileprivate let acceptedLength: Int = 40
    
    func pushKey(_ key: String, finished: ((_ exception: VaultError) -> Void)?) {
        let key = key.uppercased()
        
        guard key.characters.count == acceptedLength else {
            finished?(VaultError.nonValidLength(key: key))
            return
        }
        
        guard NSPredicate(format:"SELF MATCHES %@", hexadecimalRegex).evaluate(with: key) else {
            finished?(VaultError.nonValidFormat(key: key))
            return
        }
        
        let data = key.dataFromHexadecimalString()
        
        guard data?.count == (acceptedLength / 2) else {
            finished?(VaultError.nonValidDataLength(key: key))
            return
        }
        
        if let _ = data {
            DataInterface.vault.put(key) { (successData, failData, response, error) in
                if let _ = error {
                    finished?(VaultError.pushFailed(key: key))
                } else {
                    finished?(VaultError.noError)
                }
            }
        } else {
            finished?(VaultError.cantConvertToData(key: key))
        }
    }
}
