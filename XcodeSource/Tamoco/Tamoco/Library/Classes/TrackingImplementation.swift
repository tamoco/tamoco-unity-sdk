//
//  TrackImplementation.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 26/09/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation
import CoreLocation

class TrackingImplementation {
    
    fileprivate weak var tamoco: TamocoImplementation!
    
    init(tamoco: TamocoImplementation) {
        self.tamoco = tamoco
    }
    
    func tap(_ code: String, variant: String, completion: @escaping (Bool) -> Void) {
        let location: CLLocation!
        
        /// Let's use the known location if we have one, otherwise use invalid latitude/longitude
        if let loc = tamoco.locationManager.manager.location {
            location = loc
        } else {
            log(LogLevel.warning, message: "Current location is not found, therefore invalid is used.")
            location = CLLocation(latitude: kCLLocationCoordinate2DInvalid.latitude, longitude: kCLLocationCoordinate2DInvalid.longitude)
        }
        
        let track = ManualTrack.initializeWith(code, variant: variant, location: location)
        DataInterface.evt.tap(track) { (successData, failData, response, error) in
            if let error = error {
                log(LogLevel.warning, message: "Tap was not successfully sent to the server: \(error)")
            } else {
                log(LogLevel.info, message: "Tap was successfully sent to the server!")
            }
            
            completion(error == nil)
        }
    }
}
