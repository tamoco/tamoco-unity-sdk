//
//  TamocoImplementation.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 20/04/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation
import CoreLocation

// MARK: Configuration
private var initialConfig: Dictionary<String, AnyObject>?

// MARK: Application Controller
private var appController: ApplicationController!

// MARK: Application Controller
private var settings: Settings?

// MARK: Implementation
class TamocoImplementation {
    
    // MARK: Services
    var locationManager: LocationManager!
    var bluetoothManager: BluetoothManager!
    var detailedLocationManager: LocationManager!
    fileprivate var backendServiceController: BackendServiceController!
    
    // MARK: Properties
    var beacons: Set<BeaconTrigger>? {
        return backendServiceController.beacons
    }
    
    var geofences: Set<GeoFenceTrigger>? {
        return backendServiceController.geofences
    }
    
    var wifis: Set<WifiTrigger>? {
        return backendServiceController.wifis
    }
    
    var eddystones: Set<EddystoneTrigger>? {
        return backendServiceController.eddystones
    }
    
    var currentBeacons: Set<BeaconTrigger>? {
        return beaconMonitor?.currentTriggers as? Set<BeaconTrigger>
    }
    
    var currentGeofences: Set<GeoFenceTrigger>? {
        return geofenceMonitor?.currentTriggers as? Set<GeoFenceTrigger>
    }
    
    var currentWifis: Set<WifiTrigger>? {
        return wifiMonitor?.currentTriggers as? Set<WifiTrigger>
    }
    
    var currentEddystones: Set<EddystoneTrigger>? {
        return eddystoneMonitor?.currentTriggers as? Set<EddystoneTrigger>
    }
    
    // MARK: Constants
    static let kOnTriggersFetched: String = "com.tamoco.onTriggersFetched"
    static let kOnCustomBeaconAction: String = "com.tamoco.onCustomBeaconAction"
    static let kOnCustomFenceAction: String = "com.tamoco.onCustomFenceAction"
    static let kOnCustomWIFIAction: String = "com.tamoco.onCustomWIFIAction"
    static let kOnCustomEddystoneAction: String = "com.tamoco.onCustomEddystoneAction"
    static let kNotificationActionTypeKey: String = "com.tamoco.notificationActionTypeKey"
    static let kNotificationTriggerKey: String = "com.tamoco.notificationTriggerKey"
    static let kMinSettingsElapsedTime: TimeInterval = 60 * 60 * 8 // 8 hours
    static let kRemoteLoggerFallbackKey: String = "x1nM44hL4PHpZp8XIsoHwcZcSwCNdAFg"
    
    // MARK: Scanners
    fileprivate var beaconMonitor: TriggerMonitor?
    fileprivate var geofenceMonitor: TriggerMonitor?
    fileprivate var wifiMonitor: TriggerMonitor?
    fileprivate var eddystoneMonitor: EddystoneMonitor?
    
    // MARK: General controllers
    fileprivate var timerController: TimerController!
    
    // MARK: Communicating with outside world
    weak var main: Tamoco!
    
    // MARK: Variables
    fileprivate var apiSecret: String
    fileprivate var apiKey: String
    var customId: String? {
        didSet {
             DataInterface.setCustomId(customId)
        }
    }
    
    var listenOnly: Bool = false  {
        didSet {
            
        }
    }
    
    fileprivate var appObserver: ApplicationStateObserver = ApplicationStateObserver()
    fileprivate var lastSettingsUpdate: TimeInterval = 0
    
    // MARK: Internal Class Variables
    class var config: Dictionary<String, Any>? {
        return initialConfig
    }
    
    class var applicationController: ApplicationController {
        return appController
    }
    
    class var applicationSettings: Settings? {
        return settings
    }
    
    // MARK: Object Lifecycle
    
    init(apiKey: String, apiSecret secret: String, config: Dictionary<String, AnyObject>?, owner: Tamoco) {
        if apiKey.isEmpty || secret.isEmpty {
            log(.info, message: "API key or secret is empty!")
        }
        
        self.main = owner
        self.apiSecret = secret
        self.apiKey = apiKey
        initialConfig = config
        timerController = TimerController()
        
        timerController.onTimeInterval = { [unowned self] in
            self.checkCurrentTriggers()
        }
        
        // Sets data for Data interface
        DataInterface.setApiSecret(self.apiSecret)
        DataInterface.setApiId(self.apiKey)
        DataInterface.setCustomId(self.customId)
        
        // Initializing Application Controller
        // In case of OS X implementation Create AppOSXApplicatonController and set it here accordingly
        appController = AppiOSApplicationController()
        
        // Initialize LocationManager
        // By default it should be kCLLocationAccuracyHundredMeters
        locationManager = LocationManager(desiredAccuracy: Double(ConfigurationService.config.regionMonitoringAccuracy) as CLLocationAccuracy)
        // By default it's good enough to be kCLLocationAccuracyKilometer
        detailedLocationManager = LocationManager(desiredAccuracy: Double(ConfigurationService.config.regionMonitoringAccuracy) as CLLocationAccuracy)
        detailedLocationManager.manager.distanceFilter = 10
        
        // Initialize BluetoothManager
        bluetoothManager = BluetoothManager()
        
        // Add all needed observers
        setLocationManagerObservers()
        
        // Initializing monitors
        if ConfigurationService.config.enableBeaconRanging {
            beaconMonitor = BeaconMonitor(tamoco: self)
        }
        
        if ConfigurationService.config.enableGeofenceRanging {
            geofenceMonitor = GeofenceMonitor(tamoco: self)
        }
        
        if ConfigurationService.config.enableWifiRanging {
            wifiMonitor = WifiMonitor(tamoco: self)
        }
        
        if ConfigurationService.config.enableEddystoneRanging {
            eddystoneMonitor = EddystoneMonitor(tamoco: self)
        }
        
        // Initialize Backend service controller
        backendServiceController = BackendServiceController(tamoco: self)
        
        // Listen for retrieved triggers
        backendServiceController.onBeaconTriggersFetched = beaconMonitor?.updateTriggers
        backendServiceController.onGeofenceTriggersFetched = geofenceMonitor?.updateTriggers
        backendServiceController.onWifiTriggersFetched = wifiMonitor?.updateTriggers
        backendServiceController.onEddystoneTriggersFetched = eddystoneMonitor?.updateTriggers
        backendServiceController.onCommunicationError = { [weak owner]error in
            owner?.onCommunicationError?(error)
        }
        
        // Assign notifiers
        beaconMonitor?.onAction = { [unowned self] type, trigger in
            if ConfigurationService.config.delegateNotifications && trigger is BeaconTrigger {
                self.main.onCustomBeaconAction?(type, trigger as! BeaconTrigger)
            }
            
            if let trigger = trigger as? BeaconTrigger {
                NotificationCenter.default.postCustomBeaconAction(type, trigger: trigger)
            }
            
            self.backendServiceController.track(self.locationManager.manager.location, type: type, trigger: trigger)
            
            if let trigger = trigger as? BeaconTrigger, ConfigurationService.config.enableDebugLocalNotifications {
                TamocoImplementation.applicationController.sendNotification(String(format: "%@ - %@, major: %d, minor: %d", type.toString(), trigger.beaconId ?? "?", trigger.major ?? "?", trigger.minor ?? "?"), delay: 0, userInfo: nil)
            }
        }
        
        geofenceMonitor?.onAction = { [unowned self] type, trigger in
            if ConfigurationService.config.delegateNotifications && trigger is GeoFenceTrigger {
                self.main.onCustomFenceAction?(type, trigger as! GeoFenceTrigger)
            }
            
            if let trigger = trigger as? GeoFenceTrigger {
                NotificationCenter.default.postCustomFenceAction(type, trigger: trigger)
            }
            
            self.backendServiceController.track(self.locationManager.manager.location, type: type, trigger: trigger)

            if let trigger = trigger as? GeoFenceTrigger, ConfigurationService.config.enableDebugLocalNotifications {
                TamocoImplementation.applicationController.sendNotification(String(format: "%@ - %@ | %@", type.toString(), trigger.id, trigger.name ?? "?"), delay: 0, userInfo: nil)
            }
        }
        
        wifiMonitor?.onAction = { [unowned self] type, trigger in
            if ConfigurationService.config.delegateNotifications && trigger is WifiTrigger {
                self.main.onCustomWIFIAction?(type, trigger as! WifiTrigger)
            }
            
            if let trigger = trigger as? WifiTrigger {
                NotificationCenter.default.postCustomWIFIAction(type, trigger: trigger)
            }
            
            self.backendServiceController.track(self.locationManager.manager.location, type: type, trigger: trigger)
            
            if let trigger = trigger as? WifiTrigger,  ConfigurationService.config.enableDebugLocalNotifications {
                TamocoImplementation.applicationController.sendNotification(String(format: "%@ - %@ | %@", type.toString(), trigger.id, trigger.ssid), delay: 0, userInfo: nil)
            }
        }
        
        eddystoneMonitor?.onAction = { [unowned self] type, trigger in
            if ConfigurationService.config.delegateNotifications && trigger is EddystoneTrigger {
                self.main.onCustomEddystoneAction?(type, trigger as! EddystoneTrigger)
            }
            
            if let trigger = trigger as? EddystoneTrigger {
                NotificationCenter.default.postCustomEddystoneAction(type, trigger: trigger)
            }
            
            self.backendServiceController.track(self.locationManager.manager.location, type: type, trigger: trigger)
            
            if let trigger = trigger as? EddystoneTrigger,  ConfigurationService.config.enableDebugLocalNotifications {
                TamocoImplementation.applicationController.sendNotification(String(format: "%@ - %@ | %@", type.toString(), trigger.id, trigger.name ?? "?"), delay: 0, userInfo: nil)
            }
        }
        
        // Start Fetching all triggers when Location manager is initialized
        locationManager.addManagerObserver(LMObserver(type: .didChangeAuthorizationStatus, callback: { [weak self] (observer) in
            log(.info, message: "Location Manager: DidChangeAuthorizationStatus")
            self?.backendServiceController.checkForUpdates()
        }))
        
        // Set Application observer to listen for foreground/background observers
        appObserver.observeBecomeActiveCallback = { [weak self] notification in
            self?.timerController.startUpdateTimer()
        }
        
        appObserver.observeBecomeInactiveCallback = { [weak self] notification in
            self?.timerController.stopUpdateTimer()
        }
        
        timerController.startUpdateTimer()
        
        log(.info, message: "Tamoco initialized")
    }
    
    deinit {
        beaconMonitor?.onAction = nil
        geofenceMonitor?.onAction = nil
        wifiMonitor?.onAction = nil
        eddystoneMonitor?.onAction = nil
        timerController.clear()
    }
    
    func isCurrent(_ trigger: Trigger) -> Bool {
        // Switch is used, otherwise conditional casting is needed
        switch trigger {
        case is BeaconTrigger:
            return currentBeacons?.contains(trigger as! BeaconTrigger) ?? false
            
        case is GeoFenceTrigger:
            return currentGeofences?.contains(trigger as! GeoFenceTrigger) ?? false
            
        case is WifiTrigger:
            return currentWifis?.contains(trigger as! WifiTrigger) ?? false
            
        case is EddystoneTrigger:
            return currentEddystones?.contains(trigger as! EddystoneTrigger) ?? false
            
        default:
            return false
        }
    }
    
    func performFetchWithCompletionHandler(_ completionHandler: @escaping (_ fetchResults: TamocoBackgroundFetchResult) -> Void) {
        // lets exploit this fetch timeframe to check if there is any change
        // We also need to check for any trigger updates
        updateAndCheckTriggers(completionHandler)
    }
    
    func didReceiveLocalNotificationUserInfo(_ userInfo: [AnyHashable: Any]?) {
        if let data = userInfo?[TrackResponseController.trackResponseDataKey] as? Data, let trackResponse = try? TrackResponseDeserializer.process(data), TamocoImplementation.applicationController.applicationState() == ApplicationState.inactive
        {
            // if we got tracked "ID" then we need to trigger click
            if let tracked = trackResponse.tracked {
                DataInterface.evt.click(tracked)
            }
            
            TrackResponseController.handleResponse(trackResponse)
        }
    }
    
    // MARK: Monitor check bumps
    
    fileprivate func checkCurrentTriggers() {
        log(.info, message: "check all current triggers")
        beaconMonitor?.checkCurrentTriggers()
        geofenceMonitor?.checkCurrentTriggers()
        wifiMonitor?.checkCurrentTriggers()
        eddystoneMonitor?.checkCurrentTriggers()
        
        // lets update settings if needed
        updateSettings()
    }
    
    fileprivate func updateAndCheckTriggers(_ completionHandler: ((_ fetchResults: TamocoBackgroundFetchResult) -> Void)? = nil) {
        checkCurrentTriggers()
        backendServiceController.checkForUpdates(completionHandler)
    }
    
    // MARK: Detailed location manager
    
    fileprivate func setLocationManagerObservers() {
        // Add observer for updating location
        detailedLocationManager.addManagerObserver(LMObserver(type: .didUpdateLocations, callback: { [weak self](observer) in
            self?.updateAndCheckTriggers()
        }))
        
        // Add observers for ranging beacons
        locationManager.addManagerObserver(LMObserver(type: .didRangeBeacons, callback: { [weak self](observer) in
            self?.updateAndCheckTriggers()
        }))
    }
    
    // MARK : Settings
    
    fileprivate func updateSettings() {
        let now = Date().timeIntervalSince1970
        
        if (lastSettingsUpdate + TamocoImplementation.kMinSettingsElapsedTime) <= now {
            lastSettingsUpdate = now
            DataInterface.evt.settings(locationManager.manager.location ?? ConfigurationService.defaults.fallbackLocation) { [weak self](successData, failData, response, error) in
                settings = successData
                
                // If error occurred, then we will retry in at least 15s
                if error != nil {
                    self?.lastSettingsUpdate = (now - TamocoImplementation.kMinSettingsElapsedTime) + 15
                }

                // If server didn't return any key, then don't set it up
                if let bugFenderKey = settings?.bugFenderKey, RemoteLogger.token() == nil {
                    RemoteLogger.setup(bugFenderKey)
                }
                
                RemoteLogger.setLogLevel(settings?.loglevel)
            }
        }
    }
    
    // MARK: External services
    
    func track() -> Tracking {
        return Tracking(impl: TrackingImplementation(tamoco: self))
    }
    
    func vault() -> Vault {
        return Vault(impl: VaultImplementation())
    }
}
