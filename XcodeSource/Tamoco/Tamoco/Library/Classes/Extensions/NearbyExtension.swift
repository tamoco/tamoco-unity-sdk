//
//  NearbyExtension.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 05/10/2016.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation
import CoreLocation

extension Nearby {
    
    class func initializeWith(_ location: CLLocation) -> Nearby {
        let track = Nearby()
        track.fillBase(location)
        
        return track
    }
}
