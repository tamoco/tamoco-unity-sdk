//
//  DeviceExtension.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 05/05/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation
import AdSupport
import CoreLocation

extension Device {
    
    func initialize() -> Device {
        deviceId = ASIdentifierManager.shared().advertisingIdentifier.uuidString
        lmt = ASIdentifierManager.shared().isAdvertisingTrackingEnabled
        ip = getIFAddress()
        deviceType = TamocoImplementation.applicationController.currentDeviceType().deviceType
        model = TamocoImplementation.applicationController.currentDeviceType().toString()
        locale = ((Locale.current as NSLocale).object(forKey: NSLocale.Key.countryCode) as? String) ?? ""
        osVersion = TamocoImplementation.applicationController.systemVersion()
        hwVersion = TamocoImplementation.applicationController.modelName()
        
        let screenSize = TamocoImplementation.applicationController.screenSize()
        screenWidth = screenSize.width
        screenHeight = screenSize.height
        batteryLevel = TamocoImplementation.applicationController.batteryLevel()
        bleState = Bluetooth.state == .poweredOn
        locationServiceState = CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways
        
        return self
    }
}
