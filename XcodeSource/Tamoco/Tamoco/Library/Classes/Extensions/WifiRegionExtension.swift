//
//  WifiRegionExtensions.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 25/04/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation
import SystemConfiguration.CaptiveNetwork

extension WifiRegion {
    
    func fillFrom(_ interfaceData: [AnyHashable: Any])  {
        ssid = interfaceData[kCNNetworkInfoKeySSID as String] as? String
        bssid = interfaceData[kCNNetworkInfoKeyBSSID as String] as? String
        data = interfaceData[kCNNetworkInfoKeySSIDData as String] as? String
    }
    
    func fillFrom(_ trigger: WifiTrigger) {
        ssid = trigger.ssid
        bssid = trigger.bssid
    }
}
