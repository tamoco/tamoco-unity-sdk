//
//  EventExtension.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 05/10/2016.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

extension Event {
    
    mutating func fillEvent() {
        eventId = UUID().uuidString
    }
}
