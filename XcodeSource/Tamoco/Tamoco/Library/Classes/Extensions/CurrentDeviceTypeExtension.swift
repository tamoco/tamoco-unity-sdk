//
//  CurrentDeviceTypeExtension.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 05/05/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

extension CurrentDeviceType {
    var deviceType: DeviceType {
        switch self {
        case .phone:
            return DeviceType.phone
            
        case .pad:
            return DeviceType.tablet
        }
    }
}
