//
//  BaseTrackExtension.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 26/09/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation
import CoreLocation

extension BaseTrack {
    
    func fillBase(_ location: CLLocation) {
        sdkTimestamp = Int(Date().timeIntervalSince1970)
        device = Device().initialize()
        app = App().initialize()
        geo = Geo(location: location.coordinate, horizontalAccuracy: location.horizontalAccuracy)
    }
}
