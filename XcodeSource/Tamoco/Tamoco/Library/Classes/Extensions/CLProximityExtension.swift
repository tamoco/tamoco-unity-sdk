//
//  CLProximityExtension.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 11/10/2016.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation
import CoreLocation

extension CLProximity {
    
    var proximityDescription: String {
        switch self {
        case .far:
            return "Far"
            
        case .immediate:
            return "Immediate"
            
        case .near:
            return "Near"
            
        default:
            return "Unknown"
        }
    }
}
