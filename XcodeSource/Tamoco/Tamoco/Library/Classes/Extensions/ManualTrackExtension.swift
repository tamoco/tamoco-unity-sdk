//
//  ManualTrackExtension.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 26/09/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation
import CoreLocation

extension ManualTrack {
    
    class func initializeWith(_ code: String, variant: String, location: CLLocation) -> ManualTrack {
        var track = ManualTrack()
        
        track.fillEvent()
        track.fillBase(location)
        track.trigger = ManualTrigger(code: code, variant: variant)
        
        return track
    }
}
