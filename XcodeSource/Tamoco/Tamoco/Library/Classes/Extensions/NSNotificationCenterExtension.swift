//
//  NSNotificationCenterExtension.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 25/07/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

extension NotificationCenter {
    
    func postCustomBeaconAction(_ type: ActionType, trigger: BeaconTrigger) {
        postCustom(TamocoOnCustomBeaconAction, type: type, trigger: trigger)
    }
    
    func postCustomFenceAction(_ type: ActionType, trigger: GeoFenceTrigger) {
        postCustom(TamocoOnCustomFenceAction, type: type, trigger: trigger)
    }
    
    func postCustomWIFIAction(_ type: ActionType, trigger: WifiTrigger) {
        postCustom(TamocoOnCustomWIFIAction, type: type, trigger: trigger)
    }
    
    func postCustomEddystoneAction(_ type: ActionType, trigger: EddystoneTrigger) {
        postCustom(TamocoOnCustomEddystoneAction, type: type, trigger: trigger)
    }
    
    fileprivate func postCustom(_ name: String, type: ActionType, trigger: Trigger) {
        post(name: Foundation.Notification.Name(rawValue: name), object: nil, userInfo: [TamocoNotificationActionTypeKey: type.rawValue, TamocoNotificationTriggerKey: trigger])
    }
}
