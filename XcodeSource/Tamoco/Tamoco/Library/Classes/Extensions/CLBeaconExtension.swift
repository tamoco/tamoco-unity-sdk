//
//  CLBeaconExtension.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 22/04/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation
import CoreLocation

extension CLBeacon {
    
    var proximityDescription: String {
        return proximity.proximityDescription
    }
    
    var proximityMeters: Double {
        switch proximity {
        case .far:
            return 1.0
            
            // Till around 0.75
        case .near:
            return 0.35
            
        case .immediate:
            return 0.03
            
        default:
            return -1
        }
    }
    
    var distance: Double {
        guard rssi < 0 else {
            return -1
        }
        
        let ratio = Double(rssi)*1.0/accuracy
        if ratio < 1 {
            return pow(ratio, 10)
        } else {
            return (0.89976) * pow(ratio, 7.7095) + 0.111
        }
    }
}
