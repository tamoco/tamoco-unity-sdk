//
//  AppExtension.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 05/05/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

extension App {
    
    func initialize() -> App {
        let appBundleShortVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
        let appBundleVersion = Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? ""
        let frameworkBundleShortVersion = Bundle.frameworkBundle().infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
        let frameworkBundleVersion = Bundle.frameworkBundle().infoDictionary?["CFBundleVersion"] as? String ?? ""
        
        bundleId = Bundle.main.infoDictionary?["CFBundleIdentifier"] as? String ?? ""
        appName = Bundle.main.infoDictionary?["CFBundleName"] as? String ?? ""
        appVersion = String(format: "%@ (%@)", appBundleShortVersion, appBundleVersion)
        sdkVersion = String(format: "%@ (%@)", frameworkBundleShortVersion, frameworkBundleVersion)
        customId = DataInterface.config.customId
        
        return self
    }
}
