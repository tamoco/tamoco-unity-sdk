//
//  StringExtension.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 29/04/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

extension String {
    
    func trimmedWhitespaces() -> String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    /// Create `NSData` from hexadecimal string representation
    ///
    /// This takes a hexadecimal representation and creates a `NSData` object. Note, if the string has any spaces or non-hex characters (e.g. starts with '<' and with a '>'), those are ignored and only hex characters are processed.
    ///
    /// - returns: Data represented by this hexadecimal string.
    func dataFromHexadecimalString() -> Data? {
        let data = NSMutableData(capacity: characters.count / 2)
        
        forEachHexChunk { (val) in
            var num = val
            data?.append(&num, length: 1)
        }
        
        return data as Data?
    }
    
    func bytesFromHexadecimalString() -> [UInt8] {
        var data: [UInt8] = []
        
        forEachHexChunk { (val) in
            data.append(val)
        }
        
        return data
    }
    
    // Function returns false if it was unable to convert string to hex chunks
    @discardableResult
    fileprivate func forEachHexChunk(_ chunk: (_ val: UInt8) -> Void) -> Bool {
        let regex = try! NSRegularExpression(pattern: "[0-9a-f]{1,2}", options: .caseInsensitive)
        
        // if there is any captured group, then we should initialize data
        if regex.numberOfCaptureGroups > 0 {
            return false
        }
        
        regex.enumerateMatches(in: self, options: [], range: NSMakeRange(0, characters.count)) { match, flags, stop in
            let byteString = (self as NSString).substring(with: match!.range)
            if let num = UInt8(byteString, radix: 16) {
                chunk(num)
            }
        }
        
        return true
    }
}
