//
//  ArrayExtension.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 10/10/2016.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

extension Sequence where Iterator.Element == UInt8 {
    
    func hexToString() -> String {
        var retval = ""
        for byte in self {
            var s = String(byte, radix:16, uppercase: false)
            if s.characters.count == 1 {
                s = "0" + s
            }
            retval += s
        }
        return retval
    }
}
