//
//  NSBundleExtension.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 09/05/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

private var _frameworkBundle: Bundle!
private var _tamocoBundle: Bundle!

extension Bundle {
    
    class func frameworkBundle() -> Bundle {
        if let frameworkBundle = _frameworkBundle {
            return frameworkBundle
        } else {
            _frameworkBundle = Bundle(for: Tamoco.self)
            return _frameworkBundle
        }
    }
    
    class func tamocoBundle() -> Bundle {
        if let tamocoBundle = _tamocoBundle {
            return tamocoBundle
        } else {
            _tamocoBundle = Bundle(url: Bundle.frameworkBundle().url(forResource: "Tamoco", withExtension: "bundle")!)
            return _tamocoBundle
        }
    }
}
