//
//  BeaconInfoExtension.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 11/10/2016.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation
import CoreLocation

extension BeaconInfo {
    
    /// Returns the distance to the Beacon in meters.
    func distance() -> Double {
        if RSSI == 0 {
            return -1
        }
        
        let ratio: Double = Double(RSSI) * 1.0 / Double(txPower - 35)
        let distance: Double
        if ratio < 1.0 {
            distance = pow(ratio, 10)
        } else {
            distance = 0.89976 * pow(ratio, 7.7095) + 0.111
        }
        
        return distance
    }
    
    /// Translated distance into the `CLProximity` to use the same logic as for iBeacons
    func proximity() -> CLProximity {
        let dist = distance()
        switch dist {
        case 0..<1:
            return .immediate
            
        case 1..<3:
            return .near
            
        case 3...Double.greatestFiniteMagnitude:
            return .far
            
        default:
            return .unknown
        }
    }
}
