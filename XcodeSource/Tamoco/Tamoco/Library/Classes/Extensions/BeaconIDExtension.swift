//
//  BeaconIDExtension.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 10/10/2016.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

extension BeaconID {
    
    var beaconIDString: String {
        return beaconID.hexToString()
    }
    
    var namespaceId: [UInt8]? {
        if beaconID.count < 10 {
            return nil
        }
        
        return Array(beaconID[0..<10])
    }
    
    var instanceId: [UInt8]? {
        if beaconID.count < 16 {
            return nil
        }
        
        return Array(beaconID[10..<16])
    }
}
