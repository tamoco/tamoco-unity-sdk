//
//  CLLocationManagerExtension.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 28/04/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation
import CoreLocation

extension CLLocationManager {
    
    /// Function checks and requsts an CoreLocation Always authorization, in case that authorization is already Always, then true is returned, else false
    ///
    /// - returns: true if authorization is already Always, else false
    func checkAndRequestAlwaysAuthorization() -> Bool {
        var ret: Bool = false
        
        if CLLocationManager.isMonitoringAvailable(for: CLBeaconRegion.self) || CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
            if CLLocationManager.authorizationStatus() != CLAuthorizationStatus.authorizedAlways {
                requestAlwaysAuthorization()
            } else {
                ret = true
            }
        }
        
        if #available(iOS 9.0, *) {
            allowsBackgroundLocationUpdates = true
        }
        
        return ret
    }
}
