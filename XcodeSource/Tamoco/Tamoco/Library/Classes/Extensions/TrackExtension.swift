//
//  Track.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 05/05/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation
import CoreLocation

extension Track {
    
    class func initializeWith(_ type: ActionType, trigger: Trigger, location: CLLocation) -> Track {
        var track = Track()
        
        track.fillEvent()
        track.fillBase(location)
        Track.fill(track, trigger: trigger)
        track.trigger.triggerType = type
        
        switch trigger {
        case is WifiTrigger:
            Track.fill(track, trigger: trigger as! WifiTrigger)
        
        case is EddystoneTrigger:
            Track.fill(track, trigger: trigger as! EddystoneTrigger)
            
        default:
            break
        }
        
        return track
    }
    
    fileprivate class func fill(_ track: Track, trigger: EddystoneTrigger) {
        track.trigger = trigger.originalObject
        track.trigger.instanceId = trigger.instanceId
        track.trigger.battery = trigger.battery
    }
    
    fileprivate class func fill(_ track: Track, trigger: WifiTrigger) {
        track.trigger = trigger.originalObject
        track.trigger.mac = trigger.bssid
    }
    
    fileprivate class func fill(_ track: Track, trigger: Trigger) {
        track.trigger = trigger.originalObject
    }
}
