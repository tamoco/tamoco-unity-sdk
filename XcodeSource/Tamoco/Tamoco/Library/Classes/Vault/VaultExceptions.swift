//
//  VaultExceptions.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 08/08/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

public enum VaultError: Error {
    
    /// No error occurred and push was successful
    case noError
    
    /// Key is not in HEX format
    case nonValidFormat(key: String)
    
    /// Key length is not 40-characters
    case nonValidLength(key: String)
    
    /// Key length after converting to bytes it is not 20-bytes
    case nonValidDataLength(key: String)
    
    /// Key can not be converted to bytes
    case cantConvertToData(key: String)
    
    /// Pushing to the server has failed
    case pushFailed(key: String)
}

extension VaultError {
    
    public func toTuple() -> (Int, String?) {
        switch self {
        case .noError:
            return (0, nil)
    
        case .nonValidFormat(let key):
            return (1, key)
            
        case .nonValidLength(let key):
            return (2, key)
            
        case .nonValidDataLength(let key):
            return (3, key)
            
        case .cantConvertToData(let key):
            return (4, key)
            
        case .pushFailed(let key):
            return (5, key)
        }
    }
}
