//
//  LocationManager.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 28/04/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation
import CoreLocation

class LocationManager: NSObject, CLLocationManagerDelegate {
    
    let manager: CLLocationManager = CLLocationManager()
    fileprivate var observers = [LMObserver]()
    
    /// This flag is set when observber is triggered for the first time
    fileprivate var triggeredDidChangeAuthorizationStatusFlag: Bool = false
    
    // Object Lifecycle
    
    required init(desiredAccuracy: CLLocationAccuracy) {
        super.init()
        manager.delegate = self
        manager.desiredAccuracy = desiredAccuracy
        
        // Checking current status, and then requesting for authorization if necessary
        let isAlreadyAlways = manager.checkAndRequestAlwaysAuthorization()
        if isAlreadyAlways {
            DispatchQueue.main.async(execute: { 
                self.triggerDidChangeAuthorizationStatus()
            })
        }
    }
    
    // MARK: Managing observers
    
    func addManagerObserver(_ observer: LMObserver) {
        observers.append(observer)
    }
    
    func removeManagerObserver(_ observer: LMObserver) {
        if let index = observers.index(of: observer) {
            observers.remove(at: index)
        }
    }
    
    func removeAllManagerObservers() {
        observers.removeAll()
    }
    
    // MARK: Conforming to Location Manager Delegate
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.authorizedAlways {
            triggerDidChangeAuthorizationStatus()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        log(.info, message: "LM didUpdateLocations")
        observers.filter { observer in observer.type == LocationManagerObserverTypes.didUpdateLocations }.forEach { (observer: LMObserver) in
            observer.manager = self
            
            observer.fire()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        log(.error, message: "monitoringDidFailForRegion (\(region?.identifier ?? "")): \(error)")
        observers.filter { observer in observer.type == LocationManagerObserverTypes.monitoringDidFailForRegion }.forEach { (observer: LMObserver) in
            observer.manager = self
            observer.region = region
            observer.error = error
            observer.state = nil
            
            observer.fire()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, rangingBeaconsDidFailFor region: CLBeaconRegion, withError error: Error) {
        log(.error, message: "rangingBeaconsDidFailForRegion (\(region.identifier)): \(error)")
        observers.filter { observer in observer.type == LocationManagerObserverTypes.rangingBeaconsDidFailForRegion }.forEach { (observer: LMObserver) in
            observer.manager = self
            observer.region = region
            observer.error = error
            observer.state = nil
            
            observer.fire()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion) {
        observers.filter { observer in observer.type == LocationManagerObserverTypes.didStartMonitoringForRegion }.forEach { (observer: LMObserver) in
            observer.manager = self
            observer.region = region
            
            observer.fire()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didDetermineState state: CLRegionState, for region: CLRegion) {
        observers.filter { observer in observer.type == LocationManagerObserverTypes.didDetermineState }.forEach { (observer: LMObserver) in
            observer.manager = self
            observer.region = region
            observer.state = state
            
            observer.fire()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        log(.info, message: "LM didEnterRegion")
        observers.filter { observer in observer.type == LocationManagerObserverTypes.didEnterRegion }.forEach { (observer: LMObserver) in
            observer.manager = self
            observer.region = region
            
            observer.fire()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        log(.info, message: "LM didExitRegion")
        observers.filter { observer in observer.type == LocationManagerObserverTypes.didExitRegion }.forEach { (observer: LMObserver) in
            observer.manager = self
            observer.region = region
            
            observer.fire()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {
        log(.info, message: "LM didRangeBeacons")
        observers.filter { observer in observer.type == LocationManagerObserverTypes.didRangeBeacons }.forEach { (observer: LMObserver) in
            observer.manager = self
            observer.region = region
            observer.beacons = beacons
            
            observer.fire()
        }
    }
    
    // MARK: Fixed Observer triggers
    
    fileprivate func triggerDidChangeAuthorizationStatus() {
        if !triggeredDidChangeAuthorizationStatusFlag {
            triggeredDidChangeAuthorizationStatusFlag = true
            observers.filter { observer in observer.type == LocationManagerObserverTypes.didChangeAuthorizationStatus }.forEach { (observer: LMObserver) in
                observer.manager = self
                
                observer.fire()
            }
        }
    }
}
