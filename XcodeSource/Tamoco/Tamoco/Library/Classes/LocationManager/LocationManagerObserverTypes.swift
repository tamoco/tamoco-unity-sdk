//
//  LocationManagerObserverTypes.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 28/04/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

enum LocationManagerObserverTypes {
    case monitoringDidFailForRegion
    case rangingBeaconsDidFailForRegion
    case didChangeAuthorizationStatus
    case didUpdateLocations
    case didStartMonitoringForRegion
    case didDetermineState
    case didEnterRegion
    case didExitRegion
    case didRangeBeacons
}
