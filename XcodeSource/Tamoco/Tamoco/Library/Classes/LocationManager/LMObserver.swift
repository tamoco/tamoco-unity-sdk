//
//  LMObserver.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 28/04/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation
import CoreLocation

class LMObserver: NSObject {
    
    typealias ObserverCallback = (_ observer: LMObserver) -> Void
    
    var manager: LocationManager!
    var region: CLRegion?
    var beacons: [CLBeacon]?
    var error: Error?
    var state: CLRegionState?
    
    fileprivate var callback: ObserverCallback?
    fileprivate(set) var type: LocationManagerObserverTypes?
    
    required init(type: LocationManagerObserverTypes, callback: @escaping ObserverCallback) {
        self.type = type
        self.callback = callback
    }
    
    func fire() {
        callback?(self)
    }
}
