//
//  RemoteLogger.m
//  Tamoco
//
//  Created by Sašo Sečnjak on 12/10/2016.
//  Copyright © 2016 Inova IT. All rights reserved.
//

#import "RemoteLogger.h"
#import <BugfenderSDK/BugfenderSDK.h>

static BFLogLevel logLevel = BFLogLevelError;

@implementation RemoteLogger

+ (void)setLogLevel:(NSString* _Nullable)level {
    if ([level isEqualToString:@"error"]) {
        logLevel = BFLogLevelError;
    } else if ([level isEqualToString:@"warning"]) {
        logLevel = BFLogLevelWarning;
    } else {
        logLevel = BFLogLevelDefault;
    }
}

+ (NSString* _Nullable) token {
    return [Bugfender token];
}

+ (void) setup:(NSString* _Nonnull)key {
    [Bugfender activateLogger:key];
    [Bugfender enableUIEventLogging];
}

+ (void) log:(NSString* _Nonnull)message {
    if (logLevel <= BFLogLevelDefault) {
        [Bugfender logLineNumber:0 method:nil file:nil level:BFLogLevelDefault tag:nil message:message];
    }
}

+ (void) logWarn:(NSString* _Nonnull)message {
    if (logLevel <= BFLogLevelWarning) {
        [Bugfender logLineNumber:0 method:nil file:nil level:BFLogLevelWarning tag:nil message:message];
    }
}

+ (void) logErr:(NSString* _Nonnull)message {
    if (logLevel <= BFLogLevelError) {
        [Bugfender logLineNumber:0 method:nil file:nil level:BFLogLevelError tag:nil message:message];
    }
}

@end
