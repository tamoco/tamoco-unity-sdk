//
//  Crypto.m
//  Tamoco
//
//  Created by Sašo Sečnjak on 12/05/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

#import "Crypto.h"
#include <CommonCrypto/CommonDigest.h>
#include <CommonCrypto/CommonHMAC.h>

@implementation Crypto

+(NSData *)hmacForKeyAndData:(NSString *)key data:(NSData *)data {
    const char *cKey  = [key cStringUsingEncoding:NSUTF8StringEncoding];
    
    unsigned char cHMAC[CC_SHA512_DIGEST_LENGTH];
    CCHmacContext context;
    CCHmacInit(&context, kCCHmacAlgSHA512, cKey, strlen(cKey));
    CCHmacUpdate(&context, data.bytes, data.length);
    CCHmacFinal(&context, cHMAC);
    
    return [[NSData alloc] initWithBytes:cHMAC length:sizeof(cHMAC)];
}

@end
