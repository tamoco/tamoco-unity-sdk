//
//  NetworkTools.h
//  Tamoco
//
//  Created by Sašo Sečnjak on 05/05/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkTools : NSObject

+ (NSString *)getIPAddress;

@end
