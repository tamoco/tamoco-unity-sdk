//
//  ApplicationStateObserver.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 10/08/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

class ApplicationStateObserver: NSObject {
    
    var observeBecomeActiveCallback: ((_ notification: Foundation.Notification) -> Void)?
    var observeBecomeInactiveCallback: ((_ notification: Foundation.Notification) -> Void)?
    
    override init() {
        super.init()
        
        NotificationCenter.default.addObserver(self, selector: #selector(ApplicationStateObserver.becomeActive(_:)), name: NSNotification.Name(rawValue: ApplicationWillEnterForegroundNotification), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ApplicationStateObserver.becomeInactive(_:)), name: NSNotification.Name(rawValue: ApplicationWillResignActiveNotification), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: -
    // MARK: NSNotificationCenter callbacks
    
    func becomeActive(_ notification: Foundation.Notification) {
        observeBecomeActiveCallback?(notification)
    }
    
    func becomeInactive(_ notification: Foundation.Notification) {
        observeBecomeInactiveCallback?(notification)
    }
}
