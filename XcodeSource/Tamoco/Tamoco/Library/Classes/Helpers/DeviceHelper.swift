//
//  DeviceHelper.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 05/05/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

func getIFAddress() -> String {
    return NetworkTools.getIPAddress()
}