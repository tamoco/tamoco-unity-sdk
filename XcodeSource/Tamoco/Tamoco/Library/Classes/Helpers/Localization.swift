//
//  Localization.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 12/05/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

func tr(_ key: String, value: String? = nil, comment: String) -> String {
    return NSLocalizedString(key, tableName: "proximity", bundle: Bundle.main, value: value ?? key, comment: comment)
}
