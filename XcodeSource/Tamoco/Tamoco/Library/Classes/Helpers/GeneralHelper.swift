//
//  GeneralHelper.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 12/05/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

typealias Byte = UInt8

enum ByteOrder {
    case bigEndian
    case littleEndian
}


func pack<T>(_ value: T, byteOrder: ByteOrder = .littleEndian) -> [Byte] {
    var v = value
    let valueByteArray = withUnsafePointer(to: &v) {
        Array(UnsafeBufferPointer(start: UnsafeRawPointer($0).assumingMemoryBound(to: Byte.self), count: MemoryLayout<T>.size))
    }
    return (byteOrder == .littleEndian) ? valueByteArray : valueByteArray.reversed()
}

func unpack<T>(_ valueByteArray: [Byte], toType type: T.Type, byteOrder: ByteOrder = .littleEndian) -> T {
    let bytes = (byteOrder == .littleEndian) ? valueByteArray : valueByteArray.reversed()
    return UnsafeRawPointer(bytes).assumingMemoryBound(to: T.self).pointee
}
