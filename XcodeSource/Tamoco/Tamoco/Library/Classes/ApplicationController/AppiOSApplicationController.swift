//
//  AppiOSApplicationController.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 28/04/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import UIKit

private var screenbound: CGRect = CGRect.zero

/// This is for setting anything iOS app releated, for example anything for the UIApplciation.
class AppiOSApplicationController: ApplicationController {
        
    init() {
        UIApplication.shared.setMinimumBackgroundFetchInterval(UIApplicationBackgroundFetchIntervalMinimum)
        
        NotificationCenter.default.addObserver(self, selector: #selector(AppiOSApplicationController.becomeActive(_:)), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AppiOSApplicationController.becomeInactive(_:)), name: NSNotification.Name.UIApplicationWillResignActive, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func sendNotification(_ alert: String, delay: TimeInterval, userInfo: [AnyHashable: Any]? = nil) {
        let notification = UILocalNotification()
        notification.alertBody = alert // text that will be displayed in the notification
        notification.fireDate = Date().addingTimeInterval(delay)
        notification.soundName = ConfigurationService.config.localNotificationSound
        notification.userInfo = userInfo
        
        UIApplication.shared.scheduleLocalNotification(notification)
    }
    
    func currentDeviceType() -> CurrentDeviceType {
        switch UIDevice.current.userInterfaceIdiom {
        case .phone, .unspecified, .tv, .carPlay:
            return CurrentDeviceType.phone
            
        case .pad:
            return CurrentDeviceType.pad
        }
    }
    
    func applicationState() -> ApplicationState {
        return UIApplication.shared.applicationState.tamocoApplicationState
    }
    
    func systemVersion() -> String {
        return UIDevice.current.systemVersion
    }
    
    func modelIdentifier() -> String {
        return UIDevice.current.modelIdentifier
    }
    
    func modelName() -> String {
        return UIDevice.current.modelName
    }
    
    func openURL(_ uri: String) {
        if let url = URL(string: uri) {
            DispatchQueue.main.async {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    func openSnippetInterstitial(_ snippet: String) {
        let controller = InterstitialWebViewController.instantiateFromStoryboard()
        controller.loadHTMLString(snippet)
        controller.show()
    }
    
    func screenSize() -> (width: Double, height: Double) {
        if screenbound.equalTo(CGRect.zero) {
            let bounds = UIScreen.main.bounds
            let scale: CGFloat = UIScreen.main.scale
            
            screenbound = CGRect(x: 0, y: 0, width: bounds.width * scale, height: bounds.height * scale)
        }
        
        let width = Double(screenbound.size.width)
        let height = Double(screenbound.size.height)
        
        return (width, height)
    }
    
    func batteryLevel() -> Int {
        return abs(Int(UIDevice.current.batteryLevel * 100))
    }
    
    // MARK: -
    // MARK: NSNotificationCenter callbacks
    
    @objc func becomeActive(_ notification: Foundation.Notification) {
        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: ApplicationWillEnterForegroundNotification), object: nil, userInfo: notification.userInfo)
    }
    
    @objc func becomeInactive(_ notification: Foundation.Notification) {
        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: ApplicationWillResignActiveNotification), object: nil, userInfo: notification.userInfo)
    }
}

/// This is meant to be splitted for iOS and OS X
public extension TamocoBackgroundFetchResult {
    
    /// This is a convenience method for translating `TamocoBackgroundFetchResult` to `UIBackgroundFetchResult`.
    public var uiBackgroundFetchResult: UIBackgroundFetchResult {
        switch self {
        case .failed:
            return UIBackgroundFetchResult.failed
            
        case .newData:
            return UIBackgroundFetchResult.newData
            
        case .noData:
            return UIBackgroundFetchResult.noData
        }
    }
}

extension UIApplicationState {
    
    var tamocoApplicationState: ApplicationState {
        switch self {
        case .active:
            return ApplicationState.active
            
        case .background:
            return ApplicationState.background
            
        case .inactive:
            return ApplicationState.inactive
        }
    }
}
