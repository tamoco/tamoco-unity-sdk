//
//  ApplicationController.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 28/04/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

let ApplicationWillEnterForegroundNotification: String = "ApplicationWillEnterForegroundNotification"
let ApplicationWillResignActiveNotification: String = "ApplicationWillResignActiveNotification"

protocol ApplicationController {
    /// Used for sending a local notifications.
    ///
    /// - Parameters:
    ///     - alert: Message that should be shown
    ///     - delay: delay in seconds that's used for showing the notification
    ///     - userInfo: data that is passed with the notification
    func sendNotification(_ alert: String, delay: TimeInterval, userInfo: [AnyHashable: Any]?)
    func currentDeviceType() -> CurrentDeviceType
    func applicationState() -> ApplicationState
    func systemVersion() -> String
    func modelIdentifier() -> String
    func modelName() -> String
    func openURL(_ uri: String)
    func openSnippetInterstitial(_ snippet: String)
    func screenSize() -> (width: Double, height: Double)
    func batteryLevel() -> Int
}
