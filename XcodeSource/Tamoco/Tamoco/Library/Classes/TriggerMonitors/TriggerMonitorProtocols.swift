//
//  TriggerScanner.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 20/04/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

protocol TriggerProtocol: Hashable {
    var id: String! { get set }
    var name: String? { get set }
    var dwellInterval: TimeInterval? { get set }
    var originalObject: Inventory! { get set }
}

protocol TriggerMonitor {
    var onAction: ((_ type: ActionType, _ trigger: Trigger) -> Void)? { get set }
    var tamoco: TamocoImplementation! { get }
    var currentTriggers: Set<Trigger> { get }
    
    init(tamoco: TamocoImplementation)
    func updateTriggers(_ triggers: Set<Trigger>)
    func checkCurrentTriggers()
}
