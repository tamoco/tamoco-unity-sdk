//
//  GeofenceScanner.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 20/04/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation
import CoreLocation

class GeofenceMonitor: NSObject, TriggerMonitor, CLLocationManagerDelegate {
    
    var onAction: ((_ type: ActionType, _ trigger: Trigger) -> Void)?
    weak var tamoco: TamocoImplementation!
    var currentTriggers: Set<Trigger> {
        var triggers = Set<GeoFenceTrigger>()
        for region in enteredRegions {
            if let trigger = regionTriggerMapper[region.identifier] {
                triggers.insert(trigger)
            }
        }
        
        return triggers
    }
    
    fileprivate var regionTriggerMapper = [String: GeoFenceTrigger]()
    fileprivate var enteredRegions = Set<CLRegion>()
    fileprivate var manager: CLLocationManager {
        return tamoco.locationManager.manager
    }
    
    fileprivate var observers = [LMObserver]()
    fileprivate var lastRegionChange = [String: TimeInterval]()
    
    required init(tamoco: TamocoImplementation) {
        super.init()
        self.tamoco = tamoco
        observers.append(LMObserver(type: .monitoringDidFailForRegion, callback: { (observer) in
            
        }))
        
        observers.append(LMObserver(type: .rangingBeaconsDidFailForRegion, callback: { (observer) in
            
        }))
        
        observers.append(LMObserver(type: .didStartMonitoringForRegion, callback: { [weak self](observer) in
            if let weakSelf = self, let region = observer.region as? CLCircularRegion {
                // If reagion is not entered yet, then request it's state
                if !weakSelf.enteredRegions.contains(region) {
                    // Based on some testing this apparently should be called with a slight delay, otherwise request fails
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) {
                        weakSelf.manager.requestState(for: region)
                    }
                }
            }
        }))
        
        observers.append(LMObserver(type: .didDetermineState, callback: { [weak self](observer) in
            if let weakSelf = self, let region = observer.region as? CLCircularRegion, let state = observer.state {
                // If region is inside, then just trigger action
                if state == .inside {
                    log(.info, message: "We are inside the region \(region.identifier)")
                    // Sanity check if we entered region while this response was sent
                    let alreadyEntered = weakSelf.enteredRegions.contains(region)
                    if let trigger = weakSelf.regionTriggerMapper[region.identifier], !alreadyEntered {
                        weakSelf.enteredRegions.insert(region)
                        log(.info, message: "Geofence Inside \(region.identifier)")
                        weakSelf.trackEnter(region.identifier)
                        weakSelf.onAction?(ActionType.geoFenceEnter, trigger)
                    }
                } else {
                    weakSelf.enteredRegions.remove(region)
                }
            }
        }))
        
        observers.append(LMObserver(type: .didEnterRegion, callback: { [weak self](observer) in
            if let weakSelf = self, let region = observer.region as? CLCircularRegion {
                log(.info, message: "Geofence Enter region: \(region.identifier)")
                
                let alreadyEntered = weakSelf.enteredRegions.contains(region)
                if let trigger = weakSelf.regionTriggerMapper[region.identifier], !alreadyEntered {
                    weakSelf.enteredRegions.insert(region)
                    log(.info, message: "Geofence Enter trigger: \(trigger.name.debugDescription)")
                    weakSelf.trackEnter(region.identifier)
                    weakSelf.onAction?(ActionType.geoFenceEnter, trigger)
                }
            }
        }))
        
        observers.append(LMObserver(type: .didExitRegion, callback: { [weak self](observer) in
            if let weakSelf = self, let region = observer.region as? CLCircularRegion {
                log(.info, message: "Geofence Exit region: \(region.identifier)")
                
                weakSelf.enteredRegions.remove(region)
                if let trigger = weakSelf.regionTriggerMapper[region.identifier] {
                    log(.info, message: "Geofence Exit trigger: \(trigger.name.debugDescription)")
                    weakSelf.trackExit(region.identifier)
                    weakSelf.onAction?(ActionType.geoFenceExit, trigger)
                }
            }
            }))
        
        // Add all observers to Location Manager
        observers.forEach { (observer) in
            tamoco.locationManager.addManagerObserver(observer)
        }
    }
    
    deinit {
        // Remove all observers to Location Manager
        observers.forEach { (observer) in
            if tamoco != nil {
                tamoco.locationManager.removeManagerObserver(observer)
            }
        }
    }
    
    func updateTriggers(_ triggers: Set<Trigger>) {
        // Let's get the regions that are currently monitored
        let monitoredRegions = Set<CLRegion>(manager.monitoredRegions.filter { region in region is CLCircularRegion })
        log(.info, message: "Geofence Regions: \(monitoredRegions.count)")
        
        // - We need to filter out those triggers that are missing ID (sanity check)
        // - then just map them to CLBeaconRegion
        // - just in case we remove all Regions with an empty identifier (if mapping failed)
        let newRegions = triggers.filter { trigger in trigger.id != nil }.map { (trigger: Trigger) -> CLRegion in
            if let trigger = trigger as? GeoFenceTrigger, let locationId = trigger.id {
                let region = CLCircularRegion(center: trigger.location, radius: trigger.distance, identifier: locationId)
                region.notifyOnEntry = true
                region.notifyOnExit = true
                regionTriggerMapper[region.identifier] = trigger
                
                return region
            } else {
                return CLBeaconRegion(proximityUUID: UUID(), identifier: "")
            }
        }
        
        // Get items to remove and remove them from monitoring
        let needToRemove = monitoredRegions.subtracting(newRegions)
        for region in needToRemove {
            log(.info, message: "Removing geofence region with identifier: \(region.identifier)")
            stopMonitoring(region)
            regionTriggerMapper.removeValue(forKey: region.identifier)
        }
        
        // Start to monitor all regions
        for region in newRegions {
            startMonitoring(region)
        }
    }
    
    func checkCurrentTriggers() {
        log(.info, message: "checkCurrentTriggers - Geofence")
        
        // Lets check if we need to report any DWELL
        let now = Date().timeIntervalSince1970
        
        // Go trough all reported regions and check their last change, then compare it with the DWELL timeout
        for (region, time) in lastRegionChange {
            if let trigger = regionTriggerMapper[region] {
                let dwell: TimeInterval = trigger.dwellInterval ?? ConfigurationService.monitor.beaconDWELLTimeout
                log(.info, message: "Geofence DWELL (now - time) > dwell: \((now - time)) > \(dwell)")
                if (now - time) > dwell {
                    log(.info, message: "Geofence DWELL: \(trigger.name.debugDescription)")
                    trackReported(region)
                    onAction?(.geoFenceDwell, trigger)
                }
            }
        }
    }
    
    // MARK: Managing monitoring beacons
    
    fileprivate func startMonitoring(_ region: CLRegion) {
        // Check if region is CLBeaconRegion (it should be)
        if let region = region as? CLCircularRegion {
            manager.startMonitoring(for: region)
        }
    }
    
    fileprivate func stopMonitoring(_ region: CLRegion) {
        // Check if region is CLBeaconRegion (it should be)
        if let region = region as? CLCircularRegion {
            enteredRegions.remove(region)
            manager.stopMonitoring(for: region)
        }
    }
    
    // MARK: Tracking enter/exit changes
    
    fileprivate func trackEnter(_ identifier: String) {
        lastRegionChange[identifier] = Date().timeIntervalSince1970
    }
    
    fileprivate func trackExit(_ identifier: String) {
        lastRegionChange.removeValue(forKey: identifier)
    }
    
    fileprivate func trackReported(_ identifier: String) {
        lastRegionChange[identifier] = Double.greatestFiniteMagnitude
    }
}
