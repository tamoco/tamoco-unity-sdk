//
//  WifiScanner.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 20/04/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation
import CoreLocation
import SystemConfiguration.CaptiveNetwork

class WifiMonitor: NSObject, TriggerMonitor {
    
    var onAction: ((_ type: ActionType, _ trigger: Trigger) -> Void)?
    weak var tamoco: TamocoImplementation!
    var currentTriggers: Set<Trigger> {
        var triggers = Set<WifiTrigger>()
        if let lastWIFI = lastWIFI, let trigger = getTrigger(forWifiRegion: lastWIFI) {
            triggers.insert(trigger)
        }
        
        return triggers
    }
    
    fileprivate var regionTriggerMapper = [String: WifiTrigger]()
    fileprivate var lastWIFI: WifiRegion?
    fileprivate var lastWIFIChange: TimeInterval = 0
    fileprivate var dwellReported: Bool = false
    fileprivate var monitoredWifis = Set<WifiRegion>()
    
    required init(tamoco: TamocoImplementation) {
        super.init()
        self.tamoco = tamoco
    }
    
    func updateTriggers(_ triggers: Set<Trigger>) {
        log(.info, message: "Updating WiFi triggers")
        monitoredWifis.removeAll()
        regionTriggerMapper.removeAll()
        
        let newRegions = triggers.filter { trigger in trigger.id != nil }.map({ (trigger: Trigger) -> WifiRegion in
            if let trigger = trigger as? WifiTrigger {
                let region = WifiRegion()
                region.fillFrom(trigger)
                set(trigger, forRegion: region)
                
                return region
            }
            
            return WifiRegion()
        }).filter { wifiRegion in wifiRegion.key != nil}
        
        for region in newRegions {
            monitoredWifis.insert(region)
        }
    }
    
    func checkCurrentTriggers() {
        checkForCurrentWiFi()
    }
    
    func checkForCurrentWiFi() {
        log(.info, message: "checkCurrentTriggers - WiFi")
        
        let currentWifi = getCurrentWifi()
        
        if let currentWifi = currentWifi {
            log(.info, message: "Current WiFi: \(currentWifi.key.debugDescription)")
        }
        let clearWifi = { [weak self] in
            self?.lastWIFI = nil
            self?.lastWIFIChange = 0
            self?.dwellReported = false
        }
        
        // If the current WiFi is monitorred
        if let currentWifi = currentWifi, let wifi = getMonitored(currentWifi, monitoredWifis: monitoredWifis) {
            // Get the trigger
            let trigger: WifiTrigger? = getTrigger(forWifiRegion: wifi)
            
            // Let set current wifi data to trigger
            trigger?.bssid = currentWifi.bssid
            
            // Get DWELL interval for this WiFi, else use the default one
            let dwell: TimeInterval = trigger?.dwellInterval ?? ConfigurationService.monitor.wifiDWELLTimeout
            
            switch wifi {
                
                // Wifi has chagned!
            case _ where lastWIFI != wifi:
                exit(lastWIFI)
                lastWIFI = wifi
                lastWIFIChange = Date().timeIntervalSince1970
                dwellReported = false
                if let key = wifi.key, let trigger = trigger {
                    log(.info, message: "WiFi Enter: \(key)")
                    onAction?(.wifiEnter, trigger)
                    log(.info, message: "WiFi Connect: \(key)")
                    onAction?(.wifiConnect, trigger)
                }
                
                // Wifi is the same as last time &
                // some time has elapsed since the connection &
                // DWELL was not reported yet!
            case _ where lastWIFI == wifi &&
                (Date().timeIntervalSince1970 - lastWIFIChange) > dwell &&
                !dwellReported:
                
                dwellReported = true
                if let key = wifi.key, let trigger = trigger {
                    log(.info, message: "WiFi DWELL: \(key)")
                    onAction?(.wifiDwell, trigger)
                }
                
            default:
                break
            }
            
        } else { // There is no WiFi
            switch lastWIFI {
                
                // Wifi is lost
            case _ where lastWIFI != nil:
                exit(lastWIFI)
                
            default:
                break
            }
            
            clearWifi()
        }
    }
    
    // MARK: Helpers
    
    fileprivate func exit(_ wifi: WifiRegion?) {
        if let wifi = wifi, let trigger = getTrigger(forWifiRegion: wifi) {
            log(.info, message: "WiFi Exit: \(wifi.key.debugDescription)")
            onAction?(.wifiExit, trigger)
            log(.info, message: "WiFi Disconnect: \(wifi.key.debugDescription)")
            onAction?(.wifiDisconnect, trigger)
        }
    }
    
    fileprivate func getCurrentWifi() -> WifiRegion? {
        if let interfaces = CNCopySupportedInterfaces() {
            if let interfacesArray = interfaces as [AnyObject] as? [String] {
                for interface in interfacesArray {
                    if let unsafeInterfaceData = CNCopyCurrentNetworkInfo(interface as CFString) {
                        let interfaceData = unsafeInterfaceData as! [AnyHashable: Any]
                        let wifi = WifiRegion()
                        wifi.fillFrom(interfaceData)
                        
                        return wifi
                    }
                }
            }
        }
        
        return nil
    }
    
    fileprivate func getMonitored(_ wifi: WifiRegion, monitoredWifis: Set<WifiRegion>) -> WifiRegion? {
        var monitored: WifiRegion? = nil
        
        // Find the right one, 
        // Check if wifi with the same SSID and BSSID exists
        // else check only for SSID to match
        for region in monitoredWifis {
            let ssidMatch = region.ssid == wifi.ssid
            let bssidMatch = region.bssid == wifi.bssid
            
            // it's a candidate, but we shouldn't break, lets try to find a region that also matches a BSSID
            if ssidMatch && monitored == nil {
                monitored = region
            }
            
            if ssidMatch && bssidMatch { // that's it
                monitored = region
                
                break
            }
        }
        
        return monitored
    }
    
    fileprivate func getTrigger(forWifiRegion region: WifiRegion) -> WifiTrigger? {
        if let key = region.key, let trigger = regionTriggerMapper[key] {
            return trigger
        }
        
        return nil
    }
    
    fileprivate func set(_ trigger: WifiTrigger, forRegion region: WifiRegion) {
        if let key = region.key {
            regionTriggerMapper[key] = trigger
        }
    }
}
