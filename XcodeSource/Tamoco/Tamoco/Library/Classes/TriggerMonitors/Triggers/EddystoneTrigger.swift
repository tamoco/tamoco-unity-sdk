//
//  EddystoneTrigger.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 05/10/2016.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

open class EddystoneTrigger: Trigger {
    
    /// Eddystone namespace ID
    open var namespaceId: String!
    
    /// Eddystone instance ID
    open var instanceId: String?
    
    /// Eddystone battery level
    var battery: UInt16?
    
    init(id: String, name: String? = nil, dwellInterval: TimeInterval?, hoverInterval: TimeInterval?, originalObject: Inventory, namespaceId: String, instanceId: String?) {
        super.init(id: id, name: name, dwellInterval: dwellInterval, hoverInterval: hoverInterval, originalObject: originalObject)
        self.namespaceId = namespaceId
        self.instanceId = instanceId
    }
}
