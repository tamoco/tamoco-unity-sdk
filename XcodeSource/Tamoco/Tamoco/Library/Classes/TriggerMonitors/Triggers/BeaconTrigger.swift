//
//  BeaconTrigger.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 28/04/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation
import CoreLocation

/// Specialized trigger which is used for Beacon triggers. Inherited from `Trigger`
open class BeaconTrigger: Trigger {
    
    /// Major version of an iBeacon.
    open var major: CLBeaconMajorValue?
    
    /// Minor version of an iBeacon.
    open var minor: CLBeaconMinorValue?
    
    /// iBeacon ID or better UUID of an iBeacon.
    open var beaconId: String!

    init(id: String, name: String? = nil, dwellInterval: TimeInterval?, hoverInterval: TimeInterval?, originalObject: Inventory, beaconId: String , major: CLBeaconMajorValue?, minor: CLBeaconMinorValue?) {
        super.init(id: id, name: name, dwellInterval: dwellInterval, hoverInterval: hoverInterval, originalObject: originalObject)
        self.beaconId = beaconId
        self.minor = minor
        self.major = major
    }
    
    /// Conforming to Hashable protocol
    open override var hashValue: Int {
        var hash = id
        
        if let major = major {
            hash = hash! + " maj:\(major)"
        }
        
        if let minor = minor {
            hash = hash! + " min:\(minor)"
        }
        
        return hash!.hashValue
    }
}
