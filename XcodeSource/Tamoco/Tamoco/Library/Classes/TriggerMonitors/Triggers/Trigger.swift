//
//  Trigger.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 21/04/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation
import CoreLocation

/// Comparing two Trigger objects. If at least one Trigger id is `nil`, that means that objects are not the same.
public func ==(lhs: Trigger, rhs: Trigger) -> Bool {
    guard lhs.id != nil || rhs.id != nil else  {
        return false
    }
    
    return lhs.hashValue == rhs.hashValue
}

/// It's a base class for all triggers. All objects that their id is `nil` returns -1 as hashValue
open class Trigger: NSObject, TriggerProtocol {
    /// ID of the trigger. This is populated with the value returned from the backend.
    open internal(set) var id: String!
    
    /// Name of the trigger. This is also populated with the value returned from the backend.
    open internal(set) var name: String?
    
    /// DWELL Interval (interval is used for reporting DWELL events). This is also populated with the returned value from the backend.
    open internal(set) var dwellInterval: TimeInterval?
    
    /// HOVER Interval (interval is used for reporting HOVER events). This is also populated with the returned value from the backend.
    open internal(set) var hoverInterval: TimeInterval?
    internal(set) var originalObject: Inventory!
    
    /// Geo location of the trigger.
    open var location: CLLocationCoordinate2D = kCLLocationCoordinate2DInvalid

    /// Conforming to Hashable protocol
    open override var hashValue: Int {
        return id.hashValue
    }
    
    init(id: String, name: String?, dwellInterval: TimeInterval?, hoverInterval: TimeInterval?, originalObject: Inventory) {
        self.id = id
        self.name = name
        self.dwellInterval = dwellInterval
        self.hoverInterval = hoverInterval
        self.originalObject = originalObject
    }
}
