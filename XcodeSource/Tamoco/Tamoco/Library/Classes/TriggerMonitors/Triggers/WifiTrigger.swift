//
//  WifiTrigger.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 05/05/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

/// Sepcialized trigger which is used for WiFi triggers. Inherited from `Trigger`
open class WifiTrigger: Trigger {
    /// SSID of the WiFi.
    open var ssid: String!
    open var bssid: String?
    
    init(id: String, name: String? = nil, dwellInterval: TimeInterval?, hoverInterval: TimeInterval?, originalObject: Inventory, ssid: String, bssid: String?) {
        super.init(id: id, name: name, dwellInterval: dwellInterval, hoverInterval: hoverInterval, originalObject: originalObject)
        self.ssid = ssid
        self.bssid = bssid
    }
}
