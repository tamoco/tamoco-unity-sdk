//
//  TriggerMapper.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 05/05/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation
import CoreLocation

class TriggerMapper {
    
    class func mapFrom(_ inventory: Inventory) -> Trigger {
        switch inventory.inventoryType {

        case .beacon where inventory.beaconId != nil:
            let beacon = BeaconTrigger(id: inventory.inventoryId, name: inventory.name, dwellInterval: inventory.dwellInterval, hoverInterval: inventory.hoverInterval, originalObject: inventory, beaconId: inventory.beaconId!, major: inventory.major, minor: inventory.minor)
            
            if let latitude = inventory.geo?.location.latitude, let longitude = inventory.geo?.location.longitude {
                beacon.location = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            }
            
            return beacon
            
        case .geoFence:
            return GeoFenceTrigger(id: inventory.inventoryId, name: inventory.name, dwellInterval: inventory.dwellInterval, hoverInterval: inventory.hoverInterval, originalObject: inventory, location: inventory.geo?.location ?? kCLLocationCoordinate2DInvalid, distance: inventory.radius ?? ConfigurationService.defaults.fallbackRadius)
            
        case .wiFi where inventory.ssid != nil:
            let wifi = WifiTrigger(id: inventory.inventoryId, name: inventory.name, dwellInterval: inventory.dwellInterval, hoverInterval: inventory.hoverInterval, originalObject: inventory, ssid: inventory.ssid!, bssid: inventory.mac)
            
            if let latitude = inventory.geo?.location.latitude, let longitude = inventory.geo?.location.longitude {
                wifi.location = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            }
            
            return wifi
            
        case .eddystone where inventory.namespaceId != nil:
            let eddy = EddystoneTrigger(id: inventory.inventoryId, name: inventory.name, dwellInterval: inventory.dwellInterval, hoverInterval: inventory.hoverInterval, originalObject: inventory, namespaceId: inventory.namespaceId!, instanceId: inventory.instanceId)
            
            if let latitude = inventory.geo?.location.latitude, let longitude = inventory.geo?.location.longitude {
                eddy.location = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            }
            
            return eddy
            
            // This should never happen!
        default:
            return Trigger(id: inventory.inventoryId, name: inventory.name, dwellInterval: inventory.dwellInterval, hoverInterval: inventory.hoverInterval, originalObject: inventory)
        }
    }
}
