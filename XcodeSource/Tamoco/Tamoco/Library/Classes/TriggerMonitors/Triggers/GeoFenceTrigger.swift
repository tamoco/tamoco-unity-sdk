//
//  GeoFenceTrigger.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 22/04/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation
import CoreLocation

/// Specialized Trigger which is used for GeoFence triggers. Inherited from `Trigger`
open class GeoFenceTrigger: Trigger {
    
    /// Distance used for regioning.
    open var distance: CLLocationDistance = CLLocationDistanceMax
    
    init(id: String, name: String? = nil, dwellInterval: TimeInterval?, hoverInterval: TimeInterval?, originalObject: Inventory, location: CLLocationCoordinate2D, distance: CLLocationDistance) {
        super.init(id: id, name: name, dwellInterval: dwellInterval, hoverInterval: hoverInterval, originalObject: originalObject)
        self.location = location
        self.distance = distance
    }
}
