//
//  EddystoneMonitor.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 05/10/2016.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation
import CoreLocation

class EddystoneMonitor: NSObject, TriggerMonitor, BeaconScannerDelegate {
    
    var onAction: ((_ type: ActionType, _ trigger: Trigger) -> Void)?
    weak var tamoco: TamocoImplementation!
    var currentTriggers: Set<Trigger> {
        var triggers = Set<EddystoneTrigger>()
        
        for (beaconId, _) in inRange {
            let beaconTriggers = getTriggers(beaconId)
            for trigger in beaconTriggers {
                triggers.insert(trigger)
            }
        }
        
        return triggers
    }
    
    var scanner: BeaconScanner!
    
    // Stones that should be monitored
    fileprivate var monitoredStones = Set<EddystoneFilter>()
    fileprivate var triggerMapper = [EddystoneFilter: EddystoneTrigger]()
    fileprivate var inRange:[BeaconID: BeaconInfo] = [:]
    
    fileprivate var lastStoneChange = [BeaconID: TimeInterval]()
    fileprivate var lastStoneHoverChange = [BeaconID: TimeInterval]()
    
    required init(tamoco: TamocoImplementation) {
        super.init()
        self.tamoco = tamoco
        
        scanner = BeaconScanner()
        scanner.delegate = self
        scanner.startScanning()
    }
    
    func updateTriggers(_ triggers: Set<Trigger>) {
        // We need to map triggers to filters
        let new = triggers.map { (trigger: Trigger) -> EddystoneFilter in
            if let trigger = trigger as? EddystoneTrigger {
                let filter = EddystoneFilter(namespaceUID: trigger.namespaceId, instanceID: trigger.instanceId)
                triggerMapper[filter] = trigger
                
                return filter
            }
            
            return EddystoneFilter(namespaceUID: "", instanceID: nil)
        }.filter { filter in filter.namespaceUID != "" }
        
        // Get items to remove and remove them from discovery
        let needToRemove = monitoredStones.subtracting(new)
        for filter in needToRemove {
            log(.info, message: "Removing Eddystone filter: \(filter.namespaceUID) | \(filter.instanceID ?? ""))")
            triggerMapper.removeValue(forKey: filter)
            monitoredStones.remove(filter)
        }
        
        // And start discovering new stones
        for filter in new {
            log(.info, message: "Adding Eddystone filter: \(filter.namespaceUID) | \(filter.instanceID ?? ""))")
            monitoredStones.insert(filter)
        }
    }
    
    func checkCurrentTriggers() {
        log(.info, message: "checkCurrentTriggers - Eddystones")
        
        // Lets check if we need to report any DWELL
        let now = Date().timeIntervalSince1970
        
        // Go trough all reported Stones and check their last change, then compare it with the DWELL timeout
        forEachChange(lastStoneChange, now: now, callback: checkForDwell)
        
        // Go trough all reported stones for hover
        forEachChange(lastStoneHoverChange, now: now, callback: checkForHover)
    }
    
    fileprivate func forEachChange(_ changeSet: [BeaconID: TimeInterval], now: TimeInterval, callback: (_ identifier: BeaconID, _ trigger: EddystoneTrigger, _ now: TimeInterval, _ time: TimeInterval) -> Void) {
        for (identifier, time) in changeSet {
            
            let triggers = getTriggers(identifier)
            for trigger in triggers {
                callback(identifier, trigger, now, time)
            }
        }
    }
    
    fileprivate func checkForDwell(_ identifier: BeaconID, trigger: EddystoneTrigger, now: TimeInterval, time: TimeInterval) {
        let dwell: TimeInterval = trigger.dwellInterval ?? ConfigurationService.monitor.beaconDWELLTimeout
        if (now - time) > dwell {
            log(.info, message: "Eddy DWELL: \(trigger.namespaceId) | \(trigger.instanceId ?? "NO_INSTANCE_ID")")
            trackReported(identifier)
            onAction?(.eddyDwell, trigger)
        }
    }
    
    fileprivate func checkForHover(_ identifier: BeaconID, trigger: EddystoneTrigger, now: TimeInterval, time: TimeInterval) {
        let hover: TimeInterval = trigger.hoverInterval ?? ConfigurationService.monitor.beaconHOVERTimeout
        if (now - time) > hover {
            log(.info, message: "Hover reported: \(trigger.namespaceId) | \(trigger.instanceId ?? "NO_INSTANCE_ID")")
            trackHoverReported(identifier)
            onAction?(ActionType.eddyHover, trigger)
        }
    }
    
    // MARK: Helpers
    
    /// Function checks `monitoredStones` for filters and checks if beaconID is a match of any of the filters.
    fileprivate func shouldMonitor(_ beaconID: BeaconID) -> Bool {
        for filter in monitoredStones {
            if isValid(beaconID, for: filter) {
                return true
            } else {
                continue
            }
        }
        
        return false
    }
    
    fileprivate func isValid(_ beaconID: BeaconID, for filter: EddystoneFilter) -> Bool {
        // Skip if there is not at least 10 bytes (there is no namespace ID)
        guard let namespaceId = beaconID.namespaceId else {
            return false
        }
        
        let filterNamespaceHex = filter.namespaceUID.bytesFromHexadecimalString()
        
        // Check for namespace ID
        if filterNamespaceHex == namespaceId {
            
            // if instance ID exists, then we need to validate it
            if let instanceID = filter.instanceID {
                // Skip if length is less then 16 bytes (there is no intance ID)
                guard let instanceId = beaconID.instanceId else {
                    return false
                }
                
                let filterInstanceHex = instanceID.bytesFromHexadecimalString()
                
                // if intance id is a match, then we found it, otherwise we didn't
                if filterInstanceHex == instanceId {
                    return true
                } else {
                    return false
                }
            } else {
                // Namespace ID is OK, and instance ID does not exists in filter, therefore we can say that this is a matching Eddystone
                return true
            }
        }
        
        return false
    }
    
    // MARK: Managing in range beacons
    
    fileprivate func checkForHover(_ proximity: CLProximity, trigger: EddystoneTrigger?, beaconID: BeaconID) {
        let time: TimeInterval? = lastStoneHoverChange[beaconID]
        let wantedProximity = proximity == .near || proximity == .immediate
        
        switch (wantedProximity, trigger, time) {
        case let (true, trigger?, nil):
            log(.info, message: "Entering Eddy Hover start timer (\(trigger.name.debugDescription)")
            trackHoverEnter(beaconID)
            
        case (true, _?, _?):
            break
            
        default:
            trackHoverExit(beaconID)
        }
    }
    
    fileprivate func isAlreadyInRange(_ beaconInfo: BeaconInfo) -> Bool {
        return inRange[beaconInfo.beaconID] != nil
    }
    
    fileprivate func addInRange(_ beaconInfo: BeaconInfo) {
        // If there is no namespace... then something is really wrong!
        guard beaconInfo.beaconID.namespaceId != nil else {
            return
        }
        
        let triggers = getTriggers(beaconInfo.beaconID)
        let proximity = beaconInfo.proximity()
        
        for trigger in triggers {
            // If beacon is not in range yet, then track it as Enter
            if !isAlreadyInRange(beaconInfo) {
                trackEnter(beaconInfo.beaconID)
                fill(trigger, beaconInfo: beaconInfo)
                onAction?(ActionType.eddyEnter, trigger)
            }
            
            // Check for HOVER
            checkForHover(proximity, trigger: trigger, beaconID: beaconInfo.beaconID)
        }
        
        inRange[beaconInfo.beaconID] = beaconInfo
    }
    
    fileprivate func removeFromRange(_ beaconInfo: BeaconInfo) {
        if isAlreadyInRange(beaconInfo) {
            let triggers = getTriggers(beaconInfo.beaconID)
            for trigger in triggers {
                trackExit(beaconInfo.beaconID)
                fill(trigger, beaconInfo: beaconInfo)
                onAction?(ActionType.eddyExit, trigger)
            }
            
            inRange.removeValue(forKey: beaconInfo.beaconID)
        }
    }
    
    fileprivate func getTriggers(_ beaconID: BeaconID) -> [EddystoneTrigger] {
        let triggers = triggerMapper.filter({ (filter, trigger) -> Bool in
            return isValid(beaconID, for: filter)
        }).map({ (filter, trigger) -> EddystoneTrigger in
            return trigger
        })
        
        return triggers
    }
    
    fileprivate func fill(_ trigger: EddystoneTrigger, beaconInfo: BeaconInfo) {
        let eddyTelemetry: EddystoneTelemetry?
        if let telemetry = beaconInfo.telemetry {
            eddyTelemetry = EddystoneTelemetry(data: telemetry)
        } else {
            eddyTelemetry = nil
        }
        
        // Namespace should be already set correctly
        //trigger.namespaceId = beaconInfo.beaconID.namespaceId?.hexToString() ?? trigger.namespaceId
        trigger.instanceId = beaconInfo.beaconID.instanceId?.hexToString()
        trigger.battery = eddyTelemetry?.telemetry.battery
    }
    
    // MARK: BeaconScannerDelegate
    
    func didFindBeacon(_ beaconScanner: BeaconScanner, beaconInfo: BeaconInfo) {
        if shouldMonitor(beaconInfo.beaconID) {
            addInRange(beaconInfo)
        }
    }
    
    func didLoseBeacon(_ beaconScanner: BeaconScanner, beaconInfo: BeaconInfo) {
        if shouldMonitor(beaconInfo.beaconID) {
            removeFromRange(beaconInfo)
        }
    }
    
    func didUpdateBeacon(_ beaconScanner: BeaconScanner, beaconInfo: BeaconInfo) {
        if shouldMonitor(beaconInfo.beaconID) {
            addInRange(beaconInfo)
        }
    }
    
    func didObserveURLBeacon(_ beaconScanner: BeaconScanner, URL: Foundation.URL, RSSI: Int) {
    }
    
    // MARK: Tracking enter/exit changes for DWELL
    
    fileprivate func trackEnter(_ beaconID: BeaconID) {
        lastStoneChange[beaconID] = Date().timeIntervalSince1970
    }
    
    fileprivate func trackExit(_ beaconID: BeaconID) {
        lastStoneChange.removeValue(forKey: beaconID)
    }
    
    fileprivate func trackReported(_ beaconID: BeaconID) {
        lastStoneChange[beaconID] = Double.greatestFiniteMagnitude
    }
    
    // MARK: Tracking enter/exit changes for HOVER
    
    fileprivate func trackHoverEnter(_ beaconID: BeaconID) {
        lastStoneHoverChange[beaconID] = Date().timeIntervalSince1970
    }
    
    fileprivate func trackHoverExit(_ beaconID: BeaconID) {
        lastStoneHoverChange.removeValue(forKey: beaconID)
    }
    
    fileprivate func trackHoverReported(_ beaconID: BeaconID) {
        lastStoneHoverChange[beaconID] = Double.greatestFiniteMagnitude
    }
}
