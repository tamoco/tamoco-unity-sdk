//
//  BeaconScanner.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 20/04/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation
import CoreLocation

class BeaconMonitor: NSObject, TriggerMonitor, CLLocationManagerDelegate {
    
    var onAction: ((_ type: ActionType, _ trigger: Trigger) -> Void)?
    weak var tamoco: TamocoImplementation!
    var currentTriggers: Set<Trigger> {
        var triggers = Set<BeaconTrigger>()
        for region in enteredRegions {
            if let trigger = regionTriggerMapper[region.identifier] {
                triggers.insert(trigger)
            }
        }
        
        return triggers
    }
    
    fileprivate var regionTriggerMapper = [String: BeaconTrigger]()
    fileprivate var enteredRegions = Set<CLBeaconRegion>()
    fileprivate var manager: CLLocationManager {
        return tamoco.locationManager.manager
    }
    
    fileprivate var observers = [LMObserver]()
    fileprivate var lastRegionChange = [String: TimeInterval]()
    fileprivate var lastRegionHoverChange = [String: TimeInterval]()
    
    required init(tamoco: TamocoImplementation) {
        super.init()
        self.tamoco = tamoco
        
        observers.append(LMObserver(type: .monitoringDidFailForRegion, callback: { (observer) in
            if let region = observer.region as? CLBeaconRegion {
                let error = observer.error
                log(.warning, message: "Beacon MonitoringDidFailForRegion region: \(region.identifier), major:\(region.major.debugDescription) minor:\(region.minor.debugDescription) | error: \(error.debugDescription)")
            }
        }))
        
        observers.append(LMObserver(type: .didStartMonitoringForRegion, callback: { [weak self](observer) in
            if let weakSelf = self, let region = observer.region as? CLBeaconRegion {
                // If reagion is not entered yet, then request it's state
                if !weakSelf.enteredRegions.contains(region) {
                    log(.info, message: "Beacon requestStateForRegion region: \(region.identifier), major:\(region.major.debugDescription) minor:\(region.minor.debugDescription)")
                    weakSelf.setNotify(forRegion: region)
                    weakSelf.manager.requestState(for: region)
                }
            }
        }))
        
        observers.append(LMObserver(type: .didDetermineState, callback: { [weak self](observer) in
            if let weakSelf = self, let region = observer.region as? CLBeaconRegion, let state = observer.state {
                // If region is inside, then just trigger action
                log(.info, message: "Beacon DidDetermineState region: \(region.identifier), major:\(region.major.debugDescription) minor:\(region.minor.debugDescription)")
                if state == .inside {
                    log(.info, message: "We are inside the Beacon region \(region.identifier)")
                    
                    // Sanity check if we entered region while this response was sent
                    let alreadyEntered = weakSelf.enteredRegions.contains(region)
                    if let trigger = weakSelf.regionTriggerMapper[region.identifier], !alreadyEntered {
                        weakSelf.enteredRegions.insert(region)
                        weakSelf.trackEnter(region.identifier)
                        weakSelf.startRanging(region)
                        weakSelf.onAction?(ActionType.bleEnter, trigger)
                    }
                }
            }
        }))
        
        observers.append(LMObserver(type: .didEnterRegion, callback: { [weak self](observer) in
            if let weakSelf = self, let region = observer.region as? CLBeaconRegion {
                log(.info, message: "Beacon didEnterRegion region: \(region.identifier), major:\(region.major.debugDescription) minor:\(region.minor.debugDescription)")
                
                let alreadyEntered = weakSelf.enteredRegions.contains(region)
                if let trigger = weakSelf.regionTriggerMapper[region.identifier], !alreadyEntered {
                    weakSelf.enteredRegions.insert(region)
                    weakSelf.trackEnter(region.identifier)
                    weakSelf.startRanging(region)
                    weakSelf.onAction?(ActionType.bleEnter, trigger)
                }
            }
        }))
        
        observers.append(LMObserver(type: .didExitRegion, callback: { [weak self](observer) in
            if let weakSelf = self, let region = observer.region as? CLBeaconRegion {
                weakSelf.exitBeacon(region: region)
            }
        }))
        
        observers.append(LMObserver(type: .didRangeBeacons, callback: { [weak self](observer) in
            if let weakSelf = self, let beacons = observer.beacons, let region = observer.region {
                // Lets check if we need to report any HOVER
                let identifier = region.identifier
                let trigger: BeaconTrigger? = weakSelf.regionTriggerMapper[identifier]
                let time: TimeInterval? = weakSelf.lastRegionHoverChange[identifier]
                
                for beacon in beacons {
                    // proximity that we need to listen for
                    let wantedProximity = beacon.proximity == .near || beacon.proximity == .immediate
                    
                    switch (wantedProximity, trigger, time) {
                    case let (true, trigger?, nil):
                        log(.info, message: "Entering Hover start timer (\(trigger.name.debugDescription)")
                        weakSelf.trackHoverEnter(identifier)
                        
                    case (true, _?, _?):
                        break
                        
                    case (false, _?, _?):
                        if let region = region as? CLBeaconRegion, beacon.proximity == .unknown {
                            weakSelf.exitBeacon(region: region)
                        }
                        
                    default:
                        weakSelf.trackHoverExit(identifier)
                    }
                }
            }
        }))
        
        // Add all observers to Location Manager
        observers.forEach { (observer) in
            tamoco.locationManager.addManagerObserver(observer)
        }
    }
    
    deinit {
        // Remove all observers to Location Manager
        observers.forEach { (observer) in
            if tamoco != nil {
                tamoco.locationManager.removeManagerObserver(observer)
            }
        }
    }
    
    func updateTriggers(_ triggers: Set<Trigger>) {
        // Let's get the regions that are currently monitored
        let monitoredRegions = Set<CLRegion>(manager.rangedRegions.filter { region in region is CLBeaconRegion })
        log(.info, message: "Beacon Regions: \(monitoredRegions.count)")
        
        // - We need to filter out those triggers that are missing ID (sanity check)
        // - then just map them to CLBeaconRegion
        // - just in case we remove all Regions with an empty identifier (if mapping failed)
        let newRegions = triggers.filter { trigger in trigger.id != nil }.map { (trigger: Trigger) -> CLRegion in
                if let trigger = trigger as? BeaconTrigger, let beaconUUID = UUID(uuidString: trigger.beaconId) {
                    let identifier = trigger.id
                    var region: CLBeaconRegion!
                    
                    // If both, major and minor are provided, then initialize CLBeaconRegion with both
                    // else check if only major is provided, then initialize CLBeaconRegion only with major
                    // otherwise just create CLBeaconRegion with UUID
                    switch (trigger.major, trigger.minor) {
                    case let (major?, minor?):
                        region = CLBeaconRegion(proximityUUID: beaconUUID, major: major, minor: minor, identifier: identifier!)
                        
                    case let (major?, nil):
                        region = CLBeaconRegion(proximityUUID: beaconUUID, major: major, identifier: identifier!)
                        
                    default:
                        region = CLBeaconRegion(proximityUUID: beaconUUID, identifier: identifier!)
                    }
                    
                    setNotify(forRegion: region)
                    regionTriggerMapper[region.identifier] = trigger
                    
                    return region
                } else { // Fallback, this should not happen
                    return CLBeaconRegion(proximityUUID: UUID(), identifier: "")
                }
        }.filter { region in region.identifier != "" }
        
        // Get items to remove and remove them from monitoring
        let needToRemove = monitoredRegions.subtracting(newRegions)
        for region in needToRemove {
            log(.info, message: "Removing beacon region with identifier: \(region.identifier)")
            regionTriggerMapper.removeValue(forKey: region.identifier)
            stopMonitoring(region)
        }
        
        // Start to monitor all regions
        for region in newRegions {
            startMonitoring(region)
        }
    }
    
    func checkCurrentTriggers() {
        log(.info, message: "checkCurrentTriggers - Beacons")
        
        // Lets check if we need to report any DWELL
        let now = Date().timeIntervalSince1970

        // Go trough all reported regions and check their last change, then compare it with the DWELL timeout
        forEachChange(lastRegionChange, now: now, callback: checkForDwell)
        
        // Go trough all reported regions for hover
        forEachChange(lastRegionHoverChange, now: now, callback: checkForHover)
    }
    
    fileprivate func forEachChange(_ changeSet: [String: TimeInterval], now: TimeInterval, callback: (_ identifier: String, _ trigger: BeaconTrigger, _ now: TimeInterval, _ time: TimeInterval) -> Void) {
        for (regionIdentifier, time) in changeSet {
            if let trigger = regionTriggerMapper[regionIdentifier] {
                callback(regionIdentifier, trigger, now, time)
            }
        }
    }
    
    fileprivate func checkForDwell(_ identifier: String, trigger: BeaconTrigger, now: TimeInterval, time: TimeInterval) {
        let dwell: TimeInterval = trigger.dwellInterval ?? ConfigurationService.monitor.beaconDWELLTimeout
        if (now - time) > dwell {
            log(.info, message: "Beacon DWELL: \(trigger.beaconId)")
            trackReported(identifier)
            onAction?(.bleDwell, trigger)
        }
    }
    
    fileprivate func checkForHover(_ identifier: String, trigger: BeaconTrigger, now: TimeInterval, time: TimeInterval) {
        let hover: TimeInterval = trigger.hoverInterval ?? ConfigurationService.monitor.beaconHOVERTimeout
        if (now - time) > hover {
            log(.info, message: "Hover reported: \(trigger.beaconId)")
            trackHoverReported(identifier)
            onAction?(ActionType.bleHover, trigger)
        }
    }
    
    fileprivate func setNotify(forRegion region: CLBeaconRegion) {
        region.notifyOnEntry = true
        region.notifyOnExit = true
        region.notifyEntryStateOnDisplay = true
    }
    
    // MARK: Managing monitoring beacons
    
    fileprivate func startMonitoring(_ region: CLRegion) {
        // Check if region is CLBeaconRegion (it should be)
        if let region = region as? CLBeaconRegion {
            manager.startMonitoring(for: region)
        }
    }
    
    fileprivate func stopMonitoring(_ region: CLRegion) {
        // Check if region is CLBeaconRegion (it should be)
        if let region = region as? CLBeaconRegion {
            manager.stopMonitoring(for: region)
        }
    }
    
    fileprivate func startRanging(_ region: CLRegion) {
        // Check if region is CLBeaconRegion (it should be)
        if let region = region as? CLBeaconRegion {
            manager.startRangingBeacons(in: region)
        }
    }
    
    fileprivate func stopRanging(_ region: CLRegion) {
        // Check if region is CLBeaconRegion (it should be)
        if let region = region as? CLBeaconRegion {
            manager.stopRangingBeacons(in: region)
        }
    }
    
    fileprivate func exitBeacon(region: CLBeaconRegion) {
        log(.info, message: "Beacon didExitRegion region: \(region.identifier), major:\(region.major.debugDescription) minor:\(region.minor.debugDescription)")
        
        enteredRegions.remove(region)
        if let trigger = regionTriggerMapper[region.identifier] {
            trackExit(region.identifier)
            stopRanging(region)
            onAction?(ActionType.bleExit, trigger)
        }
    }
    
    // MARK: Tracking enter/exit changes for DWELL
    
    fileprivate func trackEnter(_ identifier: String) {
        lastRegionChange[identifier] = Date().timeIntervalSince1970
    }
    
    fileprivate func trackExit(_ identifier: String) {
        lastRegionChange.removeValue(forKey: identifier)
    }
    
    fileprivate func trackReported(_ identifier: String) {
        lastRegionChange[identifier] = Double.greatestFiniteMagnitude //  DBL_MAX
    }
    
    // MARK: Tracking enter/exit changes for HOVER
    
    fileprivate func trackHoverEnter(_ identifier: String) {
        lastRegionHoverChange[identifier] = Date().timeIntervalSince1970
    }
    
    fileprivate func trackHoverExit(_ identifier: String) {
        lastRegionHoverChange.removeValue(forKey: identifier)
    }
    
    fileprivate func trackHoverReported(_ identifier: String) {
        lastRegionHoverChange[identifier] = Double.greatestFiniteMagnitude //  DBL_MAX
    }
}
