//
//  BluetoothManager.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 05/10/2016.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation
import CoreBluetooth

class Bluetooth {
    static var state: CBCentralManagerState = .unknown
}

class BluetoothManager: NSObject, CBCentralManagerDelegate {
    
    fileprivate var cbmanager: CBCentralManager!
    
    override init() {
        super.init()
        let options = [CBCentralManagerOptionShowPowerAlertKey: false]
        cbmanager = CBCentralManager(delegate: self, queue: DispatchQueue.main, options: options)
    }
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        Bluetooth.state = CBCentralManagerState(rawValue: central.state.rawValue) ?? .unknown
    }
}
