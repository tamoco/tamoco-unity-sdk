//
//  SerializingError.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 02/05/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

enum SerializingError: Error {
    case noError
    case generalError(source: Any)
    case insufficientData(source: Any)
}
