//
//  Serializing.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 29/04/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

/// Protocol used for Serializing and Deserializing objects
protocol Serializing: class {
    
    associatedtype InputType
    associatedtype OutputType
    
    /// Method takes the input object and it process it to the output object.
    ///
    /// - Parameters:
    ///     - obj: Input object which will be processed
    /// 
    /// - returns: It returns processed input object.
    static func process(_ obj: InputType) throws -> OutputType
}
