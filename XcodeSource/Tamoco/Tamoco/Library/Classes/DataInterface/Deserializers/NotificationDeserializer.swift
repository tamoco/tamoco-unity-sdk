//
//  NotificationDeserializer.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 02/05/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

class NotificationDeserializer: Serializing {
    
    typealias InputType = Dictionary<AnyHashable, Any>
    typealias OutputType = Notification
    
    class func process(_ obj: InputType) throws -> OutputType {
        let data = Notification()
        data.body = castToStringOrNil(obj["body"])
        
        return data
    }
}
