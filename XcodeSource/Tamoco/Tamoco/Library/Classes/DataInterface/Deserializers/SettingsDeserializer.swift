//
//  SettingsDeserializer.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 12/10/2016.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

class SettignsDeserializer: Serializing {
    
    typealias InputType = Data
    typealias OutputType = Settings
    
    class func process(_ obj: InputType) throws -> OutputType {
        let data = Settings()
        
        if let json = (try? JSONSerialization.jsonObject(with: obj, options: JSONSerialization.ReadingOptions.mutableContainers)) as? NSDictionary {
            data.inventoryInterval = castToDoubleOrNil(json["inventory_interval"])
            data.bugFenderKey = castToStringOrNil(json["bug_fender_key"])
            data.loglevel = castToStringOrNil(json["log_level"])
        }
        
        return data
    }
}
