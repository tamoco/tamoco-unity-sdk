//
//  NearbyDeserializer.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 29/04/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation
import CoreLocation

class NearbyDeserializer: Serializing {
    
    typealias InputType = Data
    typealias OutputType = NearbyResponse
    
    class func process(_ obj: InputType) throws -> OutputType {
        let nearby = NearbyResponse()
        
        if let json = (try? JSONSerialization.jsonObject(with: obj, options: JSONSerialization.ReadingOptions.mutableContainers)) as? NSDictionary {
            nearby.interval = castToDoubleOrNil(json["interval"])
            nearby.threshold = castToDoubleOrNil(json["threshold"])
            
            if let array = json["inventory"] as? [NSDictionary] {
                var inventoryArray = [Inventory]()
                
                for obj in array {
                    let inventory = Inventory()
                    inventory.inventoryType = InventoryType(rawValue: castToInt(obj["inventory_type"])) ?? .unknown
                    inventory.inventoryId = "\(castToInt(obj["inventory_id"]))"
                    inventory.distance = castToDouble(obj["distance"])
                    inventory.name = castToString(obj["name"])
                    
                    if let latitude = castToDoubleOrNil(obj["latitude"]), let longitude = castToDoubleOrNil(obj["longitude"]) {
                        inventory.geo = Geo(location: CLLocationCoordinate2D(latitude: latitude, longitude: longitude))
                    }
                    
                    inventory.dwellInterval = castToDoubleOrNil(obj["dwell_interval"])
                    inventory.hoverInterval = castToDoubleOrNil(obj["hover_interval"])
                    inventory.radius = castToDoubleOrNil(obj["radius"])
                    
                    inventory.beaconId = castToStringOrNil(obj["beacon_id"])
                    
                    if let major = castToIntOrNil(obj["major"]) {
                        inventory.major = CLBeaconMajorValue(major)
                    }
                    
                    if let minor = castToIntOrNil(obj["minor"]) {
                        inventory.minor = CLBeaconMinorValue(minor)
                    }
                    
                    inventory.ssid = castToStringOrNil(obj["ssid"])
                    inventory.mac = castToStringOrNil(obj["mac"])
                    
                    inventory.namespaceId = castToStringOrNil(obj["namespace"])
                    inventory.instanceId = castToStringOrNil(obj["instance"])
                    
                    inventoryArray.append(inventory)
                }
                
                nearby.inventory = inventoryArray
            }
        }
        
        return nearby
    }
}
