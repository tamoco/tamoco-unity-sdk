//
//  EmptyDeserializer.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 02/05/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

class EmptyDeserializer: Serializing {
    
    typealias InputType = Data
    typealias OutputType = Void
    
    class func process(_ obj: InputType) throws -> OutputType {
    }
}
