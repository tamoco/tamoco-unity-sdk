//
//  ErrorDeserializer.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 29/04/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

class ErrorDeserializer: Serializing {
    
    typealias InputType = Data
    typealias OutputType = BaseError
    
    class func process(_ obj: InputType) throws -> OutputType {
        let error = BaseError()
        
        if let json = (try? JSONSerialization.jsonObject(with: obj, options: JSONSerialization.ReadingOptions.mutableContainers)) as? NSDictionary {
            error.errorCode = castToString(json["errorCode"])
        }
        
        return error
    }
}
