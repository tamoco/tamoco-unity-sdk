//
//  TrackResponseDeserializer.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 02/05/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

class TrackResponseDeserializer: Serializing {
    
    typealias InputType = Data
    typealias OutputType = TrackResponse
    
    class func process(_ obj: InputType) throws -> OutputType {
        let response = TrackResponse()
        
        if let json = (try? JSONSerialization.jsonObject(with: obj, options: JSONSerialization.ReadingOptions.mutableContainers)) as? NSDictionary {
            if let creativeTypeNum = castToIntOrNil(json["creative_type"]) {
                response.creativeType = CreativeType(rawValue: creativeTypeNum) ?? CreativeType.noCreative
            }
            
            response.redirect = castToStringOrNil(json["redirect"])
            response.snippet = castToStringOrNil(json["snippet"])
            response.tracked = castToStringOrNil(json["tracked"])
            response.delay = castToDouble(json["delay"])
            
            if let notification = json["notification"] as? NotificationDeserializer.InputType {
                response.notification = try NotificationDeserializer.process(notification)
            }
        }
        
        return response
    }
}
