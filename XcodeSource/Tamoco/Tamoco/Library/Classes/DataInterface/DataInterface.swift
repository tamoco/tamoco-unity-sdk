//
//  DataInterface.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 29/04/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

class DataInterface {
    
    struct DataInterfaceConfig {
        var apiSecret: String?
        var apiId: String?
        var customId: String?
    }
    
    fileprivate var configuration: DataInterfaceConfig = DataInterfaceConfig()
    
    fileprivate class var sharedService:DataInterface {
        struct Static {
            static let shared = DataInterface()
        }
        return Static.shared
    }
    
    // MARK: Configuration
    
    class var config : DataInterfaceConfig {
        return DataInterface.sharedService.configuration
    }
    
    class func setApiSecret(_ apiSecret: String) {
        DataInterface.sharedService.configuration.apiSecret = apiSecret
    }
    
    class func setApiId(_ apiId: String) {
        DataInterface.sharedService.configuration.apiId = apiId
    }
    
    class func setCustomId(_ customId: String?) {
        DataInterface.sharedService.configuration.customId = customId
    }
    
    // MARK: Services
    
    class var evt : DataEvtService {
        return DataInterface.sharedService.evtService
    }
    
    class var vault : DataVaultService {
        return DataInterface.sharedService.vaultService
    }
    
    fileprivate let evtService: DataEvtService = DataEvtService()
    fileprivate let vaultService: DataVaultService = DataVaultService()
}
