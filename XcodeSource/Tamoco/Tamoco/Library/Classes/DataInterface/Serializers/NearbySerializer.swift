//
//  NearbySerializer.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 05/10/2016.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

class NearbySerializer: Serializing {
    
    typealias InputType = Nearby
    typealias OutputType = Data
    
    class func process(_ obj: InputType) throws -> OutputType {
        var dataObject: Dictionary<AnyHashable, Any> = [
            "sdk_timestamp": obj.sdkTimestamp
        ]
        
        dataObject["device"] = try DeviceSerializer.process(obj.device)
        dataObject["app"] = try AppSerializer.process(obj.app)
        dataObject["geo"] = try LocationDictionarySerializer.process(obj.geo)
        
        if let data = castToJSONData(dataObject) {
            return data
        } else {
            throw SerializingError.insufficientData(source: obj)
        }
    }
}
