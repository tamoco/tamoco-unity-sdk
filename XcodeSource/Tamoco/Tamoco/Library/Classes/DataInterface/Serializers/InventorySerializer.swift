//
//  InventorySerializer.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 02/05/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

class InventorySerializer: Serializing {
    
    typealias InputType = Inventory
    typealias OutputType = Dictionary<AnyHashable, Any>
    
    class func process(_ obj: InputType) throws -> OutputType {
        var dataObject: Dictionary<AnyHashable, Any> = [
            "inventory_type": obj.inventoryType.rawValue,
            "inventory_id": Int(obj.inventoryId) ?? -1,
            "distance": obj.distance
        ]
        
        if let triggerType = obj.triggerType?.rawValue {
            dataObject["trigger_type"] = triggerType
        }
        
        if let beaconId = obj.beaconId {
            dataObject["beacon_id"] = beaconId
        }
        
        if let major = obj.major {
            dataObject["major"] = Int(major)
        }
        
        if let minor = obj.minor {
            dataObject["minor"] = Int(minor)
        }
        
        if let proximity = obj.proximity {
            dataObject["proximity"] = proximity
        }
        
        if let ssid = obj.ssid {
            dataObject["ssid"] = ssid
        }
        
        if let namespaceId = obj.namespaceId {
            dataObject["namespace"] = namespaceId
        }
        
        if let instanceId = obj.instanceId {
            dataObject["instance"] = instanceId
        }
        
        if let battery = obj.battery {
            dataObject["battery"] = Int(battery)
        }
        
        if let mac = obj.mac {
            dataObject["mac"] = mac
        }
        
        return dataObject
    }
}
