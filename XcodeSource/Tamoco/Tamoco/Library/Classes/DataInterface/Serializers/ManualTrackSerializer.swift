//
//  ManualTrackSerializer.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 12/10/2016.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

class ManualTrackSerializer: Serializing {
    
    typealias InputType = ManualTrack
    typealias OutputType = Data
    
    class func process(_ obj: InputType) throws -> OutputType {
        var dataObject: Dictionary<AnyHashable, Any> = [
            "event_id": obj.eventId,
            "sdk_timestamp": obj.sdkTimestamp        ]
        
        dataObject["device"] = try DeviceSerializer.process(obj.device)
        dataObject["app"] = try AppSerializer.process(obj.app)
        dataObject["geo"] = try LocationDictionarySerializer.process(obj.geo)
        dataObject["trigger"] = try ManualTriggerSerializer.process(obj.trigger)
        
        if let data = castToJSONData(dataObject) {
            return data
        } else {
            throw SerializingError.insufficientData(source: obj)
        }
    }
}
