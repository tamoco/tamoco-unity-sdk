//
//  DeviceSerializer.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 02/05/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

class DeviceSerializer: Serializing {
    
    typealias InputType = Device
    typealias OutputType = Dictionary<AnyHashable, Any>
    
    class func process(_ obj: InputType) throws -> OutputType {
        let data: Dictionary<AnyHashable, Any> = [
            "device_id": obj.deviceId,
            "ip": obj.ip,
            "device_type": obj.deviceType.rawValue,
            "make": obj.make,
            "model": obj.model,
            "locale": obj.locale,
            "os": obj.os,
            "os_version": obj.osVersion,
            "hw_version": obj.hwVersion,
            "screen_width": obj.screenWidth,
            "screen_height": obj.screenHeight,
            "bluetooth": obj.bleState,
            "location": obj.locationServiceState,
            "battery": obj.batteryLevel
        ]
        
        return data
    }
}
