//
//  TrackResponseSerializer.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 11/05/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

class TrackResponseSerializer: Serializing {
    
    typealias InputType = TrackResponse
    typealias OutputType = Data
    
    class func process(_ obj: InputType) throws -> OutputType {
        var dataObject: Dictionary<AnyHashable, Any> = [
            "creative_type": obj.creativeType.rawValue
        ]
        
        dataObject["redirect"] = obj.redirect
        dataObject["snippet"] = obj.snippet
        dataObject["tracked"] = obj.tracked
        
        if let data = castToJSONData(dataObject) {
            return data
        } else {
            throw SerializingError.insufficientData(source: obj)
        }
    }
}
