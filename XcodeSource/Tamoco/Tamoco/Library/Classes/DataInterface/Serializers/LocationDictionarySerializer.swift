//
//  LocationDictionarySerializer.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 05/05/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

class LocationDictionarySerializer: Serializing {
    
    typealias InputType = Geo
    typealias OutputType = Dictionary<AnyHashable, Any>
    
    class func process(_ obj: InputType) throws -> OutputType {
        var dataObject: Dictionary<AnyHashable, Any> = [
            "latitude": obj.location.latitude,
            "longitude": obj.location.longitude
        ]
        
        if let accuracy = obj.accuracy {
            dataObject["accuracy"] = accuracy
        }
        
        return dataObject
    }
}
