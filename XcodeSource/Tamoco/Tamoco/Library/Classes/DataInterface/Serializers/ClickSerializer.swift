//
//  ClickSerializer.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 02/05/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

class ClickSerializer: Serializing {
    
    typealias InputType = String
    typealias OutputType = Data
    
    class func process(_ obj: InputType) throws -> OutputType {
        let dataObject: Dictionary<AnyHashable, Any> = [
            "source": obj
        ]
        
        
        if let data = castToJSONData(dataObject) {
            return data
        } else {
            throw SerializingError.insufficientData(source: obj)
        }
    }
}
