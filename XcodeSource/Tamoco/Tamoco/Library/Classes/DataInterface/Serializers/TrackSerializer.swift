//
//  TrackSerializer.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 02/05/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

class TrackSerializer: Serializing {
    
    typealias InputType = Track
    typealias OutputType = Data
    
    class func process(_ obj: InputType) throws -> OutputType {
        var dataObject = try BaseTrackSerializer.process(obj)
        
        dataObject["event_id"] = obj.eventId
        dataObject["trigger"] = try InventorySerializer.process(obj.trigger)
        
        if let data = castToJSONData(dataObject) {
            return data
        } else {
            throw SerializingError.insufficientData(source: obj)
        }
    }
}
