//
//  ManualTriggerSerializer.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 12/10/2016.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

class ManualTriggerSerializer: Serializing {
    
    typealias InputType = ManualTrigger
    typealias OutputType = Dictionary<AnyHashable, Any>
    
    class func process(_ obj: InputType) throws -> OutputType {
        var dataObject: Dictionary<AnyHashable, Any> = [
            "trigger_code": obj.code,
            "trigger_variant": obj.variant
        ]
        
        dataObject["tac"] = obj.tac
        dataObject["tag_id"] = obj.tag_id
        
        return dataObject
    }
}
