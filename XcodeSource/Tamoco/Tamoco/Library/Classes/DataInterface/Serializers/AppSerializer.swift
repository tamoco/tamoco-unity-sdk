//
//  AppSerializer.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 02/05/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

class AppSerializer: Serializing {
    
    typealias InputType = App
    typealias OutputType = Dictionary<AnyHashable, Any>
    
    class func process(_ obj: InputType) throws -> OutputType {
        var data: Dictionary<AnyHashable, Any> = [
            "bundle_id": obj.bundleId,
            "app_name": obj.appName,
            "app_version": obj.appVersion,
            "sdk_version": obj.sdkVersion
        ]
        data["custom_id"] = obj.customId
        
        return data
    }
}
