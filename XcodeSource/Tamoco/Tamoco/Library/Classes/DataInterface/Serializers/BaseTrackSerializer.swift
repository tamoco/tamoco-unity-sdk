//
//  BaseTrackSerializer.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 12/10/2016.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

class BaseTrackSerializer: Serializing {
    
    typealias InputType = BaseTrack
    typealias OutputType = Dictionary<AnyHashable, Any>
    
    class func process(_ obj: InputType) throws -> OutputType {
        var dataObject: Dictionary<AnyHashable, Any> = [
            "sdk_timestamp": obj.sdkTimestamp
        ]
        
        dataObject["device"] = try DeviceSerializer.process(obj.device)
        dataObject["app"] = try AppSerializer.process(obj.app)
        dataObject["geo"] = try LocationDictionarySerializer.process(obj.geo)
        
        return dataObject
    }
}
