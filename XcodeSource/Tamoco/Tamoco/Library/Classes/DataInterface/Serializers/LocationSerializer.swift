//
//  LocationSerializer.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 02/05/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation
import CoreLocation

class LocationSerializer: Serializing {
    
    typealias InputType = Geo
    typealias OutputType = Data
    
    class func process(_ obj: InputType) throws -> OutputType {
        let dataObject = try LocationDictionarySerializer.process(obj)
        
        if let data = castToJSONData(dataObject) {
            return data
        } else {
            throw SerializingError.insufficientData(source: obj)
        }
    }
}
