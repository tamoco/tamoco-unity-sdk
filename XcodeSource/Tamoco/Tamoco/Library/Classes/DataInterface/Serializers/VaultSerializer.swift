//
//  VaultSerializer.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 08/08/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

class VaultSerializer: Serializing {
    
    typealias InputType = VaultObject
    typealias OutputType = Data
    
    class func process(_ obj: InputType) throws -> OutputType {
        var dataObject: Dictionary<AnyHashable, Any> = [:]
        
        dataObject["key"] = obj.key
        dataObject["value"] = obj.value
        
        if let data = castToJSONData(dataObject) {
            return data
        } else {
            throw SerializingError.insufficientData(source: obj)
        }
    }
}
