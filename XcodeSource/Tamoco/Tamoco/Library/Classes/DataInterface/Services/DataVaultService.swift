//
//  DataVaultService.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 08/08/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation
import AdSupport

class DataVaultService: DataService {
    
    let baseUrl: String = "https://vault.tamoco.com"
    let timeoutInterval: TimeInterval = 30
    
    func put(_ key: String, completed: ((_ successData: NearbyResponse?, _ failData: BaseError?, _ response: URLResponse?, _ error: Error?) -> Void)?) {
        let url = URL(string: baseUrl + "/v1/put")!
        let request = NSMutableURLRequest(url: url, cachePolicy: NSURLRequest.CachePolicy.reloadIgnoringCacheData, timeoutInterval: timeoutInterval)
        enrichHeader(request)
        request.httpBody = try? VaultSerializer.process(VaultObject(key: key, value: ASIdentifierManager.shared().advertisingIdentifier.uuidString))
        
        let task = DataController<NearbyDeserializer, ErrorDeserializer>.taskPost(request, completed: completed)
        
        task.resume()
    }
}
