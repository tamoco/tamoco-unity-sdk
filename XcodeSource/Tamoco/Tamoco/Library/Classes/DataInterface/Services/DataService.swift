//
//  DataService.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 29/04/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

class DataService {
    
    func enrichHeader(_ request: NSMutableURLRequest) {
        setAuthorization(request)
        setContentTypeJSON(request)
    }
    
    fileprivate func setAuthorization(_ request: NSMutableURLRequest) {
        guard let apiId = DataInterface.config.apiId, let apiSecret = DataInterface.config.apiSecret, !apiId.isEmpty && !apiSecret.isEmpty else {
            return
        }
        
        let token = String(format: "Token %@/%@", apiId,
                           generateAuthToken(apiSecret, timestamp: Date().timeIntervalSince1970))

        request.setValue(token, forHTTPHeaderField: "Authorization")
    }
    
    fileprivate func setContentTypeJSON(_ request: NSMutableURLRequest) {
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
    }
    
    func generateAuthToken(_ secret: String, timestamp: TimeInterval) -> String {
        let salt = pack(Int64(timestamp))

        let saltData = Data(bytes: UnsafePointer<UInt8>(salt), count: salt.count)
        let hash: Data = Crypto.hmac(forKeyAndData: secret, data: saltData)

        let hashPrefix: Data
        if hash.count >= 16 {
            //hashPrefix = hash.subdata(in: NSMakeRange(0, 16))
            hashPrefix = hash.subdata(in: Range(uncheckedBounds: (lower: 0, upper: 16)))
        } else {
            hashPrefix = Data()
        }
        
        var d = NSData(data: saltData) as Data
        d.append(hashPrefix)
        
        let generated = NSString(data: d.base64EncodedData(options: []),
                                 encoding: String.Encoding.utf8.rawValue) ?? ""
        
        return String(generated)
    }
    
    /// Method for adding a Query parameter to the passed request
    ///
    /// - Parameters:
    ///     - key: Name of the parameter
    ///     - value: Value of the given parameter
    ///     - request: Reqeust to which this query parameter will be appended
    func addQueryParameter(_ key: String, value: String, toRequest request: NSMutableURLRequest) {
        if let url = request.url {
            var components = URLComponents(url: url, resolvingAgainstBaseURL: false)
            
            
            // if query is already set
            var query: String = ""
            if let oldQuery = url.query {
                query = oldQuery + "&"
            }
            
            query = query + key + "=" + value
            components?.query = query
            
            if let newURL = components?.url {
                request.url = newURL
            }
        }
    }
}
