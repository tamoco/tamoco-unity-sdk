//
//  DataEvtService.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 29/04/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation
import CoreLocation

class DataEvtService: DataService {
    
    let baseUrl: String = "https://evt.tamoco.com"
    let timeoutInterval: TimeInterval = 30
    
    func nearby(_ nearby: Nearby, completed: ((_ successData: NearbyResponse?, _ failData: BaseError?, _ response: URLResponse?, _ error: Error?) -> Void)?) {
        let url = URL(string: baseUrl + "/v1/nearby")!
        let request = NSMutableURLRequest(url: url, cachePolicy: NSURLRequest.CachePolicy.reloadIgnoringCacheData, timeoutInterval: timeoutInterval)
        enrichHeader(request)
        request.httpBody = try? NearbySerializer.process(nearby)
        
        let task = DataController<NearbyDeserializer, ErrorDeserializer>.taskPost(request, completed: completed)
        
        task.resume()
    }
    
    func hit(_ track: Track, completed: ((_ successData: TrackResponse?, _ failData: BaseError?, _ response: URLResponse?, _ error: Error?) -> Void)?) {
        let url = URL(string: baseUrl + "/v1/track/hit")!
        let request = NSMutableURLRequest(url: url, cachePolicy: NSURLRequest.CachePolicy.reloadIgnoringCacheData, timeoutInterval: 10)
        enrichHeader(request)
        request.httpBody = try? TrackSerializer.process(track)
        
        let task = DataController<TrackResponseDeserializer, ErrorDeserializer>.taskPost(request, completed: completed)
        
        task.resume()
    }
    
    func click(_ source: String) {
        let url = URL(string: baseUrl + "/v1/click")!
        let request = NSMutableURLRequest(url: url, cachePolicy: NSURLRequest.CachePolicy.reloadIgnoringCacheData, timeoutInterval: timeoutInterval)
        enrichHeader(request)
        request.httpBody = try? ClickSerializer.process(source)
        
        let task = DataController<EmptyDeserializer, ErrorDeserializer>.taskPost(request, completed: nil)
        
        task.resume()
    }
    
    func ping() {
        let url = URL(string: baseUrl + "/v1/ping")!
        let request = NSMutableURLRequest(url: url, cachePolicy: NSURLRequest.CachePolicy.reloadIgnoringCacheData, timeoutInterval: timeoutInterval)
        enrichHeader(request)
        
        let task = DataController<EmptyDeserializer, ErrorDeserializer>.taskPost(request, completed: nil)
        
        task.resume()
    }

    func tap(_ track: ManualTrack, completed: ((_ successData: TrackResponse?, _ failData: BaseError?, _ response: URLResponse?, _ error: Error?) -> Void)?) {
        let url = URL(string: baseUrl + "/v1/track/tap")!
        let request = NSMutableURLRequest(url: url, cachePolicy: NSURLRequest.CachePolicy.reloadIgnoringCacheData, timeoutInterval: 10)
        enrichHeader(request)
        request.httpBody = try? ManualTrackSerializer.process(track)
        
        let task = DataController<TrackResponseDeserializer, ErrorDeserializer>.taskPost(request, completed: completed)
        
        task.resume()
    }
    
    func settings(_ location: CLLocation, completed: ((_ successData: Settings?, _ failData: BaseError?, _ response: URLResponse?, _ error: Error?) -> Void)?) {
        let url = URL(string: baseUrl + "/v1/app/settings")!
        let request = NSMutableURLRequest(url: url, cachePolicy: NSURLRequest.CachePolicy.reloadIgnoringCacheData, timeoutInterval: 10)
        enrichHeader(request)
        
        let track = BaseTrack()
        track.fillBase(location)
        
        if let dictionary = try? BaseTrackSerializer.process(track) {
            request.httpBody = castToJSONData(dictionary)
        }
        
        let task = DataController<SettignsDeserializer, ErrorDeserializer>.taskPost(request, completed: completed)
        
        task.resume()
    }
}
