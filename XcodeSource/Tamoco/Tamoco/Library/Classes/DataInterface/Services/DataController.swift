//
//  DataController.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 29/04/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

class DataController <SuccessDeserializer:Serializing, FailDeserializer:Serializing> {
    
    class func taskGet(_ request: NSMutableURLRequest, completed: ((_ successData: SuccessDeserializer.OutputType?, _ failData: FailDeserializer.OutputType?, _ response: URLResponse?, _ error: Error?) -> Void)?) -> URLSessionDataTask {
        request.httpMethod = "GET"
        return DataController.dataTaskWithRequest(request as URLRequest, completed: completed)
    }
    
    class func taskPost(_ request: NSMutableURLRequest, completed: ((_ successData: SuccessDeserializer.OutputType?, _ failData: FailDeserializer.OutputType?, _ response: URLResponse?, _ error: Error?) -> Void)?) -> URLSessionDataTask {
        request.httpMethod = "POST"
        return DataController.dataTaskWithRequest(request as URLRequest, completed: completed)
    }
    
    class func dataTaskWithRequest(_ request: URLRequest, completed: ((_ successData: SuccessDeserializer.OutputType?, _ failData: FailDeserializer.OutputType?, _ response: URLResponse?, _ error: Error?) -> Void)?) -> URLSessionDataTask {
        let task = URLSession.shared.dataTask(with: request, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) in
            let statusCode = (response as? HTTPURLResponse)?.statusCode ?? -1
            
            var deserializedResponse: SuccessDeserializer.OutputType? = nil
            var failDeserializedResponse: FailDeserializer.OutputType? = nil
            
            switch statusCode {
            case 200..<300 where data != nil && data is SuccessDeserializer.InputType:
                let resp = data as! SuccessDeserializer.InputType
                deserializedResponse = try? SuccessDeserializer.process(resp)
                
            default:
                if let data = data, data is FailDeserializer.InputType {
                    let resp = data as! FailDeserializer.InputType
                    failDeserializedResponse = try? FailDeserializer.process(resp)
                }
            }
            
            completed?(deserializedResponse, failDeserializedResponse, response, error)
        })
        
        return task
    }
}
