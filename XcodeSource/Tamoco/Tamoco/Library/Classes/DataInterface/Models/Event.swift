//
//  Event.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 05/10/2016.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

protocol Event {
    var eventId: String! { get set }
}
