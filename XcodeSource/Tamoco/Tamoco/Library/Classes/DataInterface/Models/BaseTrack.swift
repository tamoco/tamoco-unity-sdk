//
//  BaseTrack.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 26/09/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

class BaseTrack {
    var sdkTimestamp: Int!
    var device: Device!
    var app: App!
    var geo: Geo!
}
