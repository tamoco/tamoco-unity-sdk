//
//  TrackResponse.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 02/05/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

class TrackResponse {
    var creativeType: CreativeType = CreativeType.noCreative
    var redirect: String?
    var snippet: String?
    var tracked: String?
    var notification: Notification?
    var delay: TimeInterval = 0.0
}
