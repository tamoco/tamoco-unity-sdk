//
//  Device.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 02/05/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

class Device {
    var deviceId: String!
    var lmt: Bool!
    var ip: String!
    var deviceType: DeviceType!
    var make: String = "Apple"
    var model: String!
    var locale: String!
    var os: String = "IOS"
    var osVersion: String!
    var hwVersion: String!
    
    var screenWidth: Double!
    var screenHeight: Double!
    var batteryLevel: Int!
    var bleState: Bool!
    var locationServiceState: Bool!
}
