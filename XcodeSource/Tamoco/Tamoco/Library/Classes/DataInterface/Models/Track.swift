//
//  Track.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 02/05/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

class Track: BaseTrack, Event {
    var eventId: String!
    var trigger: Inventory!
}
