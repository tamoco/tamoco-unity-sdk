//
//  ManualTrigger.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 12/10/2016.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

class ManualTrigger {
    var code: String!
    var variant: String!
    var tac: String?
    var tag_id: String?
    
    init(code: String!, variant: String!) {
        self.code = code
        self.variant = variant
    }
}
