//
//  ManualTrack.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 26/09/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

class ManualTrack: BaseTrack, Event {
    var eventId: String!
    var trigger: ManualTrigger!
}
