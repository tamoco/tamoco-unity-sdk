//
//  NearbyResponse.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 12/10/2016.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

class NearbyResponse {
    var threshold: Double?
    var interval: TimeInterval?
    var inventory: [Inventory]?
}
