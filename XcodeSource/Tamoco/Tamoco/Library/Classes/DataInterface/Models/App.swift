//
//  App.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 02/05/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

class App {
    var bundleId: String!
    var appName: String!
    var appVersion: String!
    var sdkVersion: String!
    var customId: String?
}