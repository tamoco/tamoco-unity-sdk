//
//  VaultObject.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 08/08/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

class VaultObject {
    var key: String!
    var value: String!
    
    init(key: String, value: String) {
        self.key = key
        self.value = value
    }
}