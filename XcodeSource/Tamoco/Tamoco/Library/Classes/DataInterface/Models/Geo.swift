//
//  Geo.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 02/05/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation
import CoreLocation

class Geo {
    var location: CLLocationCoordinate2D!
    var accuracy: CLLocationAccuracy?
    
    init(location: CLLocationCoordinate2D) {
        self.location = location
    }
    
    init(location: CLLocationCoordinate2D, horizontalAccuracy: CLLocationAccuracy) {
        self.location = location
        self.accuracy = horizontalAccuracy
    }
}