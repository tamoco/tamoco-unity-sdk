//
//  Inventory.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 29/04/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation
import CoreLocation

class Inventory {
    var inventoryType: InventoryType = .unknown
    var inventoryId: String = ""
    var distance: Double = 0
    var name: String = ""
    var geo: Geo?
    
    var radius: Double?
    
    var beaconId: String?
    var major: CLBeaconMajorValue?
    var minor: CLBeaconMinorValue?
    
    var ssid: String?
    var mac: String?
    var proximity: Double?
    var dwellInterval: TimeInterval?
    var hoverInterval: TimeInterval?
    var triggerType: ActionType?
    
    var namespaceId: String?
    var instanceId: String?
    var battery: UInt16?
    
    convenience init(id: String = "",inventoryType: InventoryType) {
        self.init()
        self.inventoryId = id
        self.inventoryType = inventoryType
    }
}
