//
//  CreativeType.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 11/05/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

enum CreativeType: Int {
    case noCreative = 0
    case snippet = 1
    case redirect = 2
}
