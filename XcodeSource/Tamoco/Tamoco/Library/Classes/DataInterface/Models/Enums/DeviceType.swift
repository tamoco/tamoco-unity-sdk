//
//  DeviceType.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 05/05/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

enum DeviceType: Int {
    case unknown = 0
    case phone = 1
    case tablet = 2
}
