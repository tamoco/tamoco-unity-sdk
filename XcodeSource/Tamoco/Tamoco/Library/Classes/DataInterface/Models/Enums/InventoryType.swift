//
//  InventoryType.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 05/05/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

enum InventoryType: Int {
    case unknown = 0
    case beacon = 1
    case geoFence = 2
    case wiFi = 3
    case eddystone = 7
}
