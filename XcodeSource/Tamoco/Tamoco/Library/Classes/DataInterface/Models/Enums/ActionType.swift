//
//  ActionType.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 22/04/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

/// ActionTypes or better Trigger types.
@objc public enum ActionType: Int {
    // BLE Types
    /// When the device enters Beacon region.
    case bleEnter = 11
    /// When the device is within 1m range from the beacon.
    case bleHover = 12
    /// When the device is withing Beacon region for a certen time.
    case bleDwell = 13
    /// When te device exits the Beacon region.
    case bleExit = 14
    
    // Geofence Types
    /// When the device enters GeoFence region.
    case geoFenceEnter = 21
    /// When the device is within a GeoFence for a certen time.
    case geoFenceDwell = 22
    /// When the device exits GeoFence region.
    case geoFenceExit = 23
    
    // WiFi Types
    /// When the device enters the WiFi region.
    case wifiEnter = 31
    /// When the device is connected to a Wifi for a certen time.
    case wifiDwell = 32
    /// When the device exits the WiFi region.
    case wifiExit = 33
    /// When the device connects to the WiFi.
    case wifiConnect = 34
    /// When the device disconnects to the WiFi
    case wifiDisconnect = 35
    
    // Eddystone Types
    /// When the device enters Beacon region.
    case eddyEnter = 61
    /// When the device is within 1m range from the beacon.
    case eddyHover = 62
    /// When the device is withing Beacon region for a certen time.
    case eddyDwell = 63
    /// When te device exits the Beacon region.
    case eddyExit = 64
    
    func toString() -> String {
        switch self {
            
            // Ble
        case .bleEnter:
            return "BleEnter"
            
        case .bleHover:
            return "BleHover"
            
        case .bleDwell:
            return "BleDwell"
            
        case .bleExit:
            return "BleExit"
            
            // Geofence
        case .geoFenceEnter:
            return "GeoFenceEnter"
            
        case .geoFenceDwell:
            return "GeoFenceDwell"
            
        case .geoFenceExit:
            return "GeoFenceExit"
            
            // WiFi
        case .wifiEnter:
            return "WifiEnter"
            
        case .wifiDwell:
            return "WifiDwell"
            
        case .wifiExit:
            return "WifiExit"
            
        case .wifiConnect:
            return "WifiConnect"
            
        case .wifiDisconnect:
            return "WifiDisconnect"
            
        // Eddystone
        case .eddyEnter:
            return "EddyEnter"
            
        case .eddyHover:
            return "EddyHover"
            
        case .eddyDwell:
            return "EddyDwell"
            
        case .eddyExit:
            return "EddyExit"
        }
    }
}
