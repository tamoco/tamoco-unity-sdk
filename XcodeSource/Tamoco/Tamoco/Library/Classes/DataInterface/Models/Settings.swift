//
//  Settings.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 12/10/2016.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

class Settings {
    var inventoryInterval: TimeInterval?
    var bugFenderKey: String?
    var loglevel: String?
}
