//
//  NotificationController.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 11/05/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

class TrackResponseController {
    
    static let trackResponseDataKey: String = "TrackResponseDataKey"
    
    static func handleResponse(_ trackResponse: TrackResponse) {
        switch trackResponse.creativeType {
        case .noCreative:
            log(.info, message: ".NoCreative")
            
        case .snippet:
            if let snippet = trackResponse.snippet {
                log(.info, message: ".Snippet | Open interstitial: \(snippet)")
                TamocoImplementation.applicationController.openSnippetInterstitial(snippet)
            }
            
        case .redirect:
            if let redirect = trackResponse.redirect {
                log(.info, message: ".Redirect | Open redirect: \(redirect)")
                TamocoImplementation.applicationController.openURL(redirect)
            }
        }
    }
}
