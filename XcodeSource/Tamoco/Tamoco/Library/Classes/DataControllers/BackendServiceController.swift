//
//  BackendServiceController.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 20/04/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation
import CoreLocation

class BackendServiceController {
    
    weak var tamoco: TamocoImplementation!
    var onBeaconTriggersFetched: ((_ triggers: Set<Trigger>) -> Void)?
    var onGeofenceTriggersFetched: ((_ triggers: Set<Trigger>) -> Void)?
    var onWifiTriggersFetched: ((_ triggers: Set<Trigger>) -> Void)?
    var onEddystoneTriggersFetched: ((_ triggers: Set<Trigger>) -> Void)?
    var onCommunicationError: ((_ error: Error) -> Void)?
    
    var beacons: Set<BeaconTrigger>?
    var geofences: Set<GeoFenceTrigger>?
    var wifis: Set<WifiTrigger>?
    var eddystones: Set<EddystoneTrigger>?
    
    fileprivate var lastTriggersUpdateTime: TimeInterval = 0
    fileprivate var lastTriggersUpdateLocation: CLLocation?
    
    fileprivate var lastNearbyResponse: NearbyResponse?
    
    required init(tamoco: TamocoImplementation) {
        self.tamoco = tamoco
    }
    
    func checkForUpdates(_ completionHandler: ((_ fetchResults: TamocoBackgroundFetchResult) -> Void)? = nil) {
        let now = Date().timeIntervalSince1970
        let currentLocation = tamoco.locationManager.manager.location ?? ConfigurationService.defaults.fallbackLocation
        
        // Get last threshold and interval, otherwise fallback to default
        let nearbyInterval: TimeInterval = lastNearbyResponse?.interval ?? TamocoImplementation.applicationSettings?.inventoryInterval ?? ConfigurationService.config.triggersUpdateTime
        let nearbyThreshold: Double = lastNearbyResponse?.threshold ?? 0
        
        // Check if the minimum time needed for refresh has elapsed
        let minimumTimeElapsed = (lastTriggersUpdateTime + 10) <= now
        let hasNearbyIntervalElapsed = (lastTriggersUpdateTime + nearbyInterval) <= now
        let isNearbyThresholdDistanceReached: Bool
        
        // Get isNearbyThresholdDistanceReached
        if let lastLocation = lastTriggersUpdateLocation {
            isNearbyThresholdDistanceReached = currentLocation.distance(from: lastLocation) >= nearbyThreshold
        } else {
            isNearbyThresholdDistanceReached = true
        }
        
        if minimumTimeElapsed && (hasNearbyIntervalElapsed || isNearbyThresholdDistanceReached) {
            // Fallback location to something
            let location = currentLocation
            DataInterface.evt.nearby(Nearby.initializeWith(location)) { [weak self](successData, failData, response, error) in
                if let weakSelf = self {
                    // We should remember last Nearby response (for threshorld and interval)
                    weakSelf.lastNearbyResponse = successData
                    
                    if let inventory = successData?.inventory {
                        // Lets remember the current time, so we are able to calculate the difference next time
                        weakSelf.lastTriggersUpdateTime = now
                        // Remember the current location, so we are able to calculate the distance from last update
                        weakSelf.lastTriggersUpdateLocation = weakSelf.tamoco.locationManager.manager.location
                        
                        // Filter out Beacons and GeoFence-es, then sort it by the distance, and take first 'n' elements
                        let sortedRangingTriggers = inventory
                            .filter({ inventory in (inventory.inventoryType == .beacon || inventory.inventoryType == .geoFence) })
                            .sorted(by: { $0.distance < $1.distance })
                            .prefix(20)
                        
                        let beacons = Set<Trigger>(sortedRangingTriggers
                            .filter({ inventory in inventory.inventoryType == .beacon })
                            .map({ TriggerMapper.mapFrom($0) }))
                        weakSelf.beacons = beacons as? Set<BeaconTrigger>
                        weakSelf.onBeaconTriggersFetched?(beacons)
                        
                        let geofences = Set<Trigger>(sortedRangingTriggers
                            .filter({ inventory in inventory.inventoryType == .geoFence })
                            .map({ TriggerMapper.mapFrom($0) }))
                        weakSelf.geofences = geofences as? Set<GeoFenceTrigger>
                        weakSelf.onGeofenceTriggersFetched?(geofences)
                        
                        let wifis = Set<Trigger>(inventory
                            .filter({ inventory in inventory.inventoryType == .wiFi })
                            .map({ TriggerMapper.mapFrom($0) }))
                        weakSelf.wifis = wifis as? Set<WifiTrigger>
                        weakSelf.onWifiTriggersFetched?(wifis)
                        
                        let eddyStones = Set<Trigger>(inventory
                            .filter({ inventory in inventory.inventoryType == .eddystone })
                            .map({ TriggerMapper.mapFrom($0) }))
                        weakSelf.eddystones = eddyStones as? Set<EddystoneTrigger>
                        weakSelf.onEddystoneTriggersFetched?(eddyStones)

                        // Posting TamocoOnTriggersFetched trough NSNotification center to notify clients about new data.
                        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: TamocoOnTriggersFetched), object: nil, userInfo: nil)
                        
                        completionHandler?(.newData)
                        
                        // Start or stop monitoring significant location changes regarding to the list of monitored WiFis
                        if inventory.count > 0 {
                            weakSelf.tamoco.detailedLocationManager.manager.startUpdatingLocation()
                        } else {
                            weakSelf.tamoco.detailedLocationManager.manager.stopUpdatingLocation()
                        }
                    } else {    // If server response was an error...
                        // Lets try this "X" minutes later, by setting lastTriggersUpdateTime
                        // Subtract 'triggersUpdateTime' from now and add the retry timeout to get the "5 minutes" later
                        weakSelf.lastTriggersUpdateTime = now - ConfigurationService.config.triggersUpdateTime + ConfigurationService.defaults.triggersServiceRetryTimeout
                        
                        log(.error, message: "Completed response: \(response.debugDescription)")
                        log(.error, message: "Completed failData: \(failData.debugDescription)")
                        log(.error, message: "Completed error: \(error.debugDescription)")
                        
                        if let error = error {
                            weakSelf.onCommunicationError?(error)
                        }

                        completionHandler?(.failed)
                    }
                } else {    // If current object doens't exists anymore
                    // Lets remember the current time, so we are able to calculate the difference next time
                    self?.lastTriggersUpdateTime = 0
                    completionHandler?(.noData)
                }
            }
        } else {
            completionHandler?(.noData)
        }
    }
    
    func track(_ location: CLLocation?, type: ActionType, trigger: Trigger) {
        if let location = location {
            let track = Track.initializeWith(type, trigger: trigger, location: location)
            DataInterface.evt.hit(track, completed: { [weak self](successData, failData, response, error) in
                if let weakSelf = self {
                    if let trackResponse = successData {
                        switch trackResponse.creativeType {
                        case .noCreative:
                            log(.info, message: ".NoCreative")
                            
                        case .snippet, .redirect:
                            // If Track response says that we need to show notification, then lets do that
                            if  let notificationBody = trackResponse.notification?.body,
                                    ConfigurationService.config.delegateLocalNotifications,
                                    self?.tamoco.listenOnly == false
                            {
                                if let trackResponseData = try? TrackResponseSerializer.process(trackResponse) {
                                    TamocoImplementation.applicationController.sendNotification(notificationBody, delay: trackResponse.delay, userInfo: [TrackResponseController.trackResponseDataKey: trackResponseData])
                                }
                            }
                        }
                    } else {
                        log(.error, message: "Completed response: \(response.debugDescription)")
                        log(.error, message: "Completed failData: \(failData.debugDescription)")
                        log(.error, message: "Completed error: \(error.debugDescription)")
                        
                        if let error = error {
                            weakSelf.onCommunicationError?(error)
                        }
                    }
                }
                })
        }
    }
}
