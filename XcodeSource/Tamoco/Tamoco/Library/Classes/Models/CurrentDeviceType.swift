//
//  CurrentDeviceType.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 05/05/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

enum CurrentDeviceType {
    case phone
    case pad
    
    func toString() -> String {
        switch self {
        case .phone:
            return "iPhone"
            
        case .pad:
            return "iPad"
        }
    }
}
