//
//  WifiRegion.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 25/04/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

/// If at least one Trigger id is `nil`, that means that objects are not the same.
/// Current business logic is, that only SSID is relevant in comparing
func ==(lhs: WifiRegion, rhs: WifiRegion) -> Bool {
    guard lhs.ssid != nil || rhs.ssid != nil else  {
        return false
    }
    
    return lhs.ssid == rhs.ssid
}

func ==(lhs: WifiRegion?, rhs: WifiRegion?) -> Bool {
    if let ulhs = lhs, let urhs = rhs {
        return ulhs == urhs
    }
    
    if lhs === nil && rhs === nil {
        return true
    }
    
    return false
}

class WifiRegion: Hashable {
    var bssid: String?
    var ssid: String?
    var data: String?
    
    var key: String? {
        return ssid
    }
    
    /// Conforming to Hashable protocol
    var hashValue: Int {
        return ssid?.hashValue ?? -1
    }
}
