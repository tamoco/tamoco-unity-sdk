//
//  ApplicationState.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 11/05/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

enum ApplicationState {
    case active
    case inactive
    case background
}
