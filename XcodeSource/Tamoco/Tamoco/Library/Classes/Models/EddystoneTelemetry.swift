//
//  EddystoneTelemetry.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 10/10/2016.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

struct EddystoneTelemetryData {
    fileprivate let length: Int = 14
    
    var frameType: UInt8 = 0x00
    var version: UInt8 = 0x00
    var battery: UInt16 = 0x00
    var temp: Float = 0x00
    var advertisingPDUCount: UInt32 = 0x00
    var timeSinceBoot: UInt32 = 0x00
    
    init() {
    }
    
    init(data: Data) {
        if data.count == length {
            var bytes = [UInt8](repeating: 0, count: length)
            (data as NSData).getBytes(&bytes, length: length)
            
            (data as NSData).getBytes(&frameType, range: NSMakeRange(0, MemoryLayout<UInt8>.size))
            (data as NSData).getBytes(&version, range: NSMakeRange(1, MemoryLayout<UInt8>.size))
            (data as NSData).getBytes(&battery, range: NSMakeRange(2, MemoryLayout<UInt16>.size))
            battery = UInt16(bigEndian: battery)
            
            var tempFixed: Int8 = 0
            var tempFractional: UInt8 = 0
            (data as NSData).getBytes(&tempFixed, range: NSMakeRange(4, MemoryLayout<Int8>.size))
            (data as NSData).getBytes(&tempFractional, range: NSMakeRange(5, MemoryLayout<UInt8>.size))
            temp = EddystoneTelemetryData.calculateTemp(tempFixed, fractional: tempFractional)
            
            (data as NSData).getBytes(&advertisingPDUCount, range: NSMakeRange(6, MemoryLayout<UInt32>.size))
            advertisingPDUCount = UInt32(bigEndian: advertisingPDUCount)
            (data as NSData).getBytes(&timeSinceBoot, range: NSMakeRange(10, MemoryLayout<UInt32>.size))
            timeSinceBoot = UInt32(Float(UInt32(bigEndian: timeSinceBoot)) * 0.1)
        } else {
            NSLog("Data length should be \(length)")
        }
    }
    
    /// Calculate 8:8 Fixed Point representation
    /// Ref: https://courses.cit.cornell.edu/ee476/Math/
    static func calculateTemp(_ fixed: Int8, fractional: UInt8) -> Float {
        return Float(fixed) + Float(fractional) / 256.0
    }
}

struct EddystoneTelemetry {
    var telemetry: EddystoneTelemetryData = EddystoneTelemetryData()
    
    var descritpion: String {
        return "Telemetry: " + "\nBattery: \(telemetry.battery)" + "\nTemp: \(telemetry.temp)" + "\nAdvertisingPDUCount: \(telemetry.advertisingPDUCount)" + "\nTimeSinceBoot: \(telemetry.timeSinceBoot)" + "\n"
    }
    
    init(data: Data) {
        telemetry = EddystoneTelemetryData(data: data)
    }
}
