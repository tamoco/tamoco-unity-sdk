//
//  EddystoneFilter.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 06/10/2016.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

public func ==(lhs: EddystoneFilter, rhs: EddystoneFilter) -> Bool {
    return lhs.namespaceUID == rhs.namespaceUID &&
        lhs.instanceID == rhs.instanceID
}

open class EddystoneFilter: NSObject {
    open fileprivate(set) var namespaceUID: String!
    open fileprivate(set) var instanceID: String?
    
    public init(namespaceUID: String!, instanceID: String?) {
        super.init()
        self.namespaceUID = namespaceUID
        self.instanceID = instanceID
    }
    
    open override var hashValue: Int {
        var hash = namespaceUID
        
        if let instanceID = instanceID {
            hash = hash! + "\(instanceID)"
        }
        
        return hash!.hashValue
    }
}
