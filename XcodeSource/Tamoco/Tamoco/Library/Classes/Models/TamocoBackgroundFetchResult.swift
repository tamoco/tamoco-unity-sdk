//
//  TamocoBackgroundFetchResult.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 16/05/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

/// TamocoBackgroundFetchResult is equivalent to `UIBackgroundFetchResult`. It is used for cross platform compatibility, because `UIBackgroundFetchResult` is included in UIKit, which is available only for iOS.
@objc public enum TamocoBackgroundFetchResult : UInt {
    /// This is used when new data was available.
    case newData
    /// This is used when no new data was available.
    case noData
    /// This is used in case if retrieving new data fails.
    case failed
}
