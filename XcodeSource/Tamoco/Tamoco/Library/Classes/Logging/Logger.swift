//
//  Logger.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 22/04/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

/// Struct for setting logging level. Check `LogLevel`
open class TamocoLogger: NSObject {
    /// Wanted output level.
    open static var level: LogLevel = .error
    /// You can set the custom print closure. Every message is redirected to this closure.
    open static var printClosure:((String) -> Void)?
}

/// The level of the log output that you want.
@objc public enum LogLevel: Int {
    /// Nothing will be printed out.
    case none = 0
    /// Only Errors will be printed.
    case error = 1
    /// Only Warnings and errors are printed
    case warning = 2
    /// Info, Warnings and errors are printed.
    case info = 3
    /// Everything is printed.
    case full = 1000
}

func log(_ level: LogLevel, message: String) {
    if level != .none && level.rawValue <= TamocoLogger.level.rawValue {
        if let printClosure = TamocoLogger.printClosure {
            printClosure(message)
        } else {
            NSLog("%@", message)
        }
        
        remoteLog(level, message: message)
    }
}

private func remoteLog(_ level: LogLevel, message: String) {
    #if DEBUG
    #else
        switch level {
        case .none:
            break
            
        case .error:
            RemoteLogger.logErr(message)
            
        case .warning:
            RemoteLogger.logWarn(message)
            
        case .info, .full:
            RemoteLogger.log(message)
        }
    #endif
}
