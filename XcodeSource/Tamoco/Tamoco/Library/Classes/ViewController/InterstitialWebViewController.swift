//
//  InterstitialWebViewController.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 11/05/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import UIKit

class InterstitialWebViewController: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var closeButton: UIButton!
    fileprivate var window: UIWindow?
    
    // MARK: Content loading data
    fileprivate var htmlString: String?
    fileprivate var baseURL: URL?
    
    // MARK: Lifecycle
    
    class func instantiateFromStoryboard() -> InterstitialWebViewController {
        return UIStoryboard(name: "Main", bundle: Bundle.frameworkBundle()).instantiateViewController(withIdentifier: "InterstitialWebViewController") as! InterstitialWebViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.delegate = self
        closeButton.setTitle(tr("ProximityDoneTitle", comment: "Close button title"), for: UIControlState())
        reloadHTMLString()
    }
    
    // MARK: Content loading
    
    func loadHTMLString(_ string: String, loadBaseURL: URL? = nil) {
        htmlString = string
        baseURL = loadBaseURL
    }
    
    fileprivate func reloadHTMLString() {
        if let htmlString = htmlString {
            webView.loadHTMLString(htmlString, baseURL: baseURL)
        }
    }
    
    // MARK: User actions
    
    @IBAction func closePressed(_ sender: AnyObject) {
        close()
    }

    // MARK: Show/hide
    
    func show() {
        DispatchQueue.main.async { 
            // Ensure window
            if self.window == nil {
                self.window = UIWindow(frame: UIScreen.main.bounds)
                self.window?.backgroundColor = UIColor.red
            }
            
            // Sanity check
            if let popupWindow = self.window {
                popupWindow.windowLevel = UIWindowLevelAlert + 1
                popupWindow.isHidden = false
                popupWindow.rootViewController = self
            }
        }
    }
    
    func close() {
        DispatchQueue.main.async {
            if let popupWindow = self.window {
                popupWindow.isHidden = true
                self.window = nil
            }
        }
    }
    
    // MARK: UIWebViewDelegate
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        // Trying to intercept urls to load, in case if there is any matching confition to handle it manually
        if let url = (request as NSURLRequest).url?.absoluteString.lowercased() {
            switch url {
            case _ where url.contains("tamoco://close"):
                close()
                return false
                
            default:
                return true
            }
        }
        
        return true
    }
}
