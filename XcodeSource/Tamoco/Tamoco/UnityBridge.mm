
#import "UnityBridge.h"

@implementation UnityBridge


-(id)init{
    self = [super init];
    return self;
}

-(void)application: (UIApplication *)application didReceiveLocalNotification: (UILocalNotification *) notification{
    [TamocoObject didReceiveLocalNotificationUserInfo : notification.userInfo];
}

/*-(void)application:(UIApplication *)application performFetchWithCompletionHandler:(nonnull void (^)(UIBackgroundFetchResult))completionHandler{
    NSUInteger ptr = (NSUInteger)completionHandler;
    TamocoBackgroundFetchResult result = (TamocoBackgroundFetchResult) ptr;
    [TamocoObject performFetchWithCompletionHandler: TamocoBackgroundFetchResult(];
}
*/

-(id)initWithApiKey:(NSString * _Nonnull)apiKey apiSecret:(NSString * _Nonnull)secret config:(NSDictionary<NSString *, id> * _Nullable)config{
    self = [super init];
    TamocoObject = [[Tamoco alloc] initWithApiKey:apiKey apiSecret:secret config: config];
    
    TamocoObject.onCustomWIFIAction =
        ^void (ActionType type, Trigger* trigger){
            if(UnityDelegateWifi != nil){
                printf("Bridge WiFi: callback\n");
                UnityDelegateWifi(type, [trigger.id UTF8String], [trigger.name UTF8String]);
            
            }
        
        };
    
    TamocoObject.onCustomBeaconAction =
        ^void (ActionType type, Trigger* trigger){
            if(UnityDelegateBeacon != nil){
                printf("Bridge Beacon: callback\n");
                UnityDelegateBeacon(type, [trigger.id UTF8String], [trigger.name UTF8String]);
            
            }
        };
    
    TamocoObject.onCustomFenceAction =
    ^void (ActionType type, Trigger *trigger){
        if(UnityDelegateFence != nil){
            printf("Bridge Fence: callback\n");
            UnityDelegateFence(type, [trigger.id UTF8String], [trigger.name UTF8String]);
        }
    };
    
    TamocoObject.onCustomEddystoneAction =
    ^void (ActionType type, Trigger *trigger){
        if(UnityDelegateEddystone != nil){
            printf("Bridge Eddystone: callback\n");
            UnityDelegateEddystone(type, [trigger.id UTF8String], [trigger.name UTF8String]);
        }
    };
    
    TamocoObject.onCommunicationError =
    ^void (NSError* error){
        if(UnityDelegateEddystone != nil){
            printf("Bridge Eddystone: callback\n");
            //UnityDelegateError();
        }
    };
    
    return self;
}

-(Tamoco*)getTamocoObject{
    return TamocoObject;
}

-(TamocoLogger*)getLoggerLevel{
    return LoggerLevel;
}

-(void)SetTestCallback:(CallbackType) type Callback: (TriggerCallback *)callback{
    
    switch(type){
        case BeaconCallback:
            
            UnityDelegateBeacon = *callback;
            printf("Bridge Beacon: Setting Callback\n");
            
            break;
            
        case WifiCallback:
            
            UnityDelegateWifi = *callback;
            printf("Bridge wifi: Setting Callback\n");
                
            break;
            
        case FenceCallback:
            
            UnityDelegateFence = *callback;
            printf("Bridge Fence: Setting Callback\n");
            
            break;
            
        case EddystoneCallback:
            
            UnityDelegateEddystone = *callback;
            printf("Bridge Eddystone: Setting Callback\n");
            
            break;
            
        case ErrorCallback:
            UnityDelegateError = *callback;
            break;
        
            
        default:
            
            break;
    }
    
}

-(void)UnsetTestCallback:(CallbackType) type{
    
    switch(type){
        case BeaconCallback:
            UnityDelegateBeacon = nil;
            printf("Bridge Beacon: Unsetting Callback\n");
            break;
            
        case WifiCallback:
 
            UnityDelegateWifi = nil;
            printf("Bridge wifi: Unsetting Callback\n");
            
            break;
            
        case FenceCallback:
            
            UnityDelegateFence = nil;
            printf("Bridge Fence: Unsetting Callback\n");
            
            break;
            
        case EddystoneCallback:
            
            UnityDelegateEddystone = nil;
            printf("Bridge Eddystone: Unsetting Callback\n");
          
            break;
        
        default:
            
            break;
    }
    
}


-(void)dealloc{
   
}

@end

static UnityBridge* _BridgeInstance = nil;

NSString* CreateNSString(const char* source){
    if(source){
        return [NSString stringWithUTF8String:source];
    }
    else{
        return [NSString stringWithUTF8String:""];
    }
}


Trigger* GetTriggerFromSet(CallbackType type, const char* triggerID, const char* triggerName){
    Tamoco* ptr = _BridgeInstance.getTamocoObject;
    Trigger* returnTrigger;
    switch(type){
        case WifiCallback:
            for(int i = 0; i < ptr.currentWifis.count; i++){
                if(ptr.currentWifis.allObjects[i].id == CreateNSString(triggerID) && ptr.currentWifis.allObjects[i].name == CreateNSString(triggerName)){
                    returnTrigger = ptr.currentWifis.allObjects[i];
                }
            }
            break;
            
        case BeaconCallback:
            for(int i = 0; i < ptr.currentBeacons.count; i++){
                if(ptr.currentBeacons.allObjects[i].id == CreateNSString(triggerID) && ptr.currentBeacons.allObjects[i].name == CreateNSString(triggerName) ){
                    returnTrigger = ptr.currentBeacons.allObjects[i];
                }
            }
            break;
            
        case FenceCallback:
            for(int i = 0; i < ptr.currentGeofences.count; i++){
                if(ptr.currentGeofences.allObjects[i].id == CreateNSString(triggerID) && ptr.currentGeofences.allObjects[i].name == CreateNSString(triggerName) ){
                    returnTrigger = ptr.currentGeofences.allObjects[i];
                }
            }
            break;
            
        case EddystoneCallback:
            for(int i = 0; i < ptr.currentEddystones.count; i++){
                if(ptr.currentEddystones.allObjects[i].id == CreateNSString(triggerID) && ptr.currentEddystones.allObjects[i].name == CreateNSString(triggerName) ){
                    returnTrigger = ptr.currentEddystones.allObjects[i];
                }
            }
            break;
            
        }
    
    return returnTrigger;
    
}

extern "C"{
    
    //Do we need to add custom config?
    void Framework_Initialise(const char* Api_Key, const char* Api_Secret){
        if(_BridgeInstance == nil){
            _BridgeInstance = [[UnityBridge alloc] initWithApiKey:CreateNSString(Api_Key) apiSecret:CreateNSString(Api_Secret) config:nil];
            
            printf("Bridge Framework: Initialise\n");
        }
    }


    bool Framework_SetCustomID(const char* Id){
        if(_BridgeInstance.getTamocoObject){
            [_BridgeInstance.getTamocoObject setCustomId:CreateNSString(Id)];
            return true;
        }else{
            printf("Bridge Framework: Tamoco has not been initialised\n");
            return false;
        }
    }
    
    bool Framework_isCurrent(CallbackType type, const char* triggerID, const char* triggerName){
        return [_BridgeInstance.getTamocoObject isCurrent: GetTriggerFromSet(type, triggerID, triggerName)];
    }
    
    void Framework_PushKeyViaVault(const char* key){
        printf("Bridge Framewrok: accessing vault\n");
        
        [_BridgeInstance.getTamocoObject.vault push: CreateNSString(key) finished:nil];
    }
    
    void Framework_TapViaTracking(const char* code, const char* variant){
        
        [_BridgeInstance.getTamocoObject.track tap:CreateNSString(code) variant:CreateNSString(variant) completion:nil];
    }
    
    void Framework_setCustomFenceAction(CallbackType type ,TriggerCallback callback = nil){
        if(callback)
            [_BridgeInstance SetTestCallback:type Callback: &callback];
        else
            [_BridgeInstance UnsetTestCallback:type];
        
    }
    
  
  /*  TamocoBackgroundFetchResult Framework_performFetchWithCompletionHandler( ){
        TamocoBackgroundFetchResult fetch;
        [TamocoObject performFetchWithCompletionHandler:fetch];
        return fetch;
    }*/
    
    
}//end
