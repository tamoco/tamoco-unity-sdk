//
//  UnityBridge.h
//  Tamoco
//
//  Created by SoDigital on 25.07.2017.
//  Copyright © 2017 Inova IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Tamoco/Tamoco-Swift.h>

extern "C"{
    typedef void (*TriggerCallback)(ActionType type ,const char* ID, const char* name);
    
    
    enum CallbackType{
        BeaconCallback,
        WifiCallback,
        EddystoneCallback,
        FenceCallback,
        ErrorCallback,
        Debug
    };
}

@interface UnityBridge : NSObject

{
    
    Tamoco *TamocoObject;
    TamocoLogger *LoggerLevel;
    TriggerCallback UnityDelegateBeacon;
    TriggerCallback UnityDelegateWifi;
    TriggerCallback UnityDelegateEddystone;
    TriggerCallback UnityDelegateFence;
    TriggerCallback UnityDelegateError;
    //Boolean UseGameObjectDelegateBeacon;
    
}

-(Tamoco*)getTamocoObject;
-(TamocoLogger*)getLoggerLevel;
-(void)SetCallback:(CallbackType) type Callback: (TriggerCallback*) callback;

@end

