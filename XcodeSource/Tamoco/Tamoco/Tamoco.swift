//
//  Tamoco.swift
//  Tamoco
//
//  Created by Sašo Sečnjak on 19/04/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import Foundation

/// Listen for 'TamocoOnTriggersFetched' on NSNotificatvarCenter.defaultCenter() for updated on triggers.
public let TamocoOnTriggersFetched = TamocoImplementation.kOnTriggersFetched

/// Listen for 'TamocoOnCustomBeaconAction' on NSNotificationCenter.defaultCenter() for triggers upon each action.
public let TamocoOnCustomBeaconAction = TamocoImplementation.kOnCustomBeaconAction
/// Listen for 'TamocoOnCustomFenceAction' on NSNotificationCenter.defaultCenter() for triggers upon each action.
public let TamocoOnCustomFenceAction = TamocoImplementation.kOnCustomFenceAction
/// Listen for 'TamocoOnCustomWIFIAction' on NSNotificationCenter.defaultCenter() for triggers upon each action.
public let TamocoOnCustomWIFIAction = TamocoImplementation.kOnCustomWIFIAction
/// Listen for 'TamocoOnCustomEddystoneAction' on NSNotificationCenter.defaultCenter() for triggers upon each action.
public let TamocoOnCustomEddystoneAction = TamocoImplementation.kOnCustomEddystoneAction

/// Key used for ActionType in userInfo when NSNotification is posted.
public let TamocoNotificationActionTypeKey = TamocoImplementation.kNotificationActionTypeKey
/// Key used for Trigger in userInfo when NSNotification is posted.
public let TamocoNotificationTriggerKey = TamocoImplementation.kNotificationTriggerKey

/// Tamoco SDK is used for tracking custom triggers and acting on their events.
open class Tamoco: NSObject {
    
    /// 'onCustomBeaconAction' closure is triggered on each Beacon action
    open var onCustomBeaconAction: ((_ type: ActionType, _ trigger: BeaconTrigger) -> Void)?
    
    /// 'onCustomFenceAction' closure is triggered on each Geo-fence action
    open var onCustomFenceAction: ((_ type: ActionType, _ trigger: GeoFenceTrigger) -> Void)?
    
    /// 'onCustomWIFIAction' closure is triggered on each WiFi action
    open var onCustomWIFIAction: ((_ type: ActionType, _ trigger: WifiTrigger) -> Void)?
    
    /// 'onCustomEddystoneAction' closure is triggered on each Eddystone action
    open var onCustomEddystoneAction: ((_ type: ActionType, _ trigger: EddystoneTrigger) -> Void)?
    
    /// 'onCommunicationError' closure is triggered if any communication error occurs
    open var onCommunicationError: ((_ error: Error) -> Void)?
    
    /// All beacons that were returned from server. To listen for updates, start listen for `TamocoOnTriggersFetched` on NSNotificationCenter.defaultCenter().
    open var beacons: Set<BeaconTrigger>? {
        return impl.beacons
    }
    
    /// All GeoFences that were returned from server. To listen for updates, start listen for `TamocoOnTriggersFetched` on NSNotificationCenter.defaultCenter().
    open var geofences: Set<GeoFenceTrigger>? {
        return impl.geofences
    }
    
    /// All WiFis that were returned from server. To listen for updates, start listen for `TamocoOnTriggersFetched` on NSNotificationCenter.defaultCenter().
    open var wifis: Set<WifiTrigger>? {
        return impl.wifis
    }
    
    /// All Eddystones that were returned from server. To listen for updates, start listen for `TamocoOnTriggersFetched` on NSNotificationCenter.defaultCenter().
    public var eddystones: Set<EddystoneTrigger>? {
        return impl.eddystones
    }
    
    /// Current beacons that are in range.
    open var currentBeacons: Set<BeaconTrigger>? {
        return impl.currentBeacons
    }
    
    /// Current GeoFences that are in range.
    open var currentGeofences: Set<GeoFenceTrigger>? {
        return impl.currentGeofences
    }
    
    /// Current WiFis that are in range.
    open var currentWifis: Set<WifiTrigger>? {
        return impl.currentWifis
    }
    
    /// Current Eddystones that are in range.
    public var currentEddystones: Set<EddystoneTrigger>? {
        return impl.currentEddystones
    }
    
    /// CustomID adds the ability to allow their clients to track their own identifies on events.
    open var customId: String? {
        set {
            impl.customId = newValue
        }
        get {
            return impl.customId
        }
    }
    
    /// listenOnly = false - Set to true if you do not want notifications and pop-up messages in your app
    open var listenOnly: Bool {
        set {
            impl.listenOnly = newValue
        }
        get {
            return impl.listenOnly
        }
    }
    
    
    /// Access to Vault service. Do not keep the reference of this object, always access to it via Tamoco main object.
    open var vault: Vault!
    
    /// Access to Track service. Do not keep the reference of this object, always access to it via Tamoco main object.
    open var track: Tracking!
    
    fileprivate var impl: TamocoImplementation!
    
    /// Initialize object with given parameters
    ///
    /// - Parameters:
    ///     - apiKey: API Key to use when communicating with server API
    ///     - apiSecret: API Secret to use when communicating with server API
    ///     - config: Configuration of the Tamoco library. This overrides settings from TamocoConfig.plist
    public init(apiKey: String, apiSecret secret: String, config: Dictionary<String, AnyObject>? = nil) {
        super.init()
        impl = TamocoImplementation(apiKey: apiKey, apiSecret: secret, config: config, owner: self)
        vault = impl.vault()
        track = impl.track()
    }
    
    /// With this method you can check if you are currently in range of the given trigger.
    ///
    /// - Parameters:
    ///     - trigger: trigger of type `Trigger` that you want to check.
    open func isCurrent(_ trigger: Trigger) -> Bool {
        return impl.isCurrent(trigger)
    }
    
    /// This method needs to be called when you get a callback in your AppDelegate to the `application(application: UIApplication, performFetchWithCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void)` method
    ///
    /// - Parameters:
    ///     - completionHandler: completionHandler closure is called once triggers are updated from the backend server with the appropriate fetchResults which can easily map to `UIBackgroundFetchResult` with the property `uiBackgroundFetchResult`. 
    open func performFetchWithCompletionHandler(_ completionHandler: @escaping (_ fetchResults: TamocoBackgroundFetchResult) -> Void) {
        impl.performFetchWithCompletionHandler(completionHandler)
    }
    
    /// This method needs to be called when you get a callback in your AppDelegate to the `application(application: UIApplication, didReceiveLocalNotification notification: UILocalNotification) {` method
    ///
    /// - Parameters:
    ///     - userInfo: You should pass the userInfo property of the retrieved UILocalNotification.
    open func didReceiveLocalNotificationUserInfo(_ userInfo: [AnyHashable: Any]?) {
        impl.didReceiveLocalNotificationUserInfo(userInfo)
    }
}
