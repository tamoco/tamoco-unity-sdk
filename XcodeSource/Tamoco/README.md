![Tamoco](http://tamo.co/wp-content/themes/tamoco/images/Tamoco_logo.png)

Tamoco is mobile SDK for regioning devices regarding to the specified triggers on the Tamoco backend.

## Requirements
This integration guide is built with Xcode version 7.3.1 and Swift. The Tamoco iOS Client SDK support iOS 8 and above.

### Frameworks
The first step in integrating the Tamoco iOS client into your app is to include the Tamoco framework. To add this framework, simply drag the provided Tamoco.framework file onto the project root in the Project Navigator.

As soon as you drop in the framework, Xcode will display the above dialog. Also make sure that framework is also added into the “Embedded Binaries” in General tab of project config, then check if “Embedded Frameworks” appear in “Build Phases”.

**NOTE:** You must use this drag and drop method to successfully add the Tamoco framework to your project. In addition you must make sure the “Copy items into destination groups folder (if needed)” option is checked on this dialog. You may leave the other settings in their default state.
Press the Finish button to continue.

The second step is to include CoreLocation, SystemConfiguration, and AdSupport frameworks.

## Project Configuration
Your app project will require the following configuration settings to function properly with the Tamoco SDK.

### EMBEDDED CONTENT
If you are using Obj C project, then you need to go to Build Settings of your target and under "Build Options" set "Embedded Content Contains Swift Code" to YES

### DISABLE BITCODE
Bitcode is an optional XCode compilation setting that optimizes your app for distribution based on specific device.
The Tamoco SDK is not currently compatible with Bitcode. As a result, you’ll need to disable Bitcode support within your app. To disable Bitcode support, go to Project > Build Settings > Build Options and change the “Enable Bitcode” setting to ‘No’

### LOCATION PERMISSION DESCRIPTION
As a proximity SDK, the Tamoco client requires location always permissions. In iOS 8 and beyond, you must include an NSLocationAlwaysUsageDescription string entry within your app’s info.plist. The string entered here will appear within the permissions dialog that is presented to the user when the app requests location permissions. This description should provide a clear explanation and value for why the app requires location information.

### BACKGROUND MODES
You need to enable background modes for Location updates, otherwise you will get an runtime error similar to this:

	Terminating app due to uncaught exception 'NSInternalInconsistencyException', reason: 'Invalid parameter not satisfying: !stayUp || CLClientIsBackgroundable(internal->fClient)'

Go to yor target and select "Capabilities" then enable "Background Modes" and thick:  

- Location updates
- Uses Bluetooth LE accessories
- Background fetch

## Client Integration
### UPDATE YOUR APP DELEGATE
The first step to configure and initialize the Tamoco Client SDK into your app is to modify your main AppDelegate files. While initializing Tamoco client, you need to provide proper API Key, API Secret and custom ID. To change any of this parameters in runtime, just re-instantiate the Tamoco client.

In your AppDelegate file, start by including the Tamoco module:

	import Tamoco
	
Or in case of  using Objective C
	
	#import <Tamoco/Tamoco-Swift.h>

Then, add the following additional Tamoco property:

	var tamoco: Tamoco!

Set the Logging level:

	TamocoLogger.level = .Error

Next, implement SDK initialisation in method `func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool`:

	tamoco = Tamoco(apiKey: "TEST-API-KEY", apiSecret: "YOUR-SECRET")

Custom ID is an optional setting and could be set in the following way:

	tamoco.customId = “CUSTOM_ID”


### CUSTOM ID
Custom ID is an optional setting related to sending additional tracking attribute e.g. application user ID.

### DEBUG MESSAGING
For debug output messages, the level is needed to be set `TamocoLogger.level`. For all possible options, check `LogLevel` enum.

### CONFIGURE THE TAMOCO CLIENT OBJECT
Before initializing the Tamoco object you may custom configure a number of client settings. These settings allow you to balance the performance of the client relative to proximity responsiveness and battery consumption.

These settings are configured via an TamocoConfig.plist file included into the main bundle. Please note that if an entry is not defined, the default settings will apply.

For testing and development purposes, it is common to use aggressive values to see immediate results. However the default configuration values are the recommended production settings to optimize battery performance.

The following settings are available for configuration:**ENABLE\_GEOFENCE\_RANGING:**  
This optional boolean property determines if the Tamoco geofence ranging functionality should be enabled (YES) or disabled (NO).

**ENABLE\_BEACON\_RANGING:**  
This optional boolean property determines if the Tamoco beacon ranging functionality should be enabled (YES) or disabled (NO).

**ENABLE\_WIFI\_RANGING:**  
This optional boolean property determines if the Tamoco WIFI ranging functionality should be enabled (YES) or disabled (NO).

**TRIGGERS\_UPDATE\_TIME:**
The minimum amount of time that needs to elapse between fetch operations.

**REGION\_MONITORING\_ACCURACY:**  
This is set to `desiredAccuracy` of the `CLLocationManager` object for Geofence and Beacon ranging.

**WIFI\_MONITORING\_ACCURACY:**  
This is set to `desiredAccuracy` of the `CLLocationManager` object for WiFi ranging. For WiFi ranging the `didUpdateLocations` is used, to check for the current WiFi.

**DELEGATE\_NOTIFICATIONS:**  
This optional property determines if the Tamoco triggers the callbacks upon trigger actions to the designated closures.

**DELEGATE\_LOCAL\_NOTIFICATIONS:**  
This optional property determines if the Tamoco sends the local notifications upon trigger actions.

**LOCAL\_NOTIFICATION\_SOUND:**  
Sound used for local notifications. This value is set to the `soundName` property of the `UILocalNotification` object, before `UIApplication.sharedApplication().scheduleLocalNotification(...)` is called.

**ENABLE\_DEBUG\_LOCAL\_NOTIFICATIONS:**   
This is for debugging purposes. Each time that any trigger actions is happening, the local notification is pushed.

### CHECK FOR COMMUNICATION ERRORS
The Tamoco exposes the closure `onCommunicationError: ((error: NSError) -> Void)?`, which is called if any communication error occurs.

### STRIP NON-VALID ARCHITECTURES
To be able to launch the application containing Tamoco framework on the simulator and the devices, Tamoco framework contains a Universal Library (Fat Library). But currently Apple doesn't provide any other solution to this, then stripping the library of the non-valid architectures upon building the Store app.

Therefore you need to go to your target "Build Phases" and add a "New Run Script Phase" and name it "Strip Dynamic Frameworks". Make sure, that this is placed as the last phase. Then add this to its content:

	TAMOCO_LIB="${BUILT_PRODUCTS_DIR}/${FRAMEWORKS_FOLDER_PATH}/Tamoco.framework/Tamoco"
	# Get architectures for current file
	archs="$(lipo -info "${TAMOCO_LIB}" | rev | cut -d ':' -f1 | rev)"
	
	# Go trough all architectures and strip non-valid ones
	for arch in $archs; do
		if ! [[ "${VALID_ARCHS}" == *"$arch"* ]]; then
			# Strip non-valid architectures in-place
			lipo -remove "$arch" -output "${TAMOCO_LIB}" "${TAMOCO_LIB}" || exit 1
		fi
	done
## Proximity Notifications
When a proximity event is triggered, the Tamoco iOS Client SDK will automatically create a notification. The Tamoco iOS Client SDK supports two methods for dispatching these proximity notifications. You can configure which method the Tamoco client uses via the DELEGATE_NOTIFICATIONS and DELEGATE_LOCAL_NOTIFICATIONS property.

### ACTION CLOSURES
The first method is to trigger proximity closure. You can set onCustomBeaconAction, onCustomFenceAction, onCustomWIFIAction closures, regarding to which actions you want to listen on. You can enable/disable triggering these closures by setting DELEGATE_NOTIFICATIONS property in configuration (defaults to YES).

### LOCAL NOTIFICATION DISPATCH
You can also configure Tamoco iOS SDK client to dispatch standard iOS local notifications. You can manage dispatching this with DELEGATE_LOCAL_NOTIFICATIONS property in configuration (defaults to YES). To use this method, DELEGATE_LOCAL_NOTIFICATIONS should be set to YES and Tamoco object’s didReceiveLocalNotificationUserInfo should be called within the app’s didReceiveLocalNotification as in the following example:

	func application(application: UIApplication, didReceiveLocalNotification notification: UILocalNotification) {
		tamoco.didReceiveLocalNotificationUserInfo(notification.userInfo)
	}

If the app is in the background a local notification will automatically be generated and appear in the mobile device’s notification tray. When the user clicks the local notification, your app will open and the configured action, such as an interstitial, will then execute.

## Library properties
These are library class properties that can be used to access inventory returned from backend service. Check below snippet how to use it:

	let allBeacons: Set<BeaconTrigger>? = tamoco.beacons


### beacons: Set\<BeaconTrigger>?
All beacons that were returned from server. To listen for updates, start listen for 'TamocoOnTriggersFetched' on NSNotificationCenter.defaultCenter().

### geofences: Set\<GeoFenceTrigger>?
All GeoFences that were returned from server. To listen for updates, start listen for 'TamocoOnTriggersFetched' on NSNotificationCenter.defaultCenter().

### wifis: Set\<WifiTrigger>?
All WiFis that were returned from server. To listen for updates, start listen for 'TamocoOnTriggersFetched' on NSNotificationCenter.defaultCenter().

### currentBeacons: Set\<BeaconTrigger>?
Current beacons that are in range.

### currentGeofences: Set\<GeoFenceTrigger>?
Current GeoFences that are in range.

### currentWifis: Set\<WifiTrigger>?
Current WiFis that are in range.

## Vault service
You are able to send keys to the Vault service. The Vault stores client encrypted keys with a Device ID (IDFA/AAID). Before pushing to the Vault, SDK validates the key to meet server requirements. Key must be a 40-character (HEX) string (20 byte).

You can use Vault via vault property of the Tamoco SDK instance:

	tamoco.vault.pushKey(“abcdef1234...”) { (exception) in {
	}

Or in case of Objective C:

	[self.tamoco.vault pushKey:@"abcdef1234..." finished:^(NSInteger exception, NSString * _Nullable key) {  
	}];

VaultError is enum which is returned as parameter on callback. For Objective c this enum is splitted to two parts, an integer and String which is the key that was used.

VaultError:

- `NoError` (Objective C integer value 0)
    - No error occurred and push was successful
- `NonValidFormat` (Objective C integer value 1)
    - Key is not in HEX format
- `NonValidLength` (Objective C integer value 2)
    - Key length is not 40-characters
- `NonValidDataLength` (Objective C integer value 3)
    - Key length after converting to bytes it is not 20-bytes
- `CantConvertToData` (Objective C integer value 4)
    - Key can not be converted to bytes
- `PushFailed` (Objective C integer value 5)
    - Pushing to the server has failed



## Tracking service
You are able to use this for any kind of additional tracking needed.

You can use Tracking via track property of the Tamoco SDK instance:
	
	tamoco.track.tap("code", variant: "qr") { (isError) in
	}

Or in case of Objective C:

	[self.tamoco.track tap:@"code" variant:@"qr" completion:^(BOOL) {
	}];



## Interstitials
One of the primary event actions supported by the Tamoco iOS client SDK are hosted interstitials. Interstitials are HTML-based content that, when triggered, will do a full screen takeover of your app presenting the content. Interstitials provides a built-in, localizable close button that allows the user, when clicked, to dismiss the content and return to your app.

**NOTE:** By default iOS 9 only allows access to SSL web resources. As a result, interstitial or URI actions that are not sent via SSL will not function properly. In the case that your resources cannot be sent via SSL, Xcode provides a means to override its default behaviour via the App Transport Security Settings dictionary option within your app’s info.plist. To modify this setting, first add this entry to your info.plist if it doesn’t already exist. To allow any resources, both SSL and non-SSL set the Allow Arbitrary Loads boolean suboption to YES. To only allow certain domains, select the Domain Exceptions suboption, passing in a dictionary of domains in which SSL should not be enforced.

### LOCALIZING THE CLOSE BUTTON
By default, the Tamoco client uses the system style UIBarButtonSystemItemDone which allows the close button title to be localized to the user’s current language selection. To localize the button, the app must first be configured to support localization. This can be done from the **Info > Localizations** settings for the project.
The localized string is then placed in a file named proximity.strings which contains the following:

	“ProximityDoneTitle”=“localized close button title goes here”;
	
To override the default text of the close button title across all languages, place a copy of this file in the root of the project. To override specific languages, place a copy of this file within the corresponding .lproj folder in the apps bundle, for example fr.lproj/proximity.strings.

### CLOSING INTERSTITIALS FROM WITHIN THE HTML
You can provide your own close mechanism from within your HTML content by referencing the following built-in Tamoco close schema.
To close the interstitial from Javascript enter the following:

	window.location = “Tamoco://close”
	
To close the interstitial from an HTML anchor element enter the following:

	<a href="Tamoco://close">Close me</a>

## Limitations
- WiFi scanning and DWELL is not reliable, due to the Apple strict policy about background running apps
- For reporting BleHover, more power is consumed because we need to start ranging beacon for this matter