//
//  TamocoTests.swift
//  TamocoTests
//
//  Created by Sašo Sečnjak on 19/04/16.
//  Copyright © 2016 Inova IT. All rights reserved.
//

import XCTest
import CoreLocation
@testable import Tamoco

extension WifiRegion {
    func setup(_ ssid: String, bssid: String?) -> WifiRegion {
        self.ssid = ssid
        self.bssid = bssid
        return self
    }
}

class TamocoTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testPrintLocationAccuracy() {
        print("kCLLocationAccuracyBestForNavigation: \(kCLLocationAccuracyBestForNavigation)")
        print("kCLLocationAccuracyBest: \(kCLLocationAccuracyBest)")
        print("kCLLocationAccuracyNearestTenMeters: \(kCLLocationAccuracyNearestTenMeters)")
        print("kCLLocationAccuracyHundredMeters: \(kCLLocationAccuracyHundredMeters)")
        print("kCLLocationAccuracyKilometer: \(kCLLocationAccuracyKilometer)")
        print("kCLLocationAccuracyThreeKilometers: \(kCLLocationAccuracyThreeKilometers)")
    }
    
    func testCalculatingAuthToken() {
        let s = DataService()
        
        XCTAssertEqual(s.generateAuthToken("t0Psec3et", timestamp: 1461935000), "mFsjVwAAAAB/YUU0XrXxdq0cha2Tqzkt")
        XCTAssertEqual(s.generateAuthToken("t0Psec3et", timestamp: 1461935010), "olsjVwAAAADuzkDRuaIEr9gTJTcp/snb")
        XCTAssertEqual(s.generateAuthToken("t0Psec3et", timestamp: 1463057502), "Xnw0VwAAAADjzsxsZ1I85i5QR1+JLjwi")
        
        XCTAssertEqual(s.generateAuthToken("someT35t53ret", timestamp: 1461935000), "mFsjVwAAAACzzDw8c31ynkyY1Lbyf/bE")
        XCTAssertEqual(s.generateAuthToken("someT35t53ret", timestamp: 1461935010), "olsjVwAAAADIHw2XmQJHWN5czI4lz1At")
        XCTAssertEqual(s.generateAuthToken("someT35t53ret", timestamp: 1463057502), "Xnw0VwAAAACSqn0zDRk05FLdfMC41dOx")
    }
    
    /// Test 8:8 Fixed Point representation
    func testEddystoneTemparatureCalculation() {
        // Reference: https://courses.cit.cornell.edu/ee476/Math/
        
        XCTAssertEqual(EddystoneTelemetryData.calculateTemp(0x00, fractional: 0x00), 0.0)
        XCTAssertEqual(EddystoneTelemetryData.calculateTemp(0x01, fractional: 0x00), 1.0)
        XCTAssertEqual(EddystoneTelemetryData.calculateTemp(0x01, fractional: 0x80), 1.5)
        XCTAssertEqual(EddystoneTelemetryData.calculateTemp(0x01, fractional: 0xc0), 1.75)
        XCTAssertEqual(EddystoneTelemetryData.calculateTemp(0x01, fractional: 0x01), 1.00390625)// Reference is saying 1.00396 but if I calculate it manually it's correct.
        
        XCTAssertEqual(EddystoneTelemetryData.calculateTemp(unsafeBitCast(UInt8(0xff), to: Int8.self), fractional: 0x00), -1.0)
        XCTAssertEqual(EddystoneTelemetryData.calculateTemp(unsafeBitCast(UInt8(0xfe), to: Int8.self), fractional: 0x80), -1.5)
        XCTAssertEqual(EddystoneTelemetryData.calculateTemp(unsafeBitCast(UInt8(0xfe), to: Int8.self), fractional: 0x00), -2.0)
        XCTAssertEqual(EddystoneTelemetryData.calculateTemp(unsafeBitCast(UInt8(0x81), to: Int8.self), fractional: 0x00), -127.0)
        XCTAssertEqual(EddystoneTelemetryData.calculateTemp(unsafeBitCast(UInt8(0xff), to: Int8.self), fractional: 0x80), -0.5)
        XCTAssertEqual(EddystoneTelemetryData.calculateTemp(unsafeBitCast(UInt8(0xff), to: Int8.self), fractional: 0xc0), -0.25)
        
        XCTAssertEqual(EddystoneTelemetryData.calculateTemp(0x00, fractional: 0x80), 0.5)
        
        XCTAssertEqual(EddystoneTelemetryData.calculateTemp(unsafeBitCast(UInt8(0x80), to: Int8.self), fractional: 0x00), -128.0)
        
        XCTAssertEqual(EddystoneTelemetryData.calculateTemp(0x7f, fractional: 0x00), 127.0)
        XCTAssertEqual(EddystoneTelemetryData.calculateTemp(0x02, fractional: 0x40), 2.25)
        XCTAssertEqual(EddystoneTelemetryData.calculateTemp(unsafeBitCast(UInt8(0xfd), to: Int8.self), fractional: 0xc0), -2.25)
    }
    
    func testWifiRegionCompare() {
        let a: WifiRegion? = nil
        let b: WifiRegion? = nil
        XCTAssert(a == b)
        
        XCTAssert(WifiRegion().setup("Wifi1", bssid: nil) == WifiRegion().setup("Wifi1", bssid: nil))
        XCTAssert(WifiRegion().setup("Wifi", bssid: "0f:00:00:00:00") == WifiRegion().setup("Wifi", bssid: "00:00:00:00:00")) // BSSID is ignored
    }
}
