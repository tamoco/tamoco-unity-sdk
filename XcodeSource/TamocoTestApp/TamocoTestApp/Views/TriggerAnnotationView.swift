//
//  TriggerAnnotationView.swift
//  TamocoTestApp
//
//  Created by Sašo Sečnjak on 25/07/16.
//  Copyright © 2016 Tamoco. All rights reserved.
//

import UIKit
import MapKit
import Tamoco

class TriggerAnnotationView: MKAnnotationView {
    
    static let frameRect = CGRect(x: 0, y: 0, width: 30, height: 30)
    fileprivate var imageView: UIImageView = UIImageView(frame: TriggerAnnotationView.frameRect)
    
    override var annotation: MKAnnotation? {
        didSet {
            updateAnnotation()
        }
    }

    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        centerOffset = CGPoint(x: 0, y: -10)
        frame = TriggerAnnotationView.frameRect
        addSubview(imageView)
        
        updateAnnotation()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        updateAnnotation()
    }
    
    fileprivate func updateAnnotation() {
        if let annotation = annotation as? TriggerAnnotation {
            let trigger = annotation.trigger
            switch trigger {
            case is BeaconTrigger:
                imageView.image = (trigger?.isCurrent() ?? false) ? UIImage(named: "bluetoothActiveIcon") : UIImage(named: "bluetoothTabIcon")
                
            case is GeoFenceTrigger:
                imageView.image = (trigger?.isCurrent() ?? false) ? UIImage(named: "geofenceActiveIcon") : UIImage(named: "geofenceIcon")
                
            case is WifiTrigger:
                imageView.image = (trigger?.isCurrent() ?? false) ? UIImage(named: "wifiActiveIcon") : UIImage(named: "wifiIcon")
                
            case is EddystoneTrigger:
                imageView.image = (trigger?.isCurrent() ?? false) ? UIImage(named: "stoneActiveIcon") : UIImage(named: "stoneIcon")
                
            default:
                break
            }
        }
    }
}
