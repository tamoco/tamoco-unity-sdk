//
//  Storage.swift
//  TamocoTestApp
//
//  Created by Sašo Sečnjak on 26/07/16.
//  Copyright © 2016 Tamoco. All rights reserved.
//

import Foundation

open class Storage {
    
    // Properties
    open var apiKey: String?
    open var apiSecret: String?
    open var customId: String?
    
    // Keys
    fileprivate var apiKeyKey: String = "com.tamoco.storage.apiKey"
    fileprivate var apiSecretKey: String = "com.tamoco.storage.apiSecret"
    fileprivate var customIdKey: String = "com.tamoco.storage.customId"
    
    // Managing the storage
    
    fileprivate static var _sharedInstance: Storage!
    open class func sharedInstance() -> Storage {
        if _sharedInstance == nil {
            _sharedInstance = Storage()
            _sharedInstance.load()
        }
        
        return _sharedInstance
    }
    fileprivate var userDefaults: UserDefaults!
    
    // Group suite name
    fileprivate static let groupSuiteName: String = "group.com.tamoco.testapp"
    
    fileprivate func load() {
        userDefaults = getUserDefaults()
        
        apiKey = userDefaults.object(forKey: apiKeyKey) as? String
        apiSecret = userDefaults.object(forKey: apiSecretKey) as? String
        customId = userDefaults.object(forKey: customIdKey) as? String
    }
    
    open func reload() {
        load()
    }
    
    open func save() {
        userDefaults = getUserDefaults()
        
        userDefaults.set(apiKey, forKey: apiKeyKey)
        userDefaults.set(apiSecret, forKey: apiSecretKey)
        userDefaults.set(customId, forKey: customIdKey)
    }
    
    fileprivate func getUserDefaults() -> UserDefaults {
        if userDefaults == nil {
            userDefaults = UserDefaults(suiteName: Storage.groupSuiteName)!
        }
        
        return userDefaults
    }
}
