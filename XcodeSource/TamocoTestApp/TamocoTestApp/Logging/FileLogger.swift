//
//  FileLogger.swift
//  TamocoTestApp
//
//  Created by Sašo Sečnjak on 20/05/16.
//  Copyright © 2016 Tamoco. All rights reserved.
//

import Foundation
import MessageUI

/// Very simple file logger
class FileLogger {
    
    let fileName: String = "tamocoLogs.txt"
    var filePath: URL!
    var outputStream: OutputStream!
    
    init() {
        initialiseOutputStream()
        NotificationCenter.default.addObserver(self, selector: #selector(FileLogger.willTerminate(_:)), name: NSNotification.Name.UIApplicationWillTerminate, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(FileLogger.willEnterForeground(_:)), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(FileLogger.didEnterBackground(_:)), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func append(_ message: String) {
        outputStream.write("[\(Date())] - \(message)\n")
    }
    
    func removeAndCreate() {
        close()
        let _ = try? FileManager.default.removeItem(at: filePath)
        initialiseOutputStream()
    }
    
    func sendLogsViaEmail<T: UIViewController>(_ viewController: T) where T: MFMailComposeViewControllerDelegate {
        let mailComposer = MFMailComposeViewController()
        mailComposer.mailComposeDelegate = viewController
        
        mailComposer.setSubject("Tamoco logs")
        mailComposer.setMessageBody("Tamoco logs are attached.", isHTML: false)
        
        let fileUrl = filePath
        if let attachement = try? Data(contentsOf: fileUrl!) {
            mailComposer.addAttachmentData(attachement, mimeType: "text/plain", fileName: fileUrl!.path)
        }
        
        viewController.present(mailComposer, animated: true, completion: nil)
    }
    
    fileprivate func initialiseOutputStream() {
        filePath = createFilePath()
        print("\(filePath)")
        
        self.outputStream = OutputStream(url: filePath, append: true)
        self.outputStream.open()
    }
    
    fileprivate func close() {
        outputStream.close()
    }
    
    fileprivate func createFilePath() -> URL {
        if let dir = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.allDomainsMask, true).first {
            return URL(fileURLWithPath: dir).appendingPathComponent(fileName)
        }
        
        // Fallback
        return URL(fileURLWithPath: "")
    }
    
    // MARK: ApplicationStateObserver
    
    @objc func willTerminate(_ notification: Notification) {
        append("! Terminating app...")
        close()
    }
    
    @objc func willEnterForeground(_ notification: Notification) {
        append("! App will enter foreground")
    }
    
    @objc func didEnterBackground(_ notification: Notification) {
        append("! App did enter background")
    }
}
