//
//  RegularCellData.swift
//  TamocoTestApp
//
//  Created by Sašo Sečnjak on 25/04/16.
//  Copyright © 2016 Tamoco. All rights reserved.
//

import UIKit

func ==(lhs: RegularCellData, rhs: RegularCellData) -> Bool {
    guard lhs.id != nil || rhs.id != nil else  {
        return false
    }
    
    return lhs.hashValue == rhs.hashValue
}

class RegularCellData: Hashable {
    var id: String?
    var title: String?
    
    var hashValue: Int {
        return (id?.hashValue ?? -1) ^ (title?.hashValue ?? -1)
    }
}
