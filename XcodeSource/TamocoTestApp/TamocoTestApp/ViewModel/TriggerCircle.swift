//
//  TriggerCircle.swift
//  TamocoTestApp
//
//  Created by Sašo Sečnjak on 25/07/16.
//  Copyright © 2016 Tamoco. All rights reserved.
//

import UIKit
import MapKit

class TriggerCircle: MKCircle {
    var strokeColor: UIColor = UIColor.clear
    var fillColor: UIColor = UIColor.clear
}
