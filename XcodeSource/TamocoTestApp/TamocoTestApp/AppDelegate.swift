//
//  AppDelegate.swift
//  TamocoTestApp
//
//  Created by Sašo Sečnjak on 20/04/16.
//  Copyright © 2016 Tamoco. All rights reserved.
//

import UIKit
import Tamoco
import Fabric
import Crashlytics

let kReinitializedTamoco: String = "com.tamoco.reinitialized"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var tamoco: Tamoco!
    var logger: FileLogger!
    
    let defaultApiKey = "294" // default: 179
    let defaultApiSecret = "7a29132ddf8219407c07460272a41d061e56c782" // default: 100684bc46ce32c461914566112cf60225063777
    
    fileprivate let enableBackgroundFetchNotification: Bool = true //false
    
    class var app: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    /// Initializes the Tamoco lib with parameters from storage, or fallbacks to the default ones
    func initializeTamocoFromStorage() {
        let apiKey = Storage.sharedInstance().apiKey ?? defaultApiKey
        let apiSecret = Storage.sharedInstance().apiSecret ?? defaultApiSecret
        let customId = Storage.sharedInstance().customId
        
        initializeTamoco(apiKey, apiSecret: apiSecret, customId: customId)
    }
    
    fileprivate func initializeTamoco(_ apiKey: String, apiSecret: String, customId: String?) {
        if apiKey.isEmpty || apiSecret.isEmpty {
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) {
                let alert = UIAlertController(title: "API", message: "API key or secret is empty!", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
                    alert.dismiss(animated: true, completion: nil)
                }))
                UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
            }
        }
        
        tamoco = Tamoco(apiKey: apiKey, apiSecret: apiSecret)
        tamoco.customId = customId
        tamoco.listenOnly = false 
        
        tamoco.onCustomEddystoneAction = { [weak self] type, trigger in
            if self != nil {
                
                switch type {
                case .eddyEnter:
                    print("EddyEnter - \(trigger.namespaceId)")
                    
                case .eddyExit:
                    print("EddyExit - \(trigger.namespaceId)")
                    
                case .eddyDwell:
                    print("EddyDwell - \(trigger.namespaceId)")
                    
                case .eddyHover:
                    print("EddyHover - \(trigger.namespaceId)")
                    
                default:
                    print("Unknown action for \(trigger.namespaceId)")
                }
            }
        }
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        application.registerUserNotificationSettings(UIUserNotificationSettings(types: ([.sound, .alert, .badge]), categories: nil))
        Fabric.with([Crashlytics.self, Answers.self])
        
        logger = FileLogger()
        
        TamocoLogger.level = .full
        TamocoLogger.printClosure = { message in
            self.logger.append(message)
            CLSLogv("%@", getVaList([message]))
        }
        
        initializeTamocoFromStorage()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if enableBackgroundFetchNotification {
            let notification = UILocalNotification()
            notification.alertBody = "performFetchWithCompletionHandler" // text that will be displayed in the notification
            notification.fireDate = Date()
            notification.soundName = ""
            
            UIApplication.shared.scheduleLocalNotification(notification)
        }
        
        tamoco.performFetchWithCompletionHandler { fetchResult in
            completionHandler(fetchResult.uiBackgroundFetchResult)
        }
    }
    
    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
        tamoco.didReceiveLocalNotificationUserInfo(notification.userInfo)
    }
}

