//
//  GeofenceViewController.swift
//  TamocoTestApp
//
//  Created by Sašo Sečnjak on 25/04/16.
//  Copyright © 2016 Tamoco. All rights reserved.
//

import UIKit
import Tamoco

class GeofenceViewController: BaseTableViewController {

    fileprivate var observer: NSObjectProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        observer = NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: kReinitializedTamoco), object: nil, queue: nil) { [unowned self](notification) in
            self.view = nil
            self.distinctItems.removeAll()
        }
        
        if let current = AppDelegate.app.tamoco.currentGeofences {
            for trigger in current {
                let data = RegularCellData()
                data.fillDataFrom(trigger)
                distinctItems.insert(data)
            }
            
            content = Array(distinctItems)
            tableView.reloadData()
        }
        
        AppDelegate.app.tamoco.onCustomFenceAction = { [weak self] type, trigger in
            if let weakSelf = self {
                let data = RegularCellData()
                data.fillDataFrom(trigger)
                
                switch type {
                case .geoFenceEnter:
                    weakSelf.distinctItems.insert(data)
                    
                case .geoFenceExit:
                    weakSelf.distinctItems.remove(data)
                    
                default:
                    print("Unknown action for \(data.title ?? "")")
                }
                
                weakSelf.content = Array(weakSelf.distinctItems)
                weakSelf.tableView.reloadData()
            }
        }
    }
    
    deinit {
        if let observer = observer {
            NotificationCenter.default.removeObserver(observer)
        }
    }
}
