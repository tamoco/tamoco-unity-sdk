//
//  ConfigurationViewController.swift
//  TamocoTestApp
//
//  Created by Sašo Sečnjak on 26/07/16.
//  Copyright © 2016 Tamoco. All rights reserved.
//

import UIKit

class ConfigurationViewController: UIViewController {

    @IBOutlet weak var apiIdField: UITextField!
    @IBOutlet weak var apiSecretField: UITextField!
    @IBOutlet weak var customIdField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Close", style: .plain, target: self, action: #selector(ConfigurationViewController.hideMe))
        addResignRecognizerToView(view)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        apiIdField.text = Storage.sharedInstance().apiKey
        apiSecretField.text = Storage.sharedInstance().apiSecret
        customIdField.text = Storage.sharedInstance().customId
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Lets store the values
        if let apiId = apiIdField.text, !apiId.isEmpty {
            Storage.sharedInstance().apiKey = apiId
        } else {
            Storage.sharedInstance().apiKey = nil
        }
        
        if let apiSecret = apiSecretField.text, !apiSecret.isEmpty {
            Storage.sharedInstance().apiSecret = apiSecret
        } else {
            Storage.sharedInstance().apiSecret = nil
        }
        
        if let customId = customIdField.text, !customId.isEmpty {
            Storage.sharedInstance().customId = customId
        } else {
            Storage.sharedInstance().customId = nil
        }
        
        Storage.sharedInstance().save()
        
        // Reinitialize Library
        AppDelegate.app.initializeTamocoFromStorage()
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: kReinitializedTamoco), object: nil, userInfo: nil)
    }

    func hideMe() {
        navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func dismissKeyboard() {
        DispatchQueue.main.async {
            UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
        }
    }
    
    @discardableResult
    fileprivate func addResignRecognizerToView(_ view: UIView) -> UIGestureRecognizer {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ConfigurationViewController.dismissKeyboard))
        tapGesture.cancelsTouchesInView = true
        view.addGestureRecognizer(tapGesture)
        
        return tapGesture
    }
}
