//
//  PageViewController.swift
//  TamocoTestApp
//
//  Created by Sašo Sečnjak on 25/04/16.
//  Copyright © 2016 Tamoco. All rights reserved.
//

import UIKit

class PageViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {

    fileprivate var contentViewControllers: [UIViewController]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        contentViewControllers = [UIViewController]()
        
        // Lets instantiate view cotrollers
        let beaconViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BeaconViewController")
        let geofenceViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GeofenceViewController")
        let wifiViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WifiViewController")
        let miscellaneousViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MiscellaneousViewController")
        
        // Lets preload the views
        let _ = beaconViewController.view
        let _ = geofenceViewController.view
        let _ = wifiViewController.view
        let _ = miscellaneousViewController.view
        
        contentViewControllers?.append(beaconViewController)
        contentViewControllers?.append(geofenceViewController)
        contentViewControllers?.append(wifiViewController)
        contentViewControllers?.append(miscellaneousViewController)
        
        dataSource = self
        delegate = self
        
        if let first = contentViewControllers?[0] {
            setViewControllers([first], direction: UIPageViewControllerNavigationDirection.forward, animated: true, completion: nil)
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if let index = contentViewControllers?.index(of: viewController), index >= 1 {
            return contentViewControllers?[index - 1]
        }
        
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if let index = contentViewControllers?.index(of: viewController), let count = contentViewControllers?.count, index < count - 1 {
            return contentViewControllers?[index + 1]
        }
        
        return nil
    }
}
