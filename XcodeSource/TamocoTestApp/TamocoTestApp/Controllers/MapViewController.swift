//
//  MapViewController.swift
//  TamocoTestApp
//
//  Created by Sašo Sečnjak on 25/07/16.
//  Copyright © 2016 Tamoco. All rights reserved.
//

import UIKit
import Tamoco
import MapKit

class MapViewController: UIViewController, MKMapViewDelegate {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var positionButton: UIButton!
    
    fileprivate var observers = [NSObjectProtocol]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Update annotation on any of these actions...
        observers.append(NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: TamocoOnTriggersFetched), object: nil, queue: nil) { [unowned self](notification) in
            DispatchQueue.main.async(execute: { 
                self.updateAnnotations()
            })
        })
        
        observers.append(NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: TamocoOnCustomBeaconAction), object: nil, queue: nil) { [unowned self](notification) in
            DispatchQueue.main.async(execute: {
                self.updateAnnotations()
            })
        })
        
        observers.append(NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: TamocoOnCustomFenceAction), object: nil, queue: nil) { [unowned self](notification) in
            DispatchQueue.main.async(execute: {
                self.updateAnnotations()
            })
        })
        
        observers.append(NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: TamocoOnCustomWIFIAction), object: nil, queue: nil) { [unowned self](notification) in
            DispatchQueue.main.async(execute: {
                self.updateAnnotations()
            })
        })

        // Lets show the compas in case user rotates the map
        if #available(iOS 9.0, *) {
            mapView.showsCompass = true
        }
        
        // Start listening for changes
        mapView.delegate = self
        
        // Enable snapping to users position
        positionButton.isSelected = true
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        for observer in observers {
            NotificationCenter.default.removeObserver(observer)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateAnnotations()
        UIApplication.shared.isIdleTimerDisabled = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.isIdleTimerDisabled = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - User actions
    
    @IBAction func positionToCurrentPressed(_ sender: AnyObject) {
        positionButton.isSelected = !positionButton.isSelected
        snapToPosition(mapView.userLocation.coordinate)
    }
    
    // MARK: - Business
    
    fileprivate func snapToPosition(_ coordinate: CLLocationCoordinate2D, latitudinalMeters: CLLocationDistance = 200, longitudinalMeters: CLLocationDistance = 200) {
        let region = MKCoordinateRegionMakeWithDistance(coordinate, 200, 200)
        mapView.setRegion(region, animated: true)
    }
    
    fileprivate func updateAnnotations() {
        mapView.removeOverlays(mapView.overlays)
        mapView.removeAnnotations(mapView.annotations)
        
        // Transform triggers to annotations
        var annotiations = [TriggerAnnotation]()
        var overlays = [MKOverlay]()
        
        if let beacons = AppDelegate.app.tamoco.beacons {
            for beacon in beacons {
                annotiations.append(TriggerAnnotation.mapFrom(beacon))
            }
        }
        
        if let geofences = AppDelegate.app.tamoco.geofences {
            for geofence in geofences {
                annotiations.append(TriggerAnnotation.mapFrom(geofence))
                let circle = TriggerCircle(center: geofence.location, radius: geofence.distance)
                if geofence.isCurrent() {
                    let ciColor = CIColor(red: 255, green: 0, blue: 153)
                    circle.strokeColor = UIColor(red: ciColor.red, green: ciColor.green, blue: ciColor.blue, alpha: 1)
                    circle.fillColor = UIColor(red: ciColor.red, green: ciColor.green, blue: ciColor.blue, alpha: 0.1)
                } else {
                    circle.strokeColor = UIColor.blue
                    circle.fillColor = UIColor(red: 0, green: 0, blue: 255, alpha: 0.1)
                }
                
                overlays.append(circle)
            }
        }
        
        if let wifis = AppDelegate.app.tamoco.wifis {
            for wifi in wifis {
                annotiations.append(TriggerAnnotation.mapFrom(wifi))
            }
        }
        
        if let eddystones = AppDelegate.app.tamoco.eddystones {
            for eddystone in eddystones {
                annotiations.append(TriggerAnnotation.mapFrom(eddystone))
            }
        }
        
        mapView.addOverlays(overlays)
        mapView.addAnnotations(annotiations)
    }
    
    // MARK: MKMapViewDelegate
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        if positionButton.isSelected {
            snapToPosition(userLocation.coordinate)
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        switch annotation {
        case is MKUserLocation:
            return nil
            
        case is TriggerAnnotation:
            let identifier = "TriggerAnnotationView"
            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            
            if (annotationView != nil) {
                annotationView?.annotation = annotation
            } else {
                annotationView = TriggerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            }
            
            return annotationView
            
        default:
            return nil
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        switch overlay {
        case is TriggerCircle:
            let triggerCircle = overlay as! TriggerCircle
            let circle = MKCircleRenderer(overlay: overlay)
            circle.strokeColor = triggerCircle.strokeColor
            circle.fillColor = triggerCircle.fillColor
            circle.lineWidth = 1
            
            return circle
            
        case is MKCircle:
            let circle = MKCircleRenderer(overlay: overlay)
            circle.strokeColor = UIColor.blue
            circle.fillColor = UIColor(red: 0, green: 0, blue: 255, alpha: 0.1)
            circle.lineWidth = 1
            
            return circle
            
        default:
            return MKOverlayRenderer(overlay: overlay)
        }
    }

}
