//
//  MiscellaneousViewController.swift
//  TamocoTestApp
//
//  Created by Sašo Sečnjak on 20/05/16.
//  Copyright © 2016 Tamoco. All rights reserved.
//

import UIKit
import MessageUI

class MiscellaneousViewController: UIViewController, MFMailComposeViewControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func clearLogsPressed(_ sender: UIButton) {
        AppDelegate.app.logger.removeAndCreate()
        let alert = UIAlertController(title: "Cleared", message: "Logs are cleared!", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func sendLogsPressed(_ sender: UIButton) {
        AppDelegate.app.logger.sendLogsViaEmail(self)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}
