//
//  BaseTableViewController.swift
//  TamocoTestApp
//
//  Created by Sašo Sečnjak on 25/04/16.
//  Copyright © 2016 Tamoco. All rights reserved.
//

import UIKit

class BaseTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    var distinctItems = Set<RegularCellData>()
    var content: [RegularCellData]?
    
    fileprivate let regularCellIdentifier: String = "RegularCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "RegularCell", bundle: nil), forCellReuseIdentifier: regularCellIdentifier)
        
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return content?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = (tableView.dequeueReusableCell(withIdentifier: regularCellIdentifier) as? RegularCell) ?? RegularCell()
        if let data = content?[indexPath.row] {
            cell.setData(data)
        }
        
        return cell
    }
}
