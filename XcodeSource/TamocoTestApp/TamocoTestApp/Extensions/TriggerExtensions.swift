//
//  TriggerExtensions.swift
//  TamocoTestApp
//
//  Created by Sašo Sečnjak on 25/07/16.
//  Copyright © 2016 Tamoco. All rights reserved.
//

import Foundation
import Tamoco

extension Trigger {
    
    func isCurrent() -> Bool {
        return AppDelegate.app.tamoco.isCurrent(self)
    }
}