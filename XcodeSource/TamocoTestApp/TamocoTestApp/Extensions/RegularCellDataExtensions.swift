//
//  RegularCellDataExtensions.swift
//  TamocoTestApp
//
//  Created by Sašo Sečnjak on 25/04/16.
//  Copyright © 2016 Tamoco. All rights reserved.
//

import Foundation
import Tamoco

extension RegularCellData {
    
    func fillDataFrom(_ trigger: Trigger) {
        id = trigger.id
        title = trigger.id
    }
    
    func fillDataFrom(_ trigger: GeoFenceTrigger) {
        id = trigger.id
        title = String(format: "%@ - %@", trigger.id, trigger.name ?? "")
    }
    
    func fillDataFrom(_ trigger: BeaconTrigger) {
        id = trigger.id
        title = String(format: "%@, major: %d, minor: %d", trigger.beaconId ?? "?", trigger.major ?? "?", trigger.minor ?? "?")
    }
    
    func fillDataFrom(_ trigger: WifiTrigger) {
        id = trigger.id
        title = String(format: "%@ - %@", trigger.id, trigger.ssid ?? "?")
    }
}
