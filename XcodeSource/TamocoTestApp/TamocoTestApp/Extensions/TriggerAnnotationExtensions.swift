//
//  TriggerAnnotationExtensions.swift
//  TamocoTestApp
//
//  Created by Sašo Sečnjak on 25/07/16.
//  Copyright © 2016 Tamoco. All rights reserved.
//

import Foundation
import Tamoco

extension TriggerAnnotation {
    
    class func mapFrom(_ trigger: Trigger) -> TriggerAnnotation {
        let annotation = TriggerAnnotation(coordinate: trigger.location, title: trigger.name ?? "", subtitle: "")
        annotation.trigger = trigger
        
        return annotation
    }
}
