Pod::Spec.new do |s|
  s.name         = "Tamoco"
  s.version      = "1.0.2"
  s.summary      = "Tamoco iOS SDK"
  s.description  = "Tamoco iOS SDK Binary SDK for integrating proximity awareness"
  s.homepage     = "https://www.tamoco.com"
  s.license      = "Apache License, Version 2.0"
  s.author       = { "Rune Bromer" => "rune.bromer@tamaco.co" }
  s.platform     = :ios, "8.0"
  s.source       = { :git => 'https://bitbucket.org/tamoco/tamoco-ios-sdk.git', :tag => "#{s.version}" }
  s.ios.vendored_frameworks = 'Tamoco.framework'
  s.pod_target_xcconfig = { 'SWIFT_VERSION' => '3.0' }
end
